package com.javacore.transportMgmt;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Color;
import javax.swing.JSeparator;
import javax.swing.JTextField;

import com.javacore.transportMgmt.db.EntityDao;
import com.javacore.transportMgmt.model.Insurance;
import com.toedter.calendar.JDateChooser;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

public class InsuranceDialog extends JDialog implements ActionListener {

	private final JPanel contentPanel = new JPanel();
	private JTextField txt_com_name;
	private JTextField txt_policy_num;
	private JTextField txt_policy_Amt;
	private JButton okButton;
	private JButton cancelButton;
	private JButton btnDelete;
	private InsuranceListener listener;
	private String gr_num;
	private JComboBox cmbxRisk;
	private JDateChooser dc_po_date;

	private EntityDao entityDao = new EntityDao();
	public interface InsuranceListener {
		public void onSaveInsurance(Insurance insurance);

		public void onPressCancel();

		public void onPressDelete();
	}

	public void setInterface(Object obj) {
		this.listener = (InsuranceListener) obj;
	}

	public String getGr_num() {
		return gr_num;
	}

	public void setGr_num(String gr_num) {
		this.gr_num = gr_num;
		Insurance insurance = entityDao.getInsuranceObject(Integer.parseInt(gr_num));
		if (insurance != null) {
			txt_com_name.setText(insurance.getCompany_name());
			txt_policy_num.setText(insurance.getPolicy_num());
			txt_policy_Amt.setText(String.format("%.2f", insurance.getAmount()));
			dc_po_date.setDate(insurance.getIdate());
			cmbxRisk.setSelectedItem(insurance.getRisk());
		}
	}

	public InsuranceDialog() {
		setBounds(100, 100, 667, 385);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBackground(Color.WHITE);
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);

		JComboBox comboBox = setMainPanel();
		contentPanel.add(comboBox);
		{
			bottompanel();
		}
	}

	private JComboBox setMainPanel() {
		JLabel lblInsurance = new JLabel("INSURANCE");
		lblInsurance.setFont(new Font("Times New Roman", Font.BOLD, 20));
		lblInsurance.setHorizontalAlignment(SwingConstants.CENTER);
		lblInsurance.setBounds(258, 0, 145, 23);
		contentPanel.add(lblInsurance);

		JLabel lblNewLabel = new JLabel(
				"The Customer has Stated that he has not ensured the consignment OR he has isured the consignment\r\n");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(10, 30, 631, 16);
		contentPanel.add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("Company Name:");
		lblNewLabel_1.setBounds(10, 73, 145, 28);
		contentPanel.add(lblNewLabel_1);

		txt_com_name = new JTextField();
		txt_com_name.setBounds(165, 73, 476, 30);
		contentPanel.add(txt_com_name);
		txt_com_name.setColumns(10);

		JLabel lblPloicy = new JLabel("Policy Number:");
		lblPloicy.setBounds(10, 112, 113, 28);
		contentPanel.add(lblPloicy);

		txt_policy_num = new JTextField();
		txt_policy_num.setColumns(10);
		txt_policy_num.setBounds(165, 112, 215, 30);
		contentPanel.add(txt_policy_num);

		JLabel lblDate = new JLabel("Date:");
		lblDate.setBounds(10, 147, 113, 28);
		contentPanel.add(lblDate);

		dc_po_date = new JDateChooser();
		dc_po_date.setDateFormatString("dd-MM-yyyy");
		dc_po_date.setBounds(165, 147, 215, 26);
		contentPanel.add(dc_po_date);

		JLabel lblAmount = new JLabel("Amount:");
		lblAmount.setBounds(10, 178, 113, 28);
		contentPanel.add(lblAmount);

		txt_policy_Amt = new JTextField();
		txt_policy_Amt.setColumns(10);
		txt_policy_Amt.setBounds(165, 178, 215, 30);
		contentPanel.add(txt_policy_Amt);

		JLabel lblRisk = new JLabel("Risk:");
		lblRisk.setBounds(10, 214, 113, 28);
		contentPanel.add(lblRisk);

		cmbxRisk = new JComboBox();
		cmbxRisk.setModel(new DefaultComboBoxModel(new String[] { "Normal", "Worst", "Type1" }));
		cmbxRisk.setEditable(true);
		cmbxRisk.setBounds(165, 219, 215, 25);
		return cmbxRisk;
	}

	private void bottompanel() {
		JPanel buttonPane = new JPanel();
		buttonPane.setBackground(Color.WHITE);
		buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
		getContentPane().add(buttonPane, BorderLayout.SOUTH);
		{
			okButton = new JButton("Save");
			okButton.addActionListener(this);
			buttonPane.add(okButton);
			getRootPane().setDefaultButton(okButton);
		}

		btnDelete = new JButton("Delete");
		btnDelete.addActionListener(this);
		buttonPane.add(btnDelete);
		{
			cancelButton = new JButton("Cancel");
			cancelButton.addActionListener(this);
			buttonPane.add(cancelButton);
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == okButton) {

			if (!validateForm().isEmpty()) {
				showMessageDialog(validateForm());
			} else {
				Insurance insurance = new Insurance();
				insurance.setGr_num(Integer.parseInt(gr_num));
				insurance.setCompany_name(txt_com_name.getText());
				insurance.setPolicy_num(txt_policy_num.getText());
				insurance.setAmount(Double.parseDouble(txt_policy_Amt.getText()));
				insurance.setRisk(cmbxRisk.getSelectedItem().toString());
				System.out.println("Select Date"+dc_po_date.getDate());
				insurance.setIdate(dc_po_date.getDate());
				listener.onSaveInsurance(insurance);
				
			}
		}
		
		if (e.getSource() == cancelButton) {
			this.dispose();
		}

	}

	private String validateForm() {
		StringBuilder str = new StringBuilder();
		if (txt_com_name.getText().isEmpty()) {
			str.append("Please Enter Company Name.\n");
		}
		if (txt_policy_num.getText().isEmpty()) {
			str.append("Please Enter Policy Number.\n");
		}
		if (txt_policy_Amt.getText().isEmpty()) {
			str.append("Enter Policy Amount.\n");
		}
		return str.toString();
	}
	
	private void showMessageDialog(String message) {
		JOptionPane.showMessageDialog(null, message);
	}
	
}
