package com.javacore.transportMgmt.db;

import java.io.IOException;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.javacore.transportMgmt.model.BilityModel;
import com.javacore.transportMgmt.model.Bill;
import com.javacore.transportMgmt.model.Constants;
import com.javacore.transportMgmt.model.CreateBillEntry;
import com.javacore.transportMgmt.model.Customer;
import com.javacore.transportMgmt.model.Insurance;
import com.javacore.transportMgmt.model.Lagguage;
import com.javacore.transportMgmt.model.OwnerDetails;
import com.javacore.transportMgmt.model.Vehicle;
import com.javacore.transportMgmt.utility.PdfModel;

public class EntityDao {

	public int storeInDatabase(Object obj, int sync_status) {
		Integer pk = 0;

		if (obj instanceof BilityModel) {
			pk = storeBility((BilityModel) obj);
		}

		if (obj instanceof Vehicle) {
			pk = storeBility((Vehicle) obj);
		}

		if (obj instanceof Bill) {
			pk = storeBility((Bill) obj, sync_status);
		}

		return pk;
	}

	private int storeBility(BilityModel obj) {

		String sql = "INSERT INTO bility_table (adv_pay ,balnce" + " , bank_name, bilty_status, cgst"
				+ "	, ch_dd_date , ch_dd_no , chck_exp " + "	, consignee_address , consignee_name "
				+ "	, consignee_gst, consignor_address , consignor_name"
				+ "	, consignor_gst , freight_type , freight" + "	, gr_date , grand_total , gst_paid_by"
				+ "	, pay_mode, sgst , stat_charge " + "	, station_from , station_to , veh_no"
				+ "	, laggauge_id , insurance_in_id) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		int lagguageId = 0;
		int insuranceId = 0;
		if (obj.getInsurance() != null) {
			insuranceId = storeInsurance(obj.getInsurance());
		}

		if (obj.getLagguage() != null) {
			lagguageId = storeLuggage(obj.getLagguage());
		}

		try (Connection conn = HibernateUtils.getConnection()) {
			pst = conn.prepareStatement(sql);
			pst.setDouble(1, obj.getAdv_pay());
			pst.setDouble(2, obj.getBalnce());
			pst.setString(3, obj.getBank_name());
			pst.setString(4, obj.getBilty_status());
			pst.setDouble(5, obj.getCgst());
			try {
				if (obj.getCh_dd_date() != null) {
					pst.setDate(6, new java.sql.Date(obj.getCh_dd_date().getTime()));
				} else {
					pst.setDate(6, null);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			pst.setString(7, obj.getCh_dd_no());
			pst.setDouble(8, obj.getChck_exp());
			pst.setString(9, obj.getConsignee().getAddress());
			pst.setString(10, obj.getConsignee().getCustomer_name());
			pst.setString(11, obj.getConsignee().getGst_no());
			pst.setString(12, obj.getConsignor().getAddress());
			pst.setString(13, obj.getConsignor().getCustomer_name());
			pst.setString(14, obj.getConsignor().getGst_no());
			pst.setString(15, obj.getFre_type());
			pst.setDouble(16, obj.getFreight());
			pst.setDate(17, new java.sql.Date(obj.getGr_date().getTime()));
			pst.setDouble(18, obj.getGrand_total());
			pst.setString(19, obj.getGst_paid_by());
			pst.setString(20, obj.getPay_mode());
			pst.setDouble(21, obj.getSgst());
			pst.setDouble(22, obj.getStat_charge());
			pst.setString(23, obj.getStat_from());
			pst.setString(24, obj.getStat_to());
			pst.setString(25, obj.getVeh_num());
			pst.setInt(26, lagguageId);
			pst.setInt(27, insuranceId);
			return pst.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return 0;
	}

	private int storeBility(Vehicle obj) {
		try (Connection conn = HibernateUtils.getConnection()) {
			String sql = "insert into vehicle (`author` , `fit_form`, `fit_to` , `ins_from`,`ins_to` , `per_form` , `per_to` , "
					+ "`pollute`, `rd_tax`, `ser_form`, `ser_to` , `v_chasis`, `v_engine`, `v_model` "
					+ ", `v_num`,`v_type`, `owner_name`, `owner_mobile`, `driver_mobile`, `driver_name`)"
					+ "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			pst = conn.prepareStatement(sql);
			pst.setString(1, obj.getAuthor());
			pst.setString(2, obj.getFit_form());
			pst.setString(3, obj.getFit_to());
			pst.setString(4, obj.getIns_from());
			pst.setString(5, obj.getIns_to());
			pst.setString(6, obj.getPer_form());
			pst.setString(7, obj.getPer_to());
			pst.setString(8, obj.getPollute());
			pst.setString(9, obj.getRd_tax());
			pst.setString(10, obj.getSer_form());
			pst.setString(11, obj.getSer_to());
			pst.setString(12, obj.getV_chasis());
			pst.setString(13, obj.getV_engine());
			pst.setString(14, obj.getV_model());
			pst.setString(15, obj.getV_num());
			pst.setString(16, obj.getV_type());
			pst.setString(17, obj.getOwner_name());
			pst.setString(18, obj.getOwner_mob());
			pst.setString(19, obj.getDriver_mob());
			pst.setString(20, obj.getDriver_name());
			return pst.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return 0;
	}

	private int storeLuggage(Lagguage l) {
		// CREATE TABLE lagguage ( l_id integer, items_desc varchar(255), total_pkgs
		// double precision,
		// total_qunt double precision, total_wt double precision, primary key (l_id) )
		String sql = "insert into lagguage (items_desc, total_pkgs," + "total_qunt, total_wt ) values (?,?,?,?)";
		try (Connection conn = HibernateUtils.getConnection()) {
			pst = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			pst.setString(1, l.getItems_desc());
			pst.setDouble(2, l.getT_packages());
			pst.setDouble(3, l.getT_qty());
			pst.setDouble(4, l.getT_wt());
			pst.executeUpdate();
			ResultSet rs = pst.getGeneratedKeys();
			if (rs != null && rs.next()) {
				return rs.getInt(1);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return 0;
	}

	private int storeInsurance(Insurance i) {
		// CREATE TABLE insurance_table ( in_id integer, amount double precision not
		// null, company_name varchar(100),
		// gr_num integer not null, idate datetime, policy_num varchar(100), risk
		// varchar(255), primary key (in_id) )

		String sql = "insert into insurance_table ( amount , company_name ,"
				+ "gr_num , idate, policy_num , risk ) values (?,?,?,?,?,?)";
		try (Connection conn = HibernateUtils.getConnection()) {
			pst = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			pst.setDouble(1, i.getAmount());
			pst.setString(2, i.getCompany_name());
			pst.setInt(3, i.getGr_num());
			try {
				if (i.getIdate() != null)
					pst.setDate(4, new java.sql.Date(i.getIdate().getTime()));
				else {
					pst.setDate(4, null);
				}
			} catch (Exception e) {
				// TODO: handle exception
				pst.setDate(4, null);
			}

			pst.setString(5, i.getPolicy_num());
			pst.setString(6, i.getRisk());
			pst.executeUpdate();
			ResultSet rs = pst.getGeneratedKeys();
			if (rs != null && rs.next()) {
				return rs.getInt(1);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return 0;
	}

	private int storeBility(CreateBillEntry obj, int bill_id) {
		/*
		 * CREATE TABLE bill_entries ( s_id integer not null, amount double precision
		 * not null, att_gr varchar(50), bid_id varchar(100), bill_date datetime, nug_cb
		 * varchar(50), rate double precision not null, station varchar(100), total_amt
		 * double precision not null, weight varchar(50), billNo_bill_id integer,
		 * vehicle_v_id integer, entries_bill_id integer, primary key (s_id) )
		 */

		try (Connection conn = HibernateUtils.getConnection()) {
			String sql = "insert into bill_entries ( amount , att_gr, bid_id, bill_date , nug_cb, rate ,"
					+ " station , total_amt, weight, "
					+ "vehicle_v_id , entries_bill_id) values (?,?,?,?,?,?,?,?,?,?,?)";
			pst = conn.prepareStatement(sql);
			pst.setDouble(1, obj.getAmount());
			pst.setString(2, obj.getAtt_gr());
			pst.setString(3, obj.getBid_id());
			pst.setDate(4, new java.sql.Date(obj.getBill_date().getTime()));
			pst.setString(5, obj.getNug_cb());
			pst.setString(6, obj.getRate());
			pst.setString(7, obj.getStation());
			pst.setDouble(8, obj.getTotal_amt());
			pst.setString(9, obj.getWeight());
			pst.setInt(10, obj.getVehicle().getV_id());
			pst.setInt(11, bill_id);
			return pst.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return 0;
	}

	private int storeBility(Bill obj, int sync_status) {

		String sql = "insert into bill_generate ( bill_date," + "customer_address , customer_name "
				+ ", customer_gst_no, total_amt,sync_state) values (?,?,?,?,?,?)";
		int bill_id = 0;
		try (Connection conn = HibernateUtils.getConnection()) {
			pst = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			pst.setDate(1, new java.sql.Date(obj.getBill_date().getTime()));
			pst.setString(2, obj.getClient().getAddress());
			pst.setString(3, obj.getClient().getCustomer_name());
			pst.setString(4, obj.getClient().getGst_no());
			pst.setDouble(5, obj.getTotal_amt());
			pst.setInt(6, sync_status);
			pst.executeUpdate();

			ResultSet rs = pst.getGeneratedKeys();
			if (rs != null && rs.next()) {
				bill_id = rs.getInt(1);
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		List<CreateBillEntry> entries = obj.getEntries();
		for (CreateBillEntry entry : entries) {
			storeBility(entry, obj.getBill_id());
		}

		generatePdf(1,bill_id);
		return bill_id;
	}

	private void generatePdf(final int type, final int bill_id) {

		switch (type) {
		case 1:
			new Thread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					new PdfModel().createBill(bill_id);
				}
			}).start();
			break;
		case 2:
			new Thread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					new PdfModel().createBility(bill_id);
				}
			}).start();
			break;

		}

	}

	public void updateDatabase(Object obj) {

		if (obj instanceof BilityModel) {
			updateBility((BilityModel) obj);
		}

		if (obj instanceof Vehicle) {
			updateBility((Vehicle) obj);
		}

		if (obj instanceof CreateBillEntry) {
			updateBility((CreateBillEntry) obj);
		}

		if (obj instanceof Bill) {
			updateBility((Bill) obj);
		}

	}

	private int updateBility(BilityModel obj) {
/*
 * ( gr_id "adv_pay" , "balnce" , 
 * "bank_name", "bilty_status" , "cgst" , "ch_dd_date" , 
 * "ch_dd_no", "chck_exp", "consignee_address", "consignee_name" 
 * , "consignee_gst", "consignor_address", "consignor_name" ,
 *  "consignor_gst", "freight_type", "freight" , "gr_date" , 
 *  "grand_total" , "gst_paid_by" , "pay_mode" , "sgst" ,
 *   "stat_charge" , "station_from",
 *  "station_to" , "veh_no" , "laggauge_id" , "insurance_in_id" */
		
		String cols[] = {"adv_pay" , "balnce" , 
				  "bank_name", "bilty_status" , "cgst" , "ch_dd_date" , 
				  "ch_dd_no", "chck_exp", "consignee_address", "consignee_name" 
				  , "consignee_gst", "consignor_address", "consignor_name" ,
				   "consignor_gst", "freight_type", "freight" , 
				   "grand_total" , "gst_paid_by" , "pay_mode" , "sgst" ,
				    "stat_charge" , "station_from",
				   "station_to" , "veh_no" };
		Object values[] = {obj.getAdv_pay(),obj.getBalnce(),obj.getBank_name(),obj.getBilty_status(),obj.getCgst(),
				obj.getCh_dd_date(),obj.getCh_dd_no(), obj.getChck_exp(), obj.getConsignee().getAddress(),
				obj.getConsignee().getCustomer_name(),obj.getConsignee().getGst_no(),obj.getConsignor().getAddress(),
				obj.getConsignor().getCustomer_name(),obj.getConsignor().getGst_no(),obj.getFre_type(),obj.getFreight(),
				obj.getGrand_total(),obj.getGst_paid_by(),obj.getPay_mode(),obj.getSgst(),
				obj.getStat_charge(),obj.getStat_from(),obj.getStat_to(),obj.getVeh_num()};
		String where = "where gr_id = "+obj.getGr_id();
		updateObject("bility_table", cols, values, where);
		return 0;
	}

	private int updateBility(Vehicle obj) {
		  
		String cols[] = { "author" , "fit_form" ,
				"fit_to", "ins_from" , "ins_to" , "per_form" ,
				"per_to", "pollute", "rd_tax" , "ser_form" , "ser_to"
				, "v_chasis", "v_engine" , "v_model" , "v_num" 
				, "v_type", "owner_name", 
				"owner_mobile" , "driver_mobile" , "driver_name"};
		String values[] = {obj.getAuthor(),obj.getFit_form(),obj.getFit_to(),obj.getIns_from(),
				obj.getIns_to(),obj.getPer_form(),obj.getPer_to(),obj.getPollute(),obj.getRd_tax(),
				obj.getSer_form(),obj.getSer_to(),obj.getV_chasis(),obj.getV_engine(),obj.getV_model(),
				obj.getV_num(),obj.getV_type(),obj.getOwner_name(),obj.getOwner_mob(),obj.getDriver_mob(),obj.getDriver_name()};
		String where = " where v_id = "+obj.getV_id();
		updateObject("vehicle", cols, values, where);
		return 0;
	}

	private int updateBility(CreateBillEntry obj) {

		return 0;
	}

	private int updateBility(Bill obj) {

		return 0;
	}

	public void updateObject(String classname, String[] cols, Object[] values, String where) {
		
		StringBuilder sb = new StringBuilder();
		for(int i =0;i<cols.length;i++) {
			if(values[i] instanceof String) {
				sb.append(cols[i]).append("=").append("\"").append(values[i]).append("\"").append(",");
			}else {
				sb.append(cols[i]).append("=").append(values[i]).append(",");	
			}
			
		}
		
		String updateFields = sb.substring(0, sb.length()-",".length());
		String sql = "update "+classname+" set "+updateFields + where;
		System.out.println(sql);
		try(Connection conn = HibernateUtils.getConnection()) {
			pst = conn.prepareStatement(sql);
			pst.executeUpdate();
			System.out.println("Row updated successfully..!!!");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		
	}

	PreparedStatement pst = null;
	ResultSet rs = null;

	public List<Vehicle> getVehicles() {
		List<Vehicle> vehicles = new ArrayList<>();
		try (Connection conn = HibernateUtils.getConnection()) {
			String sql = "SELECT * FROM vehicle";
			pst = conn.prepareStatement(sql);
			rs = pst.executeQuery();
			while (rs.next()) {
				Vehicle v = new Vehicle();
				v.setV_id(rs.getInt("v_id"));
				v.setAuthor(rs.getString("author"));
				v.setFit_form(rs.getString("fit_form"));
				v.setFit_to(rs.getString("fit_to"));
				v.setIns_from(rs.getString("ins_from"));
				v.setIns_to(rs.getString("ins_to"));
				v.setPer_form(rs.getString("per_form"));
				v.setPer_to(rs.getString("per_to"));
				v.setPollute(rs.getString("pollute"));
				v.setRd_tax(rs.getString("rd_tax"));
				v.setSer_form(rs.getString("ser_form"));
				v.setSer_to(rs.getString("ser_to"));
				v.setV_chasis(rs.getString("v_chasis"));
				v.setV_engine(rs.getString("v_engine"));
				v.setV_model(rs.getString("v_model"));
				v.setV_num(rs.getString("v_num"));
				v.setV_type(rs.getString("v_type"));
				v.setOwner_name(rs.getString("owner_name"));
				v.setOwner_mob(rs.getString("owner_mobile"));
				v.setDriver_mob(rs.getString("driver_mobile"));
				v.setDriver_name(rs.getString("driver_name"));
				vehicles.add(v);
			}
			return vehicles;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return vehicles;
	}

	public List<String> getVehicleNames() {
		List<String> vehicles = new ArrayList<>();
		vehicles.add(Constants.SELECT_VEHICLE);
		try (Connection conn = HibernateUtils.getConnection()) {
			String sql = "select v_num from vehicle";
			pst = conn.prepareStatement(sql);
			rs = pst.executeQuery();
			while (rs.next()) {
				String str = rs.getString(1);
				vehicles.add(str);
			}
			return vehicles;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return vehicles;
	}

	public HashSet<String> getColList(String... strings) {
		HashSet<String> list = new HashSet<>();
		list.add("");
		String HQL = "SELECT " + "?" + " from " + "?";
		String HQ2 = "SELECT " + "?" + " from " + "?";
		try (Connection conn = HibernateUtils.getConnection()) {
			pst = conn.prepareStatement(HQL);
			pst.setString(1, strings[1]);
			pst.setString(2, strings[0]);
			pst.addBatch();
			pst.setString(1, strings[2]);
			pst.setString(2, strings[0]);
			pst.addBatch();
			rs = pst.executeQuery();
			while (rs.next()) {
				String str = rs.getString(1);
				list.add(str);
			}

			return list;
		} catch (Exception e) {
			// TODO: handle exception
		}

		return list;

	}

	public Vehicle getVehicle(int pk) {
		Vehicle v = new Vehicle();
		try (Connection conn = HibernateUtils.getConnection()) {
			String sql = "SELECT * FROM vehicle where v_id = ?";
			pst = conn.prepareStatement(sql);
			pst.setInt(1, pk);
			rs = pst.executeQuery();
			while (rs.next()) {
				v.setV_id(pk);
				v.setAuthor(rs.getString("author"));
				v.setFit_form(rs.getString("fit_form"));
				v.setFit_to(rs.getString("fit_to"));
				v.setIns_from(rs.getString("ins_from"));
				v.setIns_to(rs.getString("ins_to"));
				v.setPer_form(rs.getString("per_form"));
				v.setPer_to(rs.getString("per_to"));
				v.setPollute(rs.getString("pollute"));
				v.setRd_tax(rs.getString("rd_tax"));
				v.setSer_form(rs.getString("ser_form"));
				v.setSer_to(rs.getString("ser_to"));
				v.setV_chasis(rs.getString("v_chasis"));
				v.setV_engine(rs.getString("v_engine"));
				v.setV_model(rs.getString("v_model"));
				v.setV_num(rs.getString("v_num"));
				v.setV_type(rs.getString("v_type"));
				v.setOwner_name(rs.getString("owner_name"));
				v.setOwner_mob(rs.getString("owner_mobile"));
				v.setDriver_mob(rs.getString("driver_mobile"));
				v.setDriver_name(rs.getString("driver_name"));
			}
			return v;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return v;
	}

	public Vehicle getVehicle(String vehicle_num) {
		try (Connection conn = HibernateUtils.getConnection()) {
			Vehicle v = new Vehicle();
			String sql = "select * from vehicle where v_num = ?";
			pst = conn.prepareStatement(sql);
			pst.setString(1, vehicle_num);
			rs = pst.executeQuery();
			while (rs.next()) {
				v.setV_id(rs.getInt("v_id"));
				v.setAuthor(rs.getString("author"));
				v.setFit_form(rs.getString("fit_form"));
				v.setFit_to(rs.getString("fit_to"));
				v.setIns_from(rs.getString("ins_from"));
				v.setIns_to(rs.getString("ins_to"));
				v.setPer_form(rs.getString("per_form"));
				v.setPer_to(rs.getString("per_to"));
				v.setPollute(rs.getString("pollute"));
				v.setRd_tax(rs.getString("rd_tax"));
				v.setSer_form(rs.getString("ser_form"));
				v.setSer_to(rs.getString("ser_to"));
				v.setV_chasis(rs.getString("v_chasis"));
				v.setV_engine(rs.getString("v_engine"));
				v.setV_model(rs.getString("v_model"));
				v.setV_num(rs.getString("v_num"));
				v.setV_type(rs.getString("v_type"));
				v.setOwner_name(rs.getString("owner_name"));
				v.setOwner_mob(rs.getString("owner_mobile"));
				v.setDriver_mob(rs.getString("driver_mobile"));
				v.setDriver_name(rs.getString("driver_name"));
			}

			return v;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public BilityModel getBilty(int gr) {

		BilityModel model = new BilityModel();
		int lid = 0;
		int iid = 0;
		try (Connection conn = HibernateUtils.getConnection()) {
			String str = "SELECT * FROM bility_table where gr_id = ?";
			pst = conn.prepareStatement(str);
			pst.setInt(1, gr);
			rs = pst.executeQuery();
			while (rs.next()) {
				model.setGr_id(gr);
				model.setAdv_pay(rs.getDouble("adv_pay"));
				model.setCgst(rs.getDouble("cgst"));
				model.setBalnce(rs.getDouble("balnce"));
				model.setBank_name(rs.getString("bank_name"));
				model.setBilty_status(rs.getString("bilty_status"));
				model.setCh_dd_date(rs.getDate("ch_dd_date"));
				model.setCh_dd_no(rs.getString("ch_dd_no"));
				model.setChck_exp(rs.getDouble("chck_exp"));
				Customer consignee = new Customer();
				consignee.setAddress(rs.getString("consignee_address"));
				consignee.setCustomer_name(rs.getString("consignee_name"));
				consignee.setGst_no(rs.getString("consignee_gst"));
				model.setConsignee(consignee);
				Customer consignor = new Customer();
				consignor.setAddress(rs.getString("consignor_address"));
				consignor.setCustomer_name(rs.getString("consignor_name"));
				consignor.setGst_no(rs.getString("consignor_gst"));
				model.setConsignor(consignor);
				model.setFre_type(rs.getString("freight_type"));
				model.setFreight(rs.getDouble("freight"));
				model.setGr_date(rs.getDate("gr_date"));
				model.setGrand_total(rs.getDouble("grand_total"));
				model.setGst_paid_by(rs.getString("gst_paid_by"));
				model.setPay_mode(rs.getString("pay_mode"));
				model.setSgst(rs.getDouble("sgst"));
				model.setStat_charge(rs.getDouble("stat_charge"));
				model.setStat_from(rs.getString("station_from"));
				model.setStat_to(rs.getString("station_to"));
				model.setVeh_num(rs.getString("veh_no"));

				lid = rs.getInt("laggauge_id");
				iid = rs.getInt("insurance_in_id");

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		if (lid > 0) {
			model.setLagguage(getLagguage(lid));

		}

		if (iid > 0) {
			model.setInsurance(getInsuranceObject(iid));
		}

		return model;
	}

	public Insurance getInsuranceObject(int gr) {
		Insurance insurance = new Insurance();
		try (Connection conn = HibernateUtils.getConnection()) {
			/*
			 * CREATE TABLE insurance_table ( in_id integer, amount double precision not
			 * null, company_name varchar(100), gr_num integer not null, idate datetime,
			 * policy_num varchar(100), risk varchar(255), primary key (in_id) )
			 */
			String sql = "SELECT * from insurance_table where in_id=?";
			pst = conn.prepareStatement(sql);
			pst.setInt(1, gr);
			rs = pst.executeQuery();
			while (rs.next()) {
				insurance.setIn_id(rs.getInt("in_id"));
				insurance.setGr_num(rs.getInt("gr_num"));
				insurance.setCompany_name(rs.getString("company_name"));
				insurance.setPolicy_num(rs.getString("policy_num"));
				insurance.setIdate(rs.getDate("idate"));
				insurance.setAmount(rs.getDouble("amount"));
				insurance.setRisk(rs.getString("risk"));
			}

			return insurance;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return insurance;
	}

	public Lagguage getLagguage(int id) {
		Lagguage l = new Lagguage();
		try (Connection conn = HibernateUtils.getConnection()) {
			/*
			 * CREATE TABLE lagguage ( l_id integer, items_desc varchar(255), total_pkgs
			 * double precision, total_qunt double precision, total_wt double precision,
			 * primary key (l_id) )
			 */
			String sql = "SELECT * from lagguage where l_id=?";
			pst = conn.prepareStatement(sql);
			pst.setInt(1, id);
			rs = pst.executeQuery();
			while (rs.next()) {
				l.setL_id(id);
				l.setItems_desc(rs.getString("items_desc"));
				l.setT_packages(rs.getDouble("total_pkgs"));
				l.setT_qty(rs.getDouble("total_qunt"));
				l.setT_wt(rs.getDouble("total_wt"));
			}

			return l;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return l;
	}

	public int getMaxCount(String table) {
		try (Connection conn = HibernateUtils.getConnection()) {

			String sql = "SELECT count(*) from " + table;
			pst = conn.prepareStatement(sql);
			rs = pst.executeQuery();
			while (rs.next()) {
				int i = rs.getInt(1);
				return ++i;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	public List<BilityModel> getBilityList() {
		List<BilityModel> list = new ArrayList<>();
		try (Connection conn = HibernateUtils.getConnection()) {
			String sql = "SELECT gr_id FROM bility_table";
			pst = conn.prepareStatement(sql);
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {
				BilityModel model = getBilty(rs.getInt(1));

				/*
				 * Gson gson = new GsonBuilder().setPrettyPrinting().create();
				 * MySingleton.getInstance().sendData("bility_table", gson.toJson(model));
				 */

				list.add(model);
			}
			// MySingleton.getInstance().getDataFromRemote("bility_table");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return list;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Bill> getEntityList(String className) {

		List<Bill> list = new ArrayList<>();
		Bill b = null;
		try (Connection conn = HibernateUtils.getConnection()) {
			String sql = "SELECT bill_id FROM bill_generate";
			pst = conn.prepareStatement(sql);
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {
				list.add(getBill(rs.getInt(1)));
			}

			return list;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return list;
	}

	public List<CreateBillEntry> getBillEntry(int pk) {
		List<CreateBillEntry> list = new ArrayList<>();
		CreateBillEntry b;
		int vid = 0;
		try (Connection conn = HibernateUtils.getConnection()) {

			String sql = "SELECT * FROM bill_entries where entries_bill_id =?";
			pst = conn.prepareStatement(sql);
			// DBTablePrinter.printTable(conn, "bill_entries");

			pst.setInt(1, pk);
			ResultSet rs = pst.executeQuery();
			// DBTablePrinter.printResultSet(rs);
			int i = 0;
			while (rs.next()) {
				b = new CreateBillEntry();
				b.setS_id(rs.getInt("s_id"));
				b.setAmount(rs.getDouble("amount"));
				b.setAtt_gr(rs.getString("att_gr"));
				b.setBid_id(rs.getString("bid_id"));
				b.setBill_date(rs.getDate("bill_date"));
				b.setNug_cb(rs.getString("nug_cb"));
				b.setRate(rs.getString("rate"));
				b.setStation(rs.getString("station"));
				b.setTotal_amt(rs.getDouble("total_amt"));
				b.setWeight(rs.getString("weight"));

				vid = rs.getInt("vehicle_v_id");
				b.setVehicle(getVehicle(vid));
				list.add(b);
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return list;
	}

	public Object getObject(Class<?> classname, Serializable id) {
		/*
		 * try (Session session = HibernateUtils.getSessionFactory().openSession()) {
		 * Object obj = session.get(classname, id); if (obj != null) { return obj; } }
		 * catch (Exception e) { e.printStackTrace(); }
		 */
		return null;
	}

	public Bill getBill(int pk) {
		Bill bill = new Bill();
		/*
		 * CREATE TABLE bill_generate ( bill_id integer, bill_date datetime,
		 * customer_address varchar(255), customer_name varchar(100), customer_gst_no
		 * varchar(100), total_amt double precision, primary key (bill_id) )
		 */
		try (Connection conn = HibernateUtils.getConnection()) {
			String sql = "SELECT * FROM bill_generate WHERE bill_id = ?";
			pst = conn.prepareStatement(sql);
			pst.setInt(1, pk);
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {
				bill.setBill_id(pk);
				bill.setBill_date(rs.getDate("bill_date"));
				bill.setTotal_amt(rs.getDouble("total_amt"));
				Customer c = new Customer();
				c.setAddress(rs.getString("customer_address"));
				c.setCustomer_name(rs.getString("customer_name"));
				c.setGst_no(rs.getString("customer_gst_no"));

				bill.setClient(c);
				List<CreateBillEntry> t = getBillEntry(pk);

				bill.setEntries(t);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return bill;
	}

	public boolean deleteById(Class<?> type, Serializable id) {
		/*
		 * try (Session session = HibernateUtils.getSessionFactory().openSession()) {
		 * Object persistenceInstance = session.load(type, id); if (persistenceInstance
		 * != null) { session.beginTransaction(); session.delete(persistenceInstance);
		 * session.getTransaction().commit(); return true; }
		 * 
		 * } catch (Exception e) { e.printStackTrace(); return false; }
		 */
		return false;
	}

	public boolean deleteTable(String classname) {
		if (classname.isEmpty()) {
			return false;
		}
		/*
		 * String hql = "delete from " + classname; try (Session session =
		 * HibernateUtils.getSessionFactory().openSession()) {
		 * session.beginTransaction(); Query q = session.createQuery(hql);
		 * q.executeUpdate(); session.getTransaction().commit(); return true; } catch
		 * (Exception e) { e.printStackTrace(); return false; }
		 */
		return false;
	}

	public OwnerDetails getUserLoggedIn(String classname, String[] cols, String[] values) {
		String HQL = null;
		if (!classname.isEmpty() && cols.length > 0 && values.length > 0) {

			HQL = "SELECT *" + " from " + " owner_table" + " where " + cols[0] + " = ?" + " and " + cols[1] + " = ?";
		}
		OwnerDetails d = new OwnerDetails();
		try (Connection conn = HibernateUtils.getConnection()) {
			pst = conn.prepareStatement(HQL);
			pst.setString(1, values[0]);
			pst.setString(2, values[1]);
			rs = pst.executeQuery();
			while (rs.next()) {
				/*
				 * CREATE TABLE owner_table ( o_id integer not null, name varchar(255), password
				 * varchar(255), subscriptionFrom datetime, subscriptionTill datetime, username
				 * varchar(255), primary key (o_id) )
				 */
				d.setO_id(rs.getInt("o_id"));
				d.setName(rs.getString("name"));
				d.setPassword(rs.getString("password"));
				d.setSubscriptionFrom(rs.getDate("subscriptionFrom"));
				d.setSubscriptionTill(rs.getDate("subscriptionTill"));
				d.setUsername(rs.getString("username"));
				return d;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
