package com.javacore.transportMgmt.db;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class HibernateUtils {
	private static Connection conn = null;
	
	public static synchronized Connection getConnection() {
		try {
		
			Class.forName("org.sqlite.JDBC");
			conn = DriverManager.getConnection("jdbc:sqlite:mydb.db");
			return conn;
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return conn;
	}
	
	public static boolean netIsAvaliable() {
		String address = "http://www.google.com";
		try {
			URL url = new URL(address);
			HttpURLConnection con = (HttpURLConnection)url.openConnection();
			con.connect();
			con.getInputStream().close();
			return true;
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		return false;
	}
	

}
