package com.javacore.transportMgmt.db;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.ExecutionException;

import javax.swing.SwingWorker;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonWriter;
import com.javacore.transportMgmt.model.BilityModel;
import com.javacore.transportMgmt.model.Constants;

public class MySingleton {
	
	private static MySingleton instance;

	private MySingleton() {
		
	}
	
	public static synchronized MySingleton getInstance() {
		if(instance == null) {
			instance = new MySingleton();
		}
		return instance;
	}
	
	
	public void sendData(String tableName, final Object jsonData) {
		//String POST_URL = "http://www.morangmandi.com/android/transport/demo1.php";
		String POST_URL = "http://localhost:9999/transport/demo1.php";
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		
		if(jsonData == null)
			return;
		
		String end = "table="+tableName+"&data=";
//		System.out.println(end);System.exit(0);
		SwingWorker<String, Void> swingWorker = new SwingWorker<String, Void>() {
			@Override
			protected String doInBackground() throws Exception {
				try {
					URL url = new URL(POST_URL);
					HttpURLConnection con = (HttpURLConnection)url.openConnection();
					con.setRequestMethod("POST");
					con.setDoOutput(true);
//					final OutputStream os = con.getOutputStream();
					final OutputStream os = new BufferedOutputStream(con.getOutputStream());
					final JsonWriter writer = new JsonWriter(new OutputStreamWriter(os));
					writer.setIndent(" ");
					gson.toJson(end, String.class, writer);
					//gson.toJson(jsonData, BilityModel.class, writer);

					
					//os.write(end.getBytes());
					os.flush();
					os.close();
					
					int responseCode = con.getResponseCode();
					System.out.println("POST Response Code "+responseCode);
					if(responseCode == HttpURLConnection.HTTP_OK) {
						BufferedReader reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
						String inputLine;
						StringBuffer sb = new StringBuffer();
						while((inputLine = reader.readLine())!= null) {
							sb.append(inputLine);
						}
						reader.close();
						return sb.toString();
						
					}else {
						System.out.println("POST request not working");
					}
					
					con.disconnect();
					
				} catch (MalformedURLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}catch(Exception e) {
					System.out.println(e.getMessage());
				}
				return null;
			}
			
			@Override
			protected void done() {
				// TODO Auto-generated method stub
				super.done();
				try {
					System.out.println(get());
					EntityDao entityDao = new EntityDao();
					JsonObject jsonObject = (new JsonParser()).parse(get()).getAsJsonObject();
					JsonElement jsonElement = jsonObject.get("response");
					if(jsonElement != null) {
						String response  = jsonElement.getAsString();
						System.out.println("RESPONSE FROM SERVER: "+response);
						if(response.equalsIgnoreCase("OK")) {
							
							entityDao.storeInDatabase(jsonData, Constants.SYNC_OK);
						}else {
							entityDao.storeInDatabase(jsonData, Constants.SYNC_FAILED);
						}
					}else {
						System.out.println("RESPONSE FROM SERVER: "+null);
					}
				} catch (InterruptedException | ExecutionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		};
		swingWorker.execute();
	}
	
	public void getDataFromRemote(String tablename) {
		final String GET_URL = "http://localhost:9999/transport/demo1.php?tablename="+tablename;
		try {
			URL url = new URL(GET_URL);
			HttpURLConnection con = (HttpURLConnection)url.openConnection();
			con.setRequestMethod("GET");
			int responseCode = con.getResponseCode();
			if(responseCode == HttpURLConnection.HTTP_OK) {
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();
				
				while((inputLine = in.readLine())!= null) {
					response.append(inputLine);
				}
				
				System.out.println(response.toString());
			}
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	

}
