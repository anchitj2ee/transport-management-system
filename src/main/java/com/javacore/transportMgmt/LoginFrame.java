package com.javacore.transportMgmt;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.SwingWorker;
import javax.swing.SwingWorker.StateValue;

import java.awt.Font;
import java.awt.Toolkit;
import java.awt.Dialog.ModalityType;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Random;
import java.util.concurrent.ExecutionException;

import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.Action;
import javax.swing.BoundedRangeModel;
import javax.swing.DefaultBoundedRangeModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.border.BevelBorder;
import javax.swing.border.TitledBorder;

import com.javacore.transportMgmt.db.EntityDao;
import com.javacore.transportMgmt.model.OwnerDetails;

import javax.swing.JProgressBar;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.KeyAdapter;

public class LoginFrame extends JDialog implements KeyListener{

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField txt_username;
	private JPasswordField txt_pass;
	private LoginResponse loginResponse;
	private JButton btnlogin;

	static {
		
	}
	public interface LoginResponse {
		void onLoginSuccess();

		void onLoginFailure();
	}

	public void setInterface(Object activity) {
		this.loginResponse = (LoginResponse) activity;
	}

	/**
	 * Create the frame.
	 */
	public LoginFrame() {
		setTitle("Login");

		setBounds(100, 100, 450, 300);
		setUndecorated(true);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		setLocation((int) (dim.getWidth() / 2 - this.getWidth() / 2),
				(int) (dim.getHeight() / 2 - this.getHeight() / 2));
		setIconImage(new ImageIcon(this.getClass().getResource("/success.png")).getImage());
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBackground(Color.DARK_GRAY);
		panel.setBounds(0, 0, 450, 37);
		contentPane.add(panel);
		panel.setLayout(null);

		JLabel lblX = new JLabel("X");
		lblX.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				System.exit(0);

			}
		});
		lblX.setFont(new Font("Tahoma", Font.PLAIN, 36));
		lblX.setHorizontalAlignment(SwingConstants.CENTER);
		lblX.setForeground(new Color(250, 240, 230));
		lblX.setBounds(394, 0, 46, 37);
		panel.add(lblX);

		JLabel lblAdminLogin = new JLabel("Admin Login");
		lblAdminLogin.setFont(new Font("Lucida Sans", Font.BOLD, 25));
		lblAdminLogin.setForeground(new Color(250, 240, 230));
		lblAdminLogin.setBounds(12, 0, 283, 37);
		panel.add(lblAdminLogin);

		JLabel lblUsername = new JLabel("Username*");
		lblUsername.setFont(new Font("Arial", Font.BOLD, 15));
		lblUsername.setHorizontalAlignment(SwingConstants.CENTER);
		lblUsername.setBounds(62, 106, 93, 29);
		contentPane.add(lblUsername);

		txt_username = new JTextField();
		txt_username.setFont(new Font("Tahoma", Font.BOLD, 16));
		txt_username.setBounds(165, 106, 207, 29);
		contentPane.add(txt_username);
		txt_username.setColumns(10);

		JLabel lblPassword = new JLabel("Password*");
		lblPassword.setHorizontalAlignment(SwingConstants.CENTER);
		lblPassword.setFont(new Font("Arial", Font.BOLD, 15));
		lblPassword.setBounds(62, 151, 93, 29);
		contentPane.add(lblPassword);

		txt_pass = new JPasswordField();
		txt_pass.setBounds(165, 152, 207, 29);
		txt_pass.addKeyListener(this);
		
		contentPane.add(txt_pass);

		btnlogin = new JButton("Login");
		
		btnlogin.setMnemonic(KeyEvent.VK_ENTER);
		btnlogin.setForeground(Color.WHITE);
		btnlogin.setBackground(new Color(102, 205, 170));
		btnlogin.setBounds(165, 212, 114, 23);
		contentPane.add(btnlogin);

		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(null, "Login", TitledBorder.LEADING, TitledBorder.TOP,
				new Font("Times New Roman", Font.BOLD, 15), new Color(0, 0, 255)));
		panel_1.setBounds(40, 65, 370, 191);
		contentPane.add(panel_1);
		panel_1.setLayout(null);
		
		lblAlert = new JLabel("");
		lblAlert.setForeground(new Color(255, 69, 0));
		lblAlert.setFont(new Font("Arial", Font.BOLD, 15));
		lblAlert.setHorizontalAlignment(SwingConstants.CENTER);
		lblAlert.setBounds(40, 42, 370, 23);
		contentPane.add(lblAlert);
		
		progressBar = new JProgressBar(model);
		progressBar.setStringPainted(true);
		progressBar.setVisible(false);
		progressBar.setBounds(0, 286, 450, 14);
		contentPane.add(progressBar);
		btnlogin.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
//				loginResponse.onLoginSuccess();
//				LoginFrame.this.dispose();
				
				loginUser();
			}

			
		});

	}
	final BoundedRangeModel model = new DefaultBoundedRangeModel();
	 Random rand = new Random();
	private void loginUser() {
		if (validateForm().isEmpty()) {
			SwingWorker<OwnerDetails, Void> swingWorker = new SwingWorker<OwnerDetails, Void>() {
				
				@Override
				protected OwnerDetails doInBackground() throws Exception {
					// TODO Auto-generated method stub
					setProgress(rand.nextInt(100));
					OwnerDetails userLoggedIn = entityDao.getUserLoggedIn("OwnerDetails", new String[] {"username" ,"password"},
							new String[] {txt_username.getText(),txt_pass.getText()});
					
					return userLoggedIn;
				}
				
				@Override
				protected void done() {
					// TODO Auto-generated method stub
					OwnerDetails userLoggedIn;
					try {
						userLoggedIn = get();
//						System.out.println(userLoggedIn);
						setProgress(100);
						if (userLoggedIn != null) {
							loginResponse.onLoginSuccess();
//							LoginFrame.this.dispose();
						}else {
							lblAlert.setText("Login Error");
							loginResponse.onLoginFailure();
						}
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (ExecutionException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}
			};
			swingWorker.addPropertyChangeListener(new PropertyChangeListener() {
				
				@Override
				public void propertyChange(PropertyChangeEvent evt) {
					// TODO Auto-generated method stub
					switch(evt.getPropertyName()) {
					case "progress":
						progressBar.setIndeterminate(false);
						model.setValue((Integer) evt.getNewValue());
						break;
					case "state":
						switch((StateValue) evt.getNewValue()) {
						case DONE:
							progressBar.setVisible(false);
							break;
						case STARTED:
				        case PENDING:
				            progressBar.setVisible(true);
				            progressBar.setIndeterminate(true);
				            break;
						}
						break;
					
					}
				}
			});
			swingWorker.execute();
		}else {
			lblAlert.setText(validateForm());
		}
		
		
		
	}
	
	private EntityDao entityDao = new EntityDao();
	private JLabel lblAlert;
	private JProgressBar progressBar;
	
	private String validateForm() {
		StringBuilder err = new StringBuilder();
		if (txt_username.getText().isEmpty()) {
			err.append("Please Enter Correct Username & Password.");
		}
		if (txt_pass.getText().isEmpty()) {
			err.append("Please Enter Correct Username & Password.");
		}
		return err.toString();
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	
	
	
	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		
		if (e.getKeyCode() == KeyEvent.VK_ENTER) {
			loginUser();
		}
		if (e.getSource()==txt_pass) {
//			System.out.println("dhgwudgwfduygwe");
//			System.out.println(e.getKeyChar());
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
}
