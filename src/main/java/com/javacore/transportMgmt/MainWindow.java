package com.javacore.transportMgmt;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import javax.swing.Box;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.JToolBar;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.border.MatteBorder;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.text.AbstractDocument;
import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

import com.javacore.transportMgmt.DispatchChallan.BilityListener;
import com.javacore.transportMgmt.LoginFrame.LoginResponse;
import com.javacore.transportMgmt.VehicleEntry.VehicleListener;
import com.javacore.transportMgmt.db.EntityDao;
import com.javacore.transportMgmt.model.BilityModel;
import com.javacore.transportMgmt.model.Constants;
import com.javacore.transportMgmt.model.Lagguage;
import com.javacore.transportMgmt.model.Vehicle;
import com.javacore.transportMgmt.utility.DocumentSizeFilter;
import com.javacore.transportMgmt.utility.PdfModel;

public class MainWindow extends JFrame implements ActionListener, BilityListener, VehicleListener, LoginResponse {

	private JPanel contentPane;
	private JTable table;
	private JButton btnGenerateBill;
	private JButton btnBilitySection;
	private JButton btnVehicleMgm;
	private String[] cols = { "GR.NO", "From Station", "To Station", "Vehicle", "Weight", "Weight Charged", "Advance",
			"Balance" };
	private DefaultTableModel tableModel;
	private EntityDao entityDao = new EntityDao();
	private static final Color receive = Color.GREEN;
	private static final Color pending = Color.YELLOW;

	/**
	 * Create the frame.
	 */
	public MainWindow() {
		setTitle(
				"Transport Management System                                              powered by Onistech Info Systems");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setIconImage(new ImageIcon(this.getClass().getResource("/success.png")).getImage());
		setBounds(100, 100, 1006, 615);
		setExtendedState(MAXIMIZED_BOTH);
		contentPane = new JPanel();
		contentPane.setBackground(SystemColor.control);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		panel_6 = new JPanel();
		panel_6.setBackground(Color.BLUE);

		JPanel panel_2 = new JPanel();

		JPanel panel_3 = new JPanel();
		panel_3.setBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(0, 0, 0)));
		panel_3.setBackground(Color.WHITE);

		JToolBar toolBar = new JToolBar();
		toolBar.setFloatable(false);
		toolBar.setBackground(SystemColor.control);

		JPanel panel_4 = new JPanel();
		panel_4.setBackground(Color.WHITE);
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addComponent(panel_6, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 980, Short.MAX_VALUE)
				.addComponent(panel_3, GroupLayout.DEFAULT_SIZE, 980, Short.MAX_VALUE)
				.addGroup(gl_contentPane.createSequentialGroup()
						.addComponent(panel_4, GroupLayout.PREFERRED_SIZE, 208, GroupLayout.PREFERRED_SIZE).addGap(2)
						.addComponent(panel_2, GroupLayout.DEFAULT_SIZE, 770, Short.MAX_VALUE))
				.addComponent(toolBar, GroupLayout.DEFAULT_SIZE, 980, Short.MAX_VALUE));
		gl_contentPane
				.setVerticalGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING).addGroup(Alignment.LEADING,
						gl_contentPane.createSequentialGroup()
								.addComponent(panel_6, GroupLayout.PREFERRED_SIZE, 43, GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(toolBar, GroupLayout.PREFERRED_SIZE, 43, GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED)
								.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
										.addComponent(panel_2, GroupLayout.DEFAULT_SIZE, 447, Short.MAX_VALUE)
										.addComponent(panel_4, GroupLayout.DEFAULT_SIZE, 445, Short.MAX_VALUE))
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(panel_3, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)));

		upcomingPanel(panel_4);

		textArea = new JTextPane();
		textArea.setEditable(false);
		textArea.setMargin(new Insets(0, 5, 0, 5));

		StyledDocument styledDoc = textArea.getStyledDocument();
		if (styledDoc instanceof AbstractDocument) {
			doc = (AbstractDocument) styledDoc;
			doc.setDocumentFilter(new DocumentSizeFilter(3000));
		} else {
			System.err.println("Text pane's document isn't an AbstractDocument");

		}

		textArea.setBackground(Color.BLUE);
		textScrollPane = new JScrollPane(textArea);
		textScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		textArea.setForeground(Color.WHITE);
		textArea.setFont(new Font("Arial", Font.BOLD, 17));
		GroupLayout gl_panel_4 = new GroupLayout(panel_4);
		gl_panel_4.setHorizontalGroup(gl_panel_4.createParallelGroup(Alignment.LEADING)
				.addComponent(panel_5, GroupLayout.DEFAULT_SIZE, 208, Short.MAX_VALUE)
				.addComponent(textScrollPane, GroupLayout.DEFAULT_SIZE, 208, Short.MAX_VALUE));
		gl_panel_4.setVerticalGroup(gl_panel_4.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_4.createSequentialGroup()
						.addComponent(panel_5, GroupLayout.PREFERRED_SIZE, 29, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(textScrollPane, GroupLayout.DEFAULT_SIZE, 396, Short.MAX_VALUE)
						.addContainerGap()));
		panel_4.setLayout(gl_panel_4);

		initViews(panel_6, panel_2, panel_3, toolBar);

		Component horizontalStrut = Box.createHorizontalStrut(20);
		toolBar.add(horizontalStrut);

		btnExportToExcel = new JButton("Export To Excel");
		btnExportToExcel.setFocusable(false);
		btnExportToExcel.addActionListener(this);
		btnExportToExcel.setIcon(new ImageIcon(this.getClass().getResource("/excel.png")));
		btnExportToExcel.setForeground(Color.WHITE);
		btnExportToExcel.setFont(new Font("Times New Roman", Font.BOLD, 15));
		btnExportToExcel.setBackground(Color.BLUE);
		toolBar.add(btnExportToExcel);

		footer(panel_3);

		JLabel label = new JLabel("");
		label.setFont(new Font("Dialog", Font.PLAIN, 12));
		panel_3.add(label);

		JLabel label_1 = new JLabel("");
		label_1.setFont(new Font("Dialog", Font.PLAIN, 12));
		panel_3.add(label_1);

		lblPending = new JLabel(pen + " Pending");
		lblPending.setHorizontalAlignment(SwingConstants.CENTER);
		lblPending.setOpaque(true);
		lblPending.setFont(new Font("Times New Roman", Font.BOLD, 14));
		panel_3.add(lblPending);

		lblReceive = new JLabel(rec + " Receive");
		lblReceive.setOpaque(true);
		lblReceive.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblReceive.setHorizontalAlignment(SwingConstants.CENTER);
		lblPending.setBackground(pending);
		lblReceive.setBackground(receive);

		panel_3.add(lblReceive);
		contentPane.setLayout(gl_contentPane);
		setDefaultData();
	}

	int rec = 0, pen = 0;
	JScrollPane textScrollPane;

	private void footer(JPanel panel_3) {
		panel_3.setLayout(new GridLayout(0, 10, 0, 0));

		JLabel lblNewLabel = new JLabel("Developed By:");
		lblNewLabel.setFont(new Font("Times New Roman", Font.BOLD, 14));
		panel_3.add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("Onistech Info Systems");
		lblNewLabel_1.setFont(new Font("Dialog", Font.PLAIN, 12));
		panel_3.add(lblNewLabel_1);

		JLabel lblNewLabel_2 = new JLabel("");
		lblNewLabel_2.setFont(new Font("Times New Roman", Font.BOLD, 14));
		panel_3.add(lblNewLabel_2);

		JLabel label = new JLabel("");
		label.setFont(new Font("Dialog", Font.PLAIN, 12));
		panel_3.add(label);

		JLabel lblProductVersion = new JLabel("Product Version ");
		lblProductVersion.setFont(new Font("Times New Roman", Font.BOLD, 14));
		panel_3.add(lblProductVersion);

		JLabel lblTrial = new JLabel("Trial");
		lblTrial.setFont(new Font("Dialog", Font.PLAIN, 12));
		panel_3.add(lblTrial);
	}

	private void upcomingPanel(JPanel panel_4) {
		panel_5 = new JPanel();
		panel_5.setBackground(Color.DARK_GRAY);
		panel_5.setLayout(null);

		JLabel lblNewLabel_3 = new JLabel("Upcoming Events");
		lblNewLabel_3.setForeground(Color.WHITE);
		lblNewLabel_3.setBounds(0, 0, 208, 29);
		panel_5.add(lblNewLabel_3);
		lblNewLabel_3.setFont(new Font("Times New Roman", Font.BOLD, 20));
		lblNewLabel_3.setHorizontalAlignment(SwingConstants.CENTER);
	}

	private void initViews(JPanel panel, JPanel panel_2, JPanel panel_3, JToolBar toolBar) {
		btnBilitySection = new JButton("Bility Section");
		btnBilitySection.setFocusable(false);
		btnBilitySection.setIcon(new ImageIcon(this.getClass().getResource("/invoice.png")));
		btnBilitySection.addActionListener(this);
		btnBilitySection.setFont(new Font("Times New Roman", Font.BOLD, 15));
		btnBilitySection.setForeground(Color.WHITE);
		btnBilitySection.setBackground(Color.BLUE);
		toolBar.add(btnBilitySection);

		Component horizontalStrut = Box.createHorizontalStrut(20);
		toolBar.add(horizontalStrut);

		btnGenerateBill = new JButton("Generate Bill");
		btnGenerateBill.setFocusable(false);
		btnGenerateBill.setIcon(new ImageIcon(this.getClass().getResource("/invoice.png")));
		btnGenerateBill.addActionListener(this);
		btnGenerateBill.setFont(new Font("Times New Roman", Font.BOLD, 15));
		btnGenerateBill.setForeground(Color.WHITE);
		btnGenerateBill.setBackground(Color.BLUE);
		toolBar.add(btnGenerateBill);

		Component horizontalStrut_1 = Box.createHorizontalStrut(20);
		toolBar.add(horizontalStrut_1);

		btnVehicleMgm = new JButton("Vehicle Mgm.");
		btnVehicleMgm.setFocusable(false);
		btnVehicleMgm.addActionListener(this);
		btnVehicleMgm.setIcon(new ImageIcon(this.getClass().getResource("/truck24.png")));
		btnVehicleMgm.setFont(new Font("Times New Roman", Font.BOLD, 15));
		btnVehicleMgm.setForeground(Color.WHITE);
		btnVehicleMgm.setBackground(Color.BLUE);
		toolBar.add(btnVehicleMgm);
		panel_2.setLayout(new BorderLayout(0, 0));

		setTableData(panel_2);
		panel.setLayout(new GridLayout(0, 4, 0, 0));

		JLabel label = new JLabel("");
		panel.add(label);

		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.BLUE);
		panel.add(panel_1);
		panel_1.setLayout(null);

		JLabel lblDemoCompany = new JLabel("Chauhan Freight Carriers");
		lblDemoCompany.setForeground(Color.WHITE);
		lblDemoCompany.setFont(new Font("Times New Roman", Font.BOLD, 22));
		lblDemoCompany.setHorizontalAlignment(SwingConstants.CENTER);
		lblDemoCompany.setBounds(0, 0, 298, 43);
		panel_1.add(lblDemoCompany);

		JLabel lblUser = new JLabel("Welcome");
		lblUser.setFont(new Font("Arial", Font.BOLD, 15));
		lblUser.setIcon(new ImageIcon(this.getClass().getResource("/user.png")));
		lblUser.setHorizontalAlignment(SwingConstants.CENTER);
		lblUser.setForeground(Color.WHITE);
		panel.add(lblUser);

		JPanel panel_4 = new JPanel();
		panel_4.setBackground(Color.BLUE);
		panel_6.add(panel_4);
		panel_4.setLayout(new GridLayout(0, 2, 0, 0));

		btnLogout = new JButton("Logout");
		btnLogout.addActionListener(this);
		panel_4.add(btnLogout);
		btnLogout.setForeground(Color.WHITE);
		btnLogout.setFocusable(false);
		btnLogout.setIcon(new ImageIcon(this.getClass().getResource("/exit24.png")));
		btnLogout.setFont(new Font("Arial", Font.BOLD, 12));
		btnLogout.setBackground(Color.BLUE);
	}

	List<BilityModel> list = new ArrayList<>();

	private void setTableData(JPanel panel_2) {
		JScrollPane scrollPane = new JScrollPane();
		panel_2.add(scrollPane, BorderLayout.CENTER);

		tableModel = new DefaultTableModel() {
			public Class getColumnClass(int column) {
				try {
					return getValueAt(0, column).getClass();	
				} catch (Exception e) {
					// TODO: handle exception
					return "".getClass();
				}
			}

			@Override
			public boolean isCellEditable(int row, int column) {
				// TODO Auto-generated method stub
				return false;
			}
		};
		tableModel.setColumnIdentifiers(cols);
		// table.setModel(tableModel);

		list = entityDao.getBilityList();
		for (BilityModel model : list) {
			setTableRow(model);
		}
		table = new JTable(tableModel) {
			/**
			 * 
			 */
			private static final long serialVersionUID = 6165737754546939779L;

			@Override
			public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
				// TODO Auto-generated method stub
				Component c = super.prepareRenderer(renderer, row, column);
				if (!isRowSelected(row)) {
					c.setBackground(getBackground());
					int modelRow = convertRowIndexToModel(row);
					BilityModel bilityModel = list.get(modelRow);
					String type = bilityModel.getBilty_status();
					if (type.equalsIgnoreCase(Constants.stat_receive)) {
						c.setBackground(receive);

						/*
						 * c.setBackground(new Color(151, 207, 48)); c.setForeground(new Color(210, 90,
						 * 92));
						 */
					} else {
						c.setBackground(pending);

						/*
						 * setBackground(new Color(210, 90, 92)); setForeground(Color.WHITE);
						 */
					}

				}
				return c;
			}
		};
		JTableHeader h1 = table.getTableHeader();
		scrollPane.setViewportView(table);
		table.setFont(new Font("Arial", Font.PLAIN, 14));
		h1.setFont(new Font("Arial", Font.BOLD, 14));
		table.setRowHeight(20);

		table.getColumnModel().getColumn(0).setMaxWidth(60);

		// table.setOpaque(false);
		panel_2.add(BorderLayout.NORTH, h1);
		markreceive = new JMenuItem("Mark Receive");
		seedetails = new JMenuItem("Details");
		markreceive.addActionListener(this);
		seedetails.addActionListener(this);
		popupmenu.add(markreceive);
		popupmenu.add(seedetails);
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				super.mouseClicked(e);
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				super.mouseReleased(e);
				if (e.isPopupTrigger()) {
					popupmenu.show(table, e.getX(), e.getY());
				}
			}
		});
		setAdapter(popupmenu, table);

	}

	private void updateCounters() {
		rec = 0;
		pen = 0;
		for (BilityModel obj : list) {
			if (obj.getBilty_status().equalsIgnoreCase(Constants.stat_receive)) {
				rec++;
			} else {
				pen++;
			}
		}
		lblPending.setText(pen + " Pending");
		lblReceive.setText(rec + " Receive");
	}

	LoginFrame jf = new LoginFrame();

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == btnLogout) {
			MainWindow.this.dispose();
		}
		if (e.getSource() == btnExportToExcel) {
			new Thread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					// PdfModel.exportExcel("BILITY LIST", tableModel);
					new Thread(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub
							PdfModel.exportExcel("BILITY LIST", tableModel);
						}
					}).start();

				}
			}).start();
		}

		if (e.getSource() == btnVehicleMgm) {
			new Thread(new Runnable() {
				public void run() {
					try {

						VehicleEntry veh_frame = new VehicleEntry();
						veh_frame.setVehicleListener(MainWindow.this);
						veh_frame.setVisible(true);
						
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}).start();
			;
		}

		if (e.getSource() == btnBilitySection) {
			openBilityFrame();
		}

		if (e.getSource() == btnGenerateBill) {
			new Thread(new Runnable() {
				public void run() {
					try {

						Dashboard dash_frame = new Dashboard();
						dash_frame.setVisible(true);
						
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}).start();
		}

		if (e.getSource() == markreceive) {
			list.get(table.getSelectedRow()).setBilty_status(Constants.stat_receive);

			int gr = (Integer) table.getValueAt(table.getSelectedRow(), 0);
			// System.out.println("GR NO " + gr);
			if (gr > 0) {
				entityDao.updateObject("bility_table", new String[] { "bilty_status" },
						new String[] { Constants.stat_receive }, " where gr_id=" + gr);
			}
			tableModel.fireTableDataChanged();
			updateCounters();
		}

		if (e.getSource() == seedetails) {
			int gr = (Integer) table.getValueAt(table.getSelectedRow(), 0);
			if (gr > 0) {
				// System.out.println("GR=" + gr);
				new Thread(new Runnable() {

					@Override
					public void run() {

						DispatchChallan cha_frame = new DispatchChallan();
						cha_frame.setBilityListener(MainWindow.this);
						cha_frame.searchGR(gr);
						cha_frame.setVisible(true);

					}
				}).start();

			} else {
				System.out.println("something went wrong here GR=" + gr);
			}

		}

	}

	private void flushDatabase() {

	}

	private void openBilityFrame() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {

					DispatchChallan cha_frame = new DispatchChallan();
					cha_frame.setBilityListener(MainWindow.this);
					cha_frame.setVisible(true);
					if (cha_frame.isDisplayable()) {

						cha_frame.setState(java.awt.Frame.NORMAL);
						cha_frame.toFront();

					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	@Override
	public void onNewEntry(BilityModel bilityModel) {
		list.add(bilityModel);
		// System.out.println("SIZE OF LIST " + list.size());
		setTableRow(0, bilityModel);
		updateCounters();
	}

	@Override
	public void onUpdateEntry(BilityModel bilityModel) {
		// System.out.println(bilityModel);
		if (bilityModel != null) {

			int row = bilityModel.getGr_id();
			row--;

			updatedObject(row, bilityModel);
			tableModel.setValueAt(bilityModel.getStat_from(), row, 1);
			tableModel.setValueAt(bilityModel.getStat_to(), row, 2);
			Lagguage l = bilityModel.getLagguage();
			tableModel.setValueAt(l.getT_packages(), row, 3);
			tableModel.setValueAt(l.getT_qty(), row, 4);
			tableModel.setValueAt(l.getT_wt(), row, 5);
			tableModel.setValueAt(bilityModel.getAdv_pay(), row, 6);
			tableModel.setValueAt(bilityModel.getFreight(), row, 7);
			tableModel.fireTableDataChanged();
		}
	}

	private void updatedObject(int row, BilityModel model) {
		list.get(row).setStat_from(model.getStat_from());
		list.get(row).setStat_to(model.getStat_to());
		Lagguage l = model.getLagguage();
		list.get(row).setLagguage(l);
		list.get(row).setAdv_pay(model.getAdv_pay());
		list.get(row).setFreight(model.getFreight());
		list.get(row).setBilty_status(model.getBilty_status());

	}

	private void setTableRow(int position, BilityModel model) {
		// {"GR.NO","From Station","To
		// Station","Packages","Quantity","Weight","Advance","Hire Amount"};

		if (model != null) {
			Vector<Object> row = getRow(model);
			tableModel.insertRow(position, row);
		}
	}

	private void setTableRow(BilityModel model) {
		// {"GR.NO","From Station","To
		// Station","Packages","Quantity","Weight","Advance","Hire Amount"};

		if (model != null) {
			Vector<Object> row = getRow(model);
			tableModel.addRow(row);
		}
	}

	final JPopupMenu popupmenu = new JPopupMenu("Edit");
	private JMenuItem markreceive, seedetails;

	private void setAdapter(final JPopupMenu popupmenu, final JTable table) {

		popupmenu.addPopupMenuListener(new PopupMenuListener() {

			@Override
			public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
				// TODO Auto-generated method stub
				SwingUtilities.invokeLater(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						int rowAtPoint = table
								.rowAtPoint(SwingUtilities.convertPoint(popupmenu, new Point(0, 0), table));
						if (rowAtPoint > -1) {
							table.setRowSelectionInterval(rowAtPoint, rowAtPoint);
						}
					}
				});
			}

			@Override
			public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void popupMenuCanceled(PopupMenuEvent e) {
				// TODO Auto-generated method stub

			}
		});
		/*
		 * SwingUtilities.invokeLater(new Runnable() {
		 * 
		 * @Override public void run() { // TODO Auto-generated method stub int
		 * rowAtPoint = table_recent .rowAtPoint(SwingUtilities.convertPoint(popupmenu,
		 * new Point(0, 0), table)); if (rowAtPoint > -1) {
		 * table_recent.setRowSelectionInterval(rowAtPoint, rowAtPoint); } } });
		 */
	}

	private Vector<Object> getRow(BilityModel model) {
		Vector<Object> row = new Vector<>();
		row.add(model.getGr_id());
		row.add(model.getStat_from());
		row.add(model.getStat_to());
		row.add(model.getVeh_num());
		Lagguage l = model.getLagguage();
		if (l != null) {

			row.add(l.getT_qty());
			row.add(l.getT_wt());
		} else {
			row.add("");
			row.add("");
			row.add("");
		}

		row.add(model.getAdv_pay());
		row.add(model.getBalnce());
		return row;
	}

	private void setDefaultData() {
		setEventTable();
		updateCounters();
	}

	private Date date = new Date();
	private DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
	private JPanel panel_5;
	private JTextPane textArea;
	AbstractDocument doc;
	private JButton btnExportToExcel;
	private JLabel lblPending;
	private JLabel lblReceive;
	private JPanel panel_6;
	private JButton btnLogout;

	private void setEventTable() {
		List<Vehicle> vehicles = entityDao.getVehicles();
		for (Vehicle vehicle : vehicles) {

			try {
				if (vehicle.getIns_to() != null) {
					long i = numOfDays(vehicle.getIns_to());
					makePrint(i);
					if (i < 15) {
						if (i > 0)
							doc.insertString(doc.getLength(),
									vehicle.getV_num() + "\n" + Constants.ins_exp + "\n" + vehicle.getIns_to() + "\n\n",
									getAttrSet(vehicle.getIns_to()));
						else
							doc.insertString(doc.getLength(), vehicle.getV_num() + "\n" + Constants.ins_exp_0 + "\n"
									+ vehicle.getIns_to() + "\n\n", getAttrSet(vehicle.getIns_to()));
					}
				}
				if (vehicle.getFit_to() != null) {
					long f = numOfDays(vehicle.getFit_to());
					makePrint(f);
					if (f < 15) {
						if (f > 0)
							doc.insertString(doc.getLength(),
									vehicle.getV_num() + "\n" + Constants.fit_exp + "\n" + vehicle.getFit_to() + "\n\n",
									getAttrSet(vehicle.getFit_to()));
						else
							doc.insertString(doc.getLength(), vehicle.getV_num() + "\n" + Constants.fit_exp_0 + "\n"
									+ vehicle.getFit_to() + "\n\n", getAttrSet(vehicle.getFit_to()));
					}
				}
				if (vehicle.getPer_to() != null) {
					long p = numOfDays(vehicle.getPer_to());
					makePrint(p);
					if (p < 15) {
						if (p > 0)
							doc.insertString(doc.getLength(),
									vehicle.getV_num() + "\n" + Constants.permit + "\n" + vehicle.getPer_to() + "\n\n",
									getAttrSet(vehicle.getPer_to()));
						else
							doc.insertString(doc.getLength(), vehicle.getV_num() + "\n" + Constants.permit_0 + "\n"
									+ vehicle.getPer_to() + "\n\n", getAttrSet(vehicle.getPer_to()));
					}
				}

				if (vehicle.getAuthor() != null) {
					long a = numOfDays(vehicle.getAuthor());
					makePrint(a);
					if (a < 15) {
						if (a > 0)
							doc.insertString(doc.getLength(),
									vehicle.getV_num() + "\n" + Constants.auth + "\n" + vehicle.getAuthor() + "\n\n",
									getAttrSet(vehicle.getAuthor()));
						else
							doc.insertString(doc.getLength(),
									vehicle.getV_num() + "\n" + Constants.auth_0 + "\n" + vehicle.getAuthor() + "\n\n",
									getAttrSet(vehicle.getAuthor()));
					}
				}
				if (vehicle.getSer_to() != null) {
					if (!vehicle.getSer_to().isEmpty()) {
						doc.insertString(doc.getLength(),
								vehicle.getV_num() + "\n" + Constants.service + "\n" + vehicle.getSer_to() + "\n\n",
								null);
					}
				}

				if (vehicle.getRd_tax() != null) {
					long r = numOfDays(vehicle.getRd_tax());
					if (r < 15) {
						if (r > 0)
							doc.insertString(doc.getLength(),
									vehicle.getV_num() + "\n" + Constants.rd_tax + "\n" + vehicle.getRd_tax() + "\n\n",
									getAttrSet(vehicle.getRd_tax()));
						else
							doc.insertString(doc.getLength(), vehicle.getV_num() + "\n" + Constants.rd_tax_0 + "\n"
									+ vehicle.getRd_tax() + "\n\n", getAttrSet(vehicle.getRd_tax()));
					}
				}
				if (vehicle.getPollute() != null) {
					long po = numOfDays(vehicle.getPollute());
					if (po < 15) {
						if (po > 0)
							doc.insertString(doc.getLength(), vehicle.getV_num() + "\n" + Constants.pollute + "\n"
									+ vehicle.getPollute() + "\n\n", getAttrSet(vehicle.getPollute()));
						else
							doc.insertString(doc.getLength(), vehicle.getV_num() + "\n" + Constants.pollute_0 + "\n"
									+ vehicle.getPollute() + "\n\n", getAttrSet(vehicle.getPollute()));
					}
				}

			} catch (BadLocationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// textArea.append(vehicle.getV_num()+"\n"+Constants.ins_exp+"\n"+vehicle.getIns_to()+"\n\n");

			;
			// textArea.append();

			;

			// textArea.append();

			// service not goes for isAlertVehicle //always includes for all vehicles
			// textArea.append(vehicle.getV_num()+"\n"+Constants.service+"\n"+vehicle.getSer_to()+"\n\n");
			;
			// textArea.append();

			;
			// textArea.append(vehicle.getV_num()+"\n"+Constants.rd_tax+"\n"+vehicle.getRd_tax()+"\n\n");

			;
			// textArea.append();

		}
	}

	private <T> void makePrint(T obj) {
		System.out.println(obj);
	}

	private SimpleAttributeSet getAttrSet(String dateTo) {

		long noOfDays = numOfDays(dateTo);
		// System.out.println("noOfDays:" + dateTo + " : " + noOfDays);
		SimpleAttributeSet attrs = new SimpleAttributeSet();
		StyleConstants.setFontFamily(attrs, "Arial");
		StyleConstants.setBold(attrs, true);
		if (noOfDays < 0) {
			StyleConstants.setForeground(attrs, Color.RED);
		} else {
			if (noOfDays < 15) {
				StyleConstants.setForeground(attrs, Color.CYAN);
			} else {
				StyleConstants.setForeground(attrs, Color.WHITE);
			}
		}
		return attrs;
	}

	private long numOfDays(String dateTo) {
		if (dateTo != null && !dateTo.isEmpty()) {
			LocalDate dateToday = LocalDate.parse(dateFormat.format(this.date), formatter);
			LocalDate dateFrom = LocalDate.parse(dateTo, formatter);
			long noOfDays = ChronoUnit.DAYS.between(dateToday, dateFrom);
			return noOfDays;
		} else {
			return 0;
		}

	}

	@Override
	public void onNewVehicle(Vehicle vehicle) {
		setEventTable();
	}

	@Override
	public void onUpdateVehicle(Vehicle vehicle) {
		setEventTable();
	}

	@Override
	public void onDeleteVehicle(String vehicle_num) {
		setEventTable();
	}

	@Override
	public void onDeleteAll() {
		setEventTable();
	}

	@Override
	public void onLoginSuccess() {
		// TODO Auto-generated method stub
		flushDatabase();
	}

	@Override
	public void onLoginFailure() {
		// TODO Auto-generated method stub
		showMessageDialog(1, "Not Authorized ");
	}

	private void showMessageDialog(int count, String message) {
		switch (count) {

		case 1:
			JOptionPane.showMessageDialog(this, message, "Alert", JOptionPane.WARNING_MESSAGE);
			break;
		default:
			JOptionPane.showMessageDialog(this, message);
		}

	}
}
