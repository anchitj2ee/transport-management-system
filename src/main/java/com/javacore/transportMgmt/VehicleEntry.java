package com.javacore.transportMgmt;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.JTableHeader;

import java.awt.GridLayout;
import java.awt.Dialog.ModalityType;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JToolBar;
import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.Font;
import java.awt.Color;
import javax.swing.SwingConstants;

import com.javacore.transportMgmt.LoginFrame.LoginResponse;
import com.javacore.transportMgmt.db.EntityDao;
import com.javacore.transportMgmt.model.Constants;
import com.javacore.transportMgmt.model.Vehicle;
import com.javacore.transportMgmt.utility.MyTableModel;
import com.javacore.transportMgmt.utility.RowData;
import com.toedter.calendar.JDateChooser;
import javax.swing.JScrollPane;
import javax.swing.JTable;

public class VehicleEntry extends JFrame implements ActionListener,LoginResponse{

	/**
	 * 
	 */
	private static final long serialVersionUID = -705145382525914728L;
	private JPanel contentPane;
	private JTextField txt_vid;
	private JTextField txt_vno;
	private JTextField txt_vtype;
	private JTextField txt_vmodel;
	private JTextField txt_engno;
	private JTextField txt_chasis;
	private JTable table;
	private String[] cols = {"V.ID","V.No.","Owner","Type","Model","Ins.","Fit.","Permit","Service","Eng. No.","Chasis"};
	private MyTableModel veh_model; 
	private JButton btnSave;
	private JButton btnNew;
	private JButton btnDelete;
	private JDateChooser dcInsFrom;
	private JDateChooser dcInsTo;
	private EntityDao entityDao;
	private VehicleListener vehicleListener;
	
	
	public interface VehicleListener{
		public abstract void onNewVehicle(Vehicle vehicle);
		public abstract void onUpdateVehicle(Vehicle vehicle);
		public abstract void onDeleteVehicle(String vehicle_num);
		public abstract void onDeleteAll();
		
	}
	
	public void setVehicleListener(Object app) {
		this.vehicleListener = (VehicleListener)app;
	}
	

	public  VehicleEntry() {
		setTitle("Vehicle Management");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setIconImage(new ImageIcon(this.getClass().getResource("/success.png")).getImage());
		setBounds(100, 100, 884, 578);
		//setExtendedState(MAXIMIZED_BOTH);
		
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		entityDao = new EntityDao();
		toolBar_1 = new JToolBar();
		toolBar_1.setFloatable(false);
		
		
		panel_3 = new JPanel();
		panel_3.setBackground(Color.WHITE);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.WHITE);
		
		JPanel panel_2 = new JPanel();
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addComponent(toolBar_1, GroupLayout.DEFAULT_SIZE, 858, Short.MAX_VALUE)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addComponent(panel_3, GroupLayout.DEFAULT_SIZE, 382, Short.MAX_VALUE)
					.addGap(18)
					.addComponent(panel_1, GroupLayout.DEFAULT_SIZE, 448, Short.MAX_VALUE)
					.addContainerGap())
				.addComponent(panel_2, GroupLayout.DEFAULT_SIZE, 858, Short.MAX_VALUE)
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addComponent(toolBar_1, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(panel_1, GroupLayout.PREFERRED_SIZE, 241, GroupLayout.PREFERRED_SIZE)
						.addComponent(panel_3, GroupLayout.DEFAULT_SIZE, 307, Short.MAX_VALUE))
					.addGap(18)
					.addComponent(panel_2, GroupLayout.PREFERRED_SIZE, 167, GroupLayout.PREFERRED_SIZE))
		);
		panel_2.setLayout(new BorderLayout(0, 0));
		
		setTableData(panel_2);
		panel_1.setLayout(new GridLayout(6, 4, 0, 15));
		
		initViews(toolBar_1, panel_3, panel_1, gl_contentPane);
		
		
	}

	private void setTableData(JPanel panel_2) {
		JScrollPane scrollPane = new JScrollPane();
		panel_2.add(scrollPane, BorderLayout.CENTER);
		
		table = new JTable();
		veh_model = new MyTableModel();
		veh_model.setColumnNames(cols);
		table.setModel(veh_model);
		JTableHeader h1 = table.getTableHeader();
		scrollPane.setViewportView(table);
		table.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		h1.setFont(new Font("Times New Roman", Font.BOLD, 14));
		table.setRowHeight(20);
		table.getSelectionModel().addListSelectionListener(new RowListener());
		panel_2.add(BorderLayout.NORTH, h1);
		
		populateData();
	}

	private void populateData() {
		// "V.ID","V.No.","Owner","Type","Model","Ins.","Fit.","Permit","Service","Eng. No.","Chasis"
		
		List<Vehicle> vehicles = entityDao.getVehicles();
	
		for(Vehicle v:vehicles) {
			setTableRow(v);
		}
	}

	private void setTableRow(Vehicle v) {
		Map<String, Object> row;
		row = new HashMap<>();
		row.put(cols[0], v.getV_id());
		row.put(cols[1], v.getV_num());
		row.put(cols[2], v.getOwner_name());
		row.put(cols[3], v.getV_type());
		row.put(cols[4], v.getV_model());
		row.put(cols[5], v.getIns_to());
		row.put(cols[6], v.getFit_to());
		row.put(cols[7], v.getPer_to());
		row.put(cols[8], v.getSer_to());
		row.put(cols[9], v.getV_engine());
		row.put(cols[10], v.getV_chasis());
		veh_model.addRow(new RowData(row));
	}

	private class RowListener implements ListSelectionListener{

		@Override
		public void valueChanged(ListSelectionEvent e) {
			// TODO Auto-generated method stub
			if (e.getValueIsAdjusting()) {
				return;
			}
			if (table.getSelectedRow()==-1) {
				return ;
			}
			clearFormData();
			int id = (Integer)veh_model.getValueAt(table.getSelectedRow(), 0);
			Vehicle veh = entityDao.getVehicle(id);
			setFormData(veh);
		}
		
		
		
	}
	private void initViews(JToolBar toolBar, JPanel panel, JPanel panel_1, GroupLayout gl_contentPane) {
		JLabel lblNewLabel_6 = new JLabel("Insurance");
		lblNewLabel_6.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_6.setFont(new Font("Times New Roman", Font.BOLD, 15));
		panel_1.add(lblNewLabel_6);
		
		dcInsFrom = new JDateChooser();
		dcInsFrom.setDateFormatString("dd-MM-yyyy");
		panel_1.add(dcInsFrom);
		
		JLabel lblTo = new JLabel("To");
		lblTo.setHorizontalAlignment(SwingConstants.CENTER);
		lblTo.setFont(new Font("Times New Roman", Font.BOLD, 15));
		panel_1.add(lblTo);
		
		dcInsTo = new JDateChooser();
		dcInsTo.setDateFormatString("dd-MM-yyyy");
		panel_1.add(dcInsTo);
		
		JLabel lblFitness = new JLabel("Fitness");
		lblFitness.setHorizontalAlignment(SwingConstants.CENTER);
		lblFitness.setFont(new Font("Times New Roman", Font.BOLD, 15));
		panel_1.add(lblFitness);
		
		dcFitFrom = new JDateChooser();
		dcFitFrom.setDateFormatString("dd-MM-yyyy");
		panel_1.add(dcFitFrom);
		
		JLabel lblTo_1 = new JLabel("To");
		lblTo_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblTo_1.setFont(new Font("Times New Roman", Font.BOLD, 15));
		panel_1.add(lblTo_1);
		
		dcFitTo = new JDateChooser();
		dcFitTo.setDateFormatString("dd-MM-yyyy");
		panel_1.add(dcFitTo);
		
		JLabel lblPermit = new JLabel("Permit");
		lblPermit.setHorizontalAlignment(SwingConstants.CENTER);
		lblPermit.setFont(new Font("Times New Roman", Font.BOLD, 15));
		panel_1.add(lblPermit);
		
		dcPerFrom = new JDateChooser();
		dcPerFrom.setDateFormatString("dd-MM-yyyy");
		panel_1.add(dcPerFrom);
		
		JLabel lblTo_2 = new JLabel("To");
		lblTo_2.setHorizontalAlignment(SwingConstants.CENTER);
		lblTo_2.setFont(new Font("Times New Roman", Font.BOLD, 15));
		panel_1.add(lblTo_2);
		
		dcPerTo = new JDateChooser();
		dcPerTo.setDateFormatString("dd-MM-yyyy");
		panel_1.add(dcPerTo);
		
		JLabel lblService = new JLabel("Service");
		lblService.setHorizontalAlignment(SwingConstants.CENTER);
		lblService.setFont(new Font("Times New Roman", Font.BOLD, 15));
		panel_1.add(lblService);
		
		dcSerFrom = new JTextField();
//		dcSerFrom.setInputVerifier(inputVerifier);
		panel_1.add(dcSerFrom);
		
		JLabel lblTo_3 = new JLabel("To");
		lblTo_3.setHorizontalAlignment(SwingConstants.CENTER);
		lblTo_3.setFont(new Font("Times New Roman", Font.BOLD, 15));
		panel_1.add(lblTo_3);
		
		dcSerTo = new JTextField();
		panel_1.add(dcSerTo);
		panel.setLayout(new GridLayout(0, 2, 0, 0));
		
		JLabel lblNewLabel = new JLabel("Vehicle ID*");
		lblNewLabel.setFont(new Font("Times New Roman", Font.BOLD, 15));
		panel.add(lblNewLabel);
		
		txt_vid = new JTextField();
		
		txt_vid.setEditable(false);
		txt_vid.setFont(new Font("Arial", Font.PLAIN, 12));
		panel.add(txt_vid);
		txt_vid.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("Vehicle No.*");
		lblNewLabel_1.setFont(new Font("Times New Roman", Font.BOLD, 15));
		panel.add(lblNewLabel_1);
		
		txt_vno = new JTextField();
		txt_vno.setFont(new Font("Arial", Font.PLAIN, 12));
		panel.add(txt_vno);
		txt_vno.setColumns(10);
		
		lblOwnerName = new JLabel("Owner Name ");
		lblOwnerName.setFont(new Font("Times New Roman", Font.BOLD, 15));
		panel.add(lblOwnerName);
		
		txt_owner = new JTextField();
		txt_owner.setFont(new Font("Arial", Font.PLAIN, 12));
		txt_owner.setColumns(10);
		panel.add(txt_owner);
		
		lblOwnerMobile = new JLabel("Owner Mobile");
		lblOwnerMobile.setFont(new Font("Times New Roman", Font.BOLD, 15));
		panel_3.add(lblOwnerMobile);
		
		txt_owner_mob = new JTextField();
		txt_owner_mob.setFont(new Font("Arial", Font.PLAIN, 12));
		txt_owner_mob.setColumns(10);
		panel_3.add(txt_owner_mob);
		
		lblDriverName = new JLabel("Driver Name");
		lblDriverName.setFont(new Font("Times New Roman", Font.BOLD, 15));
		panel_3.add(lblDriverName);
		
		txt_driver_name = new JTextField();
		txt_driver_name.setFont(new Font("Arial", Font.PLAIN, 12));
		txt_driver_name.setColumns(10);
		panel_3.add(txt_driver_name);
		
		lblDriverMobile = new JLabel("Driver Mobile");
		lblDriverMobile.setFont(new Font("Times New Roman", Font.BOLD, 15));
		panel_3.add(lblDriverMobile);
		
		txt_driver_mobile = new JTextField();
		txt_driver_mobile.setFont(new Font("Arial", Font.PLAIN, 12));
		txt_driver_mobile.setColumns(10);
		panel_3.add(txt_driver_mobile);
		
		JLabel lblNewLabel_2 = new JLabel("Type");
		lblNewLabel_2.setFont(new Font("Times New Roman", Font.BOLD, 15));
		panel.add(lblNewLabel_2);
		
		txt_vtype = new JTextField();
		txt_vtype.setFont(new Font("Arial", Font.PLAIN, 12));
		panel.add(txt_vtype);
		txt_vtype.setColumns(10);
		
		JLabel lblNewLabel_3 = new JLabel("Model");
		lblNewLabel_3.setFont(new Font("Times New Roman", Font.BOLD, 15));
		panel.add(lblNewLabel_3);
		
		txt_vmodel = new JTextField();
		txt_vmodel.setFont(new Font("Arial", Font.PLAIN, 12));
		panel.add(txt_vmodel);
		txt_vmodel.setColumns(10);
		
		JLabel lblNewLabel_4 = new JLabel("Engine No.*");
		lblNewLabel_4.setFont(new Font("Times New Roman", Font.BOLD, 15));
		panel.add(lblNewLabel_4);
		
		txt_engno = new JTextField();
		txt_engno.setFont(new Font("Arial", Font.PLAIN, 12));
		panel.add(txt_engno);
		txt_engno.setColumns(10);
		
		JLabel lblNewLabel_5 = new JLabel("Chasis No.*");
		lblNewLabel_5.setFont(new Font("Times New Roman", Font.BOLD, 15));
		panel.add(lblNewLabel_5);
		
		txt_chasis = new JTextField();
		txt_chasis.setFont(new Font("Arial", Font.PLAIN, 12));
		panel.add(txt_chasis);
		txt_chasis.setColumns(10);
		
		btnNew = new JButton("New");
		btnNew.addActionListener(this);
		btnNew.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		
		btnNew.setFocusable(false);
		btnNew.setIcon(new ImageIcon(this.getClass().getResource("/plus.png")));
		toolBar.add(btnNew);
		
		btnSave = new JButton("Save");
		btnSave.addActionListener(this);
		btnSave.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		
		btnSave.setFocusable(false);
		btnSave.setIcon(new ImageIcon(this.getClass().getResource("/checked.png")));
		toolBar.add(btnSave);
		
		btnUpdate = new JButton("Update");
		btnUpdate.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		btnUpdate.addActionListener(this);
		btnUpdate.setIcon(new ImageIcon(this.getClass().getResource("/update.png")));
		toolBar_1.add(btnUpdate);
		
		btnDelete = new JButton("Delete");
		btnDelete.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		btnDelete.addActionListener(this);
		btnDelete.setFocusable(false);
		btnDelete.setIcon(new ImageIcon(this.getClass().getResource("/error.png")));
		toolBar.add(btnDelete);
		contentPane.setLayout(gl_contentPane);
		btnDeleteAll = new JButton("Delete All");
		btnDeleteAll.setIcon(new ImageIcon(this.getClass().getResource("/bin.png")));
		btnDeleteAll.addActionListener(this);
		btnDeleteAll.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		btnDeleteAll.setFocusable(false);
		toolBar_1.add(btnDeleteAll);
		
		Pollution = new JLabel("Authorization");
		Pollution.setHorizontalAlignment(SwingConstants.CENTER);
		Pollution.setFont(new Font("Times New Roman", Font.BOLD, 15));
		panel_1.add(Pollution);
		
		dcAuthor = new JDateChooser();
		dcAuthor.setDateFormatString("dd-MM-yyyy");
		panel_1.add(dcAuthor);
		
		lblRoadTax = new JLabel("Road Tax");
		lblRoadTax.setHorizontalAlignment(SwingConstants.CENTER);
		lblRoadTax.setFont(new Font("Times New Roman", Font.BOLD, 15));
		panel_1.add(lblRoadTax);
		
		dcRoadTax = new JDateChooser();
		dcRoadTax.setDateFormatString("dd-MM-yyyy");
		panel_1.add(dcRoadTax);
		
		lblPollution = new JLabel("Pollution");
		lblPollution.setHorizontalAlignment(SwingConstants.CENTER);
		lblPollution.setFont(new Font("Times New Roman", Font.BOLD, 15));
		panel_1.add(lblPollution);
		
		dcPollution = new JDateChooser();
		dcPollution.setDateFormatString("dd-MM-yyyy");
		panel_1.add(dcPollution);
		clearFormData();
	}

	private Date date = new Date();
	private DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
	private JDateChooser dcFitFrom;
	private JDateChooser dcFitTo;
	private JDateChooser dcPerTo;
	private JTextField dcSerTo;
	private JDateChooser dcPerFrom;
	private JTextField dcSerFrom;
	private JLabel Pollution;
	private JDateChooser dcAuthor;
	private JLabel lblRoadTax;
	private JDateChooser dcRoadTax;
	private JLabel lblPollution;
	private JDateChooser dcPollution;
	private JLabel lblOwnerName;
	private JTextField txt_owner;
	private JButton btnUpdate;
	private JToolBar toolBar_1;
	private JLabel lblOwnerMobile;
	private JPanel panel_3;
	private JTextField txt_owner_mob;
	private JLabel lblDriverName;
	private JTextField txt_driver_name;
	private JLabel lblDriverMobile;
	private JTextField txt_driver_mobile;
	private JButton btnDeleteAll;
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if (e.getSource() == btnDelete) {
			if (!txt_vid.getText().isEmpty()&&!txt_vno.getText().isEmpty()) {
				int id = Integer.parseInt(txt_vid.getText().trim());
				String veh_num = txt_vno.getText().trim();
				boolean deleteById = entityDao.deleteById(Vehicle.class, id);
				if (!deleteById) {
					showMessageDialog(1, "Vehicle No. Does not Exist..");
				}
				veh_model.removeAll();
				populateData();
				if(!veh_num.isEmpty())
					vehicleListener.onDeleteVehicle(veh_num);
			}else {
				showMessageDialog(1,"Please Select a Row for delete ");
			}
		}
		
		if (e.getSource() == btnSave) {
			if (!validateForm().isEmpty()) {
				showMessageDialog(1,validateForm());
			}else {
				Vehicle vehicle = getFormData();
				boolean status = true;
				for (int i = 0; i < veh_model.getRowCount(); i++) {
					if (veh_model.getValueAt(i, 1).equals(vehicle.getV_num())) {
						status = false;
						showMessageDialog(1, "Vehicle Number already exist.");
					}
				}
				
				if (status) {
					entityDao.storeInDatabase(vehicle,Constants.SYNC_FAILED);
					setTableRow(vehicle);
					vehicleListener.onNewVehicle(vehicle);
				}
			}
		}
		
		if (e.getSource() == btnNew) {
			clearFormData();
		}
		
		if (e.getSource() == btnUpdate) {
			if (!validateForm().isEmpty()) {
				showMessageDialog(1,validateForm());
			}else {
				Vehicle vehicle = getFormData();
				entityDao.updateDatabase(vehicle);
				table.clearSelection();
				table.getSelectionModel().clearSelection();
				veh_model.removeAll();
				populateData();
				showMessageDialog(5,"Update Successfull");
				vehicleListener.onUpdateVehicle(vehicle);
			}
		}
		
		if (e.getSource() == btnDeleteAll) {
			int a = JOptionPane.showConfirmDialog(this, "Delete All Entries");
			if (a == JOptionPane.YES_OPTION) {
				new Thread(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						jf.setModal(true);
				    	jf.setAlwaysOnTop(true);
				    	jf.setModalityType(ModalityType.APPLICATION_MODAL);
						jf.setInterface(VehicleEntry.this);
						jf.setVisible(true);
					}
				}).start();
			
			}
			veh_model.removeAll();
			populateData();
			vehicleListener.onDeleteAll();
		}
	}
	LoginFrame jf = new LoginFrame();
	
	private Vehicle getFormData() {
		Vehicle vehicle = new Vehicle();
		vehicle.setV_id(Integer.parseInt(txt_vid.getText()));
		vehicle.setV_num(txt_vno.getText());
		vehicle.setOwner_name(txt_owner.getText());
		vehicle.setOwner_mob(txt_owner_mob.getText());
		vehicle.setDriver_name(txt_driver_name.getText());
		vehicle.setDriver_mob(txt_driver_mobile.getText());
		vehicle.setV_type(txt_vtype.getText());
		vehicle.setV_model(txt_vmodel.getText());
		vehicle.setV_engine(txt_engno.getText());
		vehicle.setV_chasis(txt_chasis.getText());
		vehicle.setSer_form(dcSerFrom.getText());
		vehicle.setSer_to(dcSerTo.getText());
		try {
//			System.out.println(checkDateFields(dcInsFrom.getDate())+":"+dcInsTo.getDate()+":"+dcFitTo.getDate()+":"+dcPerFrom.getDate());
			if (checkDateFields(dcInsFrom.getDate())&&
					checkDateFields(dcInsTo.getDate())&&
					checkDateFields(dcFitFrom.getDate())&&
					checkDateFields(dcFitTo.getDate())&&
					checkDateFields(dcPerFrom.getDate())&&
					checkDateFields(dcPerTo.getDate())&&
					checkDateFields(dcAuthor.getDate())&&
					checkDateFields(dcRoadTax.getDate())&&
					checkDateFields(dcPollution.getDate())) {
				vehicle.setIns_from(dateFormat.format(dcInsFrom.getDate()));
				vehicle.setIns_to(dateFormat.format(dcInsTo.getDate()));
				vehicle.setFit_form(dateFormat.format(dcFitFrom.getDate()));
				vehicle.setFit_to(dateFormat.format(dcFitTo.getDate()));
				vehicle.setPer_form(dateFormat.format(dcPerFrom.getDate()));
				vehicle.setPer_to(dateFormat.format(dcPerTo.getDate()));
				vehicle.setAuthor(dateFormat.format(dcAuthor.getDate()));
				vehicle.setRd_tax(dateFormat.format(dcRoadTax.getDate()));
				vehicle.setPollute(dateFormat.format(dcPollution.getDate()));
			}
		} catch (Exception e) {
				e.printStackTrace();
		}
		
		
		
		return vehicle;
	}
	
	private void setFormData(Vehicle veh) {
		txt_vid.setText(String.valueOf(veh.getV_id()));
		txt_vno.setText(veh.getV_num());
		txt_owner.setText(veh.getOwner_name());
		txt_owner_mob.setText(veh.getOwner_mob());
		txt_driver_name.setText(veh.getDriver_name());
		txt_driver_mobile.setText(veh.getDriver_mob());
		txt_vtype.setText(veh.getV_type());
		txt_vmodel.setText(veh.getV_model());
		txt_engno.setText(veh.getV_engine());
		txt_chasis.setText(veh.getV_chasis());
		dcSerFrom.setText(veh.getSer_form());
		dcSerTo.setText(veh.getSer_to());
		try {
			if (checkDateFields(veh.getIns_from())&&
					checkDateFields(veh.getIns_to())&&
					checkDateFields(veh.getFit_form())&&
					checkDateFields(veh.getFit_to())&&
					checkDateFields(veh.getPer_form())&&
					checkDateFields(veh.getPer_to())&&
					checkDateFields(veh.getPollute())&&
					checkDateFields(veh.getRd_tax())&&
					checkDateFields(veh.getAuthor())) {
				dcInsFrom.setDate(dateFormat.parse(veh.getIns_from()));
				dcInsTo.setDate(dateFormat.parse(veh.getIns_to()));
				dcFitFrom.setDate(dateFormat.parse(veh.getFit_form()));
				dcFitTo.setDate(dateFormat.parse(veh.getFit_to()));
				dcPerFrom.setDate(dateFormat.parse(veh.getPer_form()));
				dcPerTo.setDate(dateFormat.parse(veh.getPer_to()));
				dcPollution.setDate(dateFormat.parse(veh.getPollute()));
				dcRoadTax.setDate(dateFormat.parse(veh.getRd_tax()));
				dcAuthor.setDate(dateFormat.parse(veh.getAuthor()));
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	private boolean checkDateFields(String data) {
		if (data == null) {
			return false;
		}
		if (data.isEmpty()) {
			return false;
		}
		return true;
	}	
	
	private boolean checkDateFields(Date data) {
		if (data == null) {
			return false;
		}
		
		return true;
	}	
	
	
	public void clearFormData() {
		int maxCount = entityDao.getMaxCount("vehicle");
		if(maxCount>0)
			txt_vid.setText(String.valueOf(maxCount));
		txt_vno.setText("");
		txt_vtype.setText("");
		txt_vmodel.setText("");
		txt_engno.setText("");
		txt_chasis.setText("");
		txt_owner.setText("");
		txt_owner_mob.setText("");
		txt_driver_mobile.setText("");
		txt_driver_name.setText("");
		dcInsFrom.setCalendar(null);
		dcAuthor.setCalendar(null);
		dcFitFrom.setCalendar(null);
		dcFitTo.setCalendar(null);
		dcInsFrom.setCalendar(null);
		dcInsTo.setCalendar(null);
		dcPerFrom.setCalendar(null);
		dcPerTo.setCalendar(null);
		dcPollution.setCalendar(null);
		dcRoadTax.setCalendar(null);
		dcSerFrom.setText("");
		dcSerTo.setText("");
	}
	private String validateForm() {
		StringBuilder err = new StringBuilder();
		if (txt_vid.getText().isEmpty()) {
			err.append("Please Enter Vehicle Id");
		}
		if (txt_vno.getText().isEmpty()) {
			err.append("Please Enter Vehicle Number");
		}
		/*if (txt_vmodel.getText().isEmpty()) {
			err.append("Please Enter Vehicle Model");
		}*/
		
		
		/*if (txt_engno.getText().isEmpty()) {
			err.append("Please Enter Engine Number");
		}
		
		if (txt_chasis.getText().isEmpty()) {
			err.append("Please Enter Chasis Id");
		}*/
		return err.toString();
	}
	
	private void showMessageDialog(int count,String message) {
		
		switch(count) {
		case 1:
			JOptionPane.showMessageDialog(this, message, "Alert", JOptionPane.WARNING_MESSAGE);
			break;
		default:
			JOptionPane.showMessageDialog(null, message);	
		}
		
		
	}
	@Override
	public void onLoginSuccess() {
		// TODO Auto-generated method stub
		entityDao.deleteTable("Vehicle");
	}
	@Override
	public void onLoginFailure() {
		// TODO Auto-generated method stub
		showMessageDialog(1, "Not Authorized");
	}
	
	
}
