package com.javacore.transportMgmt;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.MatteBorder;
import javax.swing.border.TitledBorder;

import com.javacore.transportMgmt.InsuranceDialog.InsuranceListener;
import com.javacore.transportMgmt.db.EntityDao;
import com.javacore.transportMgmt.db.HibernateUtils;
import com.javacore.transportMgmt.db.MySingleton;
import com.javacore.transportMgmt.model.BilityModel;
import com.javacore.transportMgmt.model.Bill;
import com.javacore.transportMgmt.model.Constants;
import com.javacore.transportMgmt.model.Customer;
import com.javacore.transportMgmt.model.Insurance;
import com.javacore.transportMgmt.model.Lagguage;
import com.javacore.transportMgmt.model.Vehicle;
import com.javacore.transportMgmt.utility.AutoComplete;
import com.javacore.transportMgmt.utility.PdfModel;
import com.toedter.calendar.JDateChooser;

import net.miginfocom.swing.MigLayout;

public class DispatchChallan extends JFrame implements ActionListener ,InsuranceListener{

	private static final long serialVersionUID = 7482673012294896712L;
	private JPanel contentPane;
	private JTextField txt_gr_num;
	private JTextField txt_fr_rate;
	private JTextField txt_cgst;
	private JTextField txt_sgst;
	private JTextField txt_checkExp;
	private JTextField txt_adv_pay;
	private JTextField txt_packages;
	private JTextField txt_quantity;
	private JTextField txt_weight;
	private JTextField txt_bank_name;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JDateChooser dateChooser;
	private JTextField txt_consignor_add;
	private JTextField txt_consignee_add;
	private JTextField txt_consignor_gst;
	private JTextField txt_consignee_gst;
	private JComboBox txt_veh_no;
	private JTextField txt_veh_ownr;
	private JTextField txt_driver;
	private JButton btnInsurance;
	private JTextField txt_grand_total;
	private JTextField txt_balance;
	private JTextField txt_stat_charge;
	private final ButtonGroup buttonGroup_1 = new ButtonGroup();
	private JComboBox cmbxConsignor;
	private BilityModel bility_model = new BilityModel();
	private BilityListener listener;
	
	
	
	
	public interface BilityListener{
		public abstract void onNewEntry(BilityModel bilityModel);
		public abstract void onUpdateEntry(BilityModel bilityModel);
	}
	
	
	public DispatchChallan() {
		setTitle("BILTY FORM");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 1132, 625);
		setIconImage(new ImageIcon(this.getClass().getResource("/success.png")).getImage());
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JToolBar toolBar = new JToolBar();
		toolBar.setFloatable(false);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.WHITE);
		panel_1.setBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(0, 0, 0)));
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.WHITE);
		panel.setBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(0, 0, 0)));
		
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(Color.WHITE);
		panel_2.setBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229)), "Freight Payment", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 255)));
		
		JPanel panel_3 = new JPanel();
		panel_3.setBackground(Color.WHITE);
		
		JPanel panel_4 = new JPanel();
		panel_4.setBackground(Color.WHITE);
		panel_4.setBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229)), "Payment Mode", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 255)));
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(panel_1, GroupLayout.DEFAULT_SIZE, 1082, Short.MAX_VALUE)
					.addContainerGap())
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(panel, GroupLayout.DEFAULT_SIZE, 1082, Short.MAX_VALUE)
					.addContainerGap())
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addContainerGap()
							.addComponent(panel_2, GroupLayout.DEFAULT_SIZE, 526, Short.MAX_VALUE)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(panel_4, GroupLayout.DEFAULT_SIZE, 269, Short.MAX_VALUE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(panel_3, GroupLayout.DEFAULT_SIZE, 281, Short.MAX_VALUE))
						.addComponent(toolBar, GroupLayout.DEFAULT_SIZE, 1106, Short.MAX_VALUE))
					.addGap(0))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addComponent(toolBar, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(panel, GroupLayout.PREFERRED_SIZE, 98, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(panel_1, GroupLayout.PREFERRED_SIZE, 206, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
						.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE, false)
							.addComponent(panel_2, GroupLayout.PREFERRED_SIZE, 224, GroupLayout.PREFERRED_SIZE)
							.addComponent(panel_4, GroupLayout.PREFERRED_SIZE, 224, GroupLayout.PREFERRED_SIZE))
						.addComponent(panel_3, GroupLayout.PREFERRED_SIZE, 216, GroupLayout.PREFERRED_SIZE)))
		);
		setPanel5(panel_4);
		
		txt_chq_dd = new JTextField();
		txt_chq_dd.setHorizontalAlignment(SwingConstants.TRAILING);
		txt_chq_dd.setFont(new Font("Arial", Font.PLAIN, 12));
		txt_chq_dd.setColumns(10);
		panel_4.add(txt_chq_dd);
		
		JLabel lblDate = new JLabel("Date");
		lblDate.setFont(new Font("Times New Roman", Font.BOLD, 13));
		panel_4.add(lblDate);
		
		dcChqDate = new JDateChooser();
		dcChqDate.setDateFormatString("dd-MM-yyyy");
		dcChqDate.setMinSelectableDate(this.date);
		dcChqDate.setDate(this.date);
		panel_4.add(dcChqDate);
		
		setPanel4(panel_3);
		panel_2.setLayout(null);
		
		setpanel3(panel_2);
		panel.setLayout(new MigLayout("", "[286px][644px]", "[74px]"));
		
		setPanel1(panel);
		
		
		panel_1.setLayout(new MigLayout("", "[][]", "[]"));
		
		setPanel2(panel_1);
		
		setToolbar(toolBar);
		
		
		contentPane.setLayout(gl_contentPane);
		updateCustomerList(null);
	}
	private Date date = new Date();
//	private DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
	
	public void setBilityListener(Object app) {
		this.listener = (BilityListener)app;
	}
private void updateCustomerList(BilityModel obj) {
		
		boolean addCustomer = true;
		DefaultComboBoxModel<String> model = (DefaultComboBoxModel<String>) cmbxConsignee.getModel();
		DefaultComboBoxModel<String> modelConsinor = (DefaultComboBoxModel<String>) cmbxConsignor.getModel();
		List<BilityModel> bilityList = entityDao.getBilityList();
		if(bilityList.isEmpty())
			return;
		
		for(BilityModel bill :bilityList) {
			if (bill != null) {
				model.addElement(bill.getConsignee().getCustomer_name());
				modelConsinor.addElement(bill.getConsignor().getCustomer_name());
				if (obj != null) {
					if (obj.getConsignee().getCustomer_name().equalsIgnoreCase(bill.getConsignee().getCustomer_name())) {
						addCustomer = false;
					}
				}
			}
			
		}
		if (addCustomer&& obj != null) {
			model.addElement(obj.getConsignee().getCustomer_name());
		}
		cmbxConsignee.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				String str = (String)cmbxConsignee.getSelectedItem();
				if (str != null) {
					for(BilityModel bill:bilityList) {
						if (str.equalsIgnoreCase(bill.getConsignee().getCustomer_name())) {
							txt_consignee_add.setText(bill.getConsignee().getAddress());
							if (bill.getConsignee().getGst_no()!= null) {
								txt_consignee_gst.setText(bill.getConsignee().getGst_no());
							}
						}
					}
				}
			}
		});
		
		cmbxConsignor.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				String str = (String)cmbxConsignor.getSelectedItem();
				if (str != null) {
					for(BilityModel bill:bilityList) {
						if (str.equalsIgnoreCase(bill.getConsignor().getCustomer_name())) {
							txt_consignee_add.setText(bill.getConsignor().getAddress());
							if (bill.getConsignor().getGst_no()!= null) {
								txt_consignee_gst.setText(bill.getConsignor().getGst_no());
							}
						}
					}
				}
			}
		});
		
	}
	
	private void setToolbar(JToolBar toolBar) {
		btnNew = new JButton("New");
		btnNew.addActionListener(this);
		btnNew.setFocusable(false);
		toolBar.add(btnNew);
		btnNew.setIcon(new ImageIcon(this.getClass().getResource("/plus.png")));
		
		btnSave = new JButton("Save");
		btnSave.addActionListener(this);
		btnSave.setFocusable(false);
		toolBar.add(btnSave);
		btnSave.setIcon(new ImageIcon(this.getClass().getResource("/checked.png")));
		
		btnPrint = new JButton("Print");
		btnPrint.addActionListener(this);
		btnPrint.setIcon(new ImageIcon(this.getClass().getResource("/printer.png")));
		btnPrint.setFocusable(false);
		toolBar.add(btnPrint);
		
		btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(this);
		btnCancel.setFocusable(false);
		toolBar.add(btnCancel);
		btnCancel.setIcon(new ImageIcon(this.getClass().getResource("/error.png")));
		btnUpdateStatus = new JButton("Update Status");
		btnUpdateStatus.setIcon(new ImageIcon(this.getClass().getResource("/update.png")));
		btnUpdateStatus.setFocusable(false);
		btnUpdateStatus.addActionListener(this);
		toolBar.add(btnUpdateStatus);
		
//		btnUpdateStatus.setFont(new Font("Times New Roman", Font.BOLD, 12));
	}

	private void setPanel5(JPanel panel_4) {
		panel_4.setLayout(new GridLayout(0, 2, 0, 20));
		
		rdbtnC = new JRadioButton("Cash");
		rdbtnC.setSelected(true);
		rdbtnC.setBackground(Color.WHITE);
		buttonGroup.add(rdbtnC);
		panel_4.add(rdbtnC);
		
		rdbtnBank = new JRadioButton("Bank");
		rdbtnBank.setBackground(Color.WHITE);
		buttonGroup.add(rdbtnBank);
		panel_4.add(rdbtnBank);
		
		JLabel lblBankName = new JLabel("Bank Name:");
		lblBankName.setFont(new Font("Times New Roman", Font.BOLD, 13));
		panel_4.add(lblBankName);
		
		txt_bank_name = new JTextField();
		txt_bank_name.setFont(new Font("Arial", Font.PLAIN, 12));
		txt_bank_name.setHorizontalAlignment(SwingConstants.TRAILING);
		txt_bank_name.setColumns(10);
		panel_4.add(txt_bank_name);
		
		JLabel lblChqddNo = new JLabel("Chq./DD No.");
		lblChqddNo.setFont(new Font("Times New Roman", Font.BOLD, 13));
		panel_4.add(lblChqddNo);
	}

	private void setPanel4(JPanel panel_3) {
		JPanel panel_13 = new JPanel();
		panel_13.setBackground(Color.WHITE);
		panel_13.setLayout(new GridLayout(0, 2, 0, 0));
		
		JLabel lblTotalPkgs = new JLabel("Total Pkgs.:");
		panel_13.add(lblTotalPkgs);
		lblTotalPkgs.setFont(new Font("Times New Roman", Font.BOLD, 13));
		
		txt_packages = new JTextField();
		txt_packages.setFont(new Font("Arial", Font.PLAIN, 12));
		panel_13.add(txt_packages);
		txt_packages.setHorizontalAlignment(SwingConstants.TRAILING);
		txt_packages.setColumns(10);
		
		JLabel lblTotalQty = new JLabel("Total Weight(Kg.):");
		panel_13.add(lblTotalQty);
		lblTotalQty.setFont(new Font("Times New Roman", Font.BOLD, 13));
		
		txt_quantity = new JTextField();
		txt_quantity.setFont(new Font("Arial", Font.PLAIN, 12));
		panel_13.add(txt_quantity);
		txt_quantity.setHorizontalAlignment(SwingConstants.TRAILING);
		txt_quantity.setColumns(10);
		
		JLabel lblTotalWeight = new JLabel("Weight Charged(per Kg.):");
		panel_13.add(lblTotalWeight);
		lblTotalWeight.setFont(new Font("Times New Roman", Font.BOLD, 13));
		
		txt_weight = new JTextField();
		txt_weight.setFont(new Font("Arial", Font.PLAIN, 12));
		panel_13.add(txt_weight);
		txt_weight.setHorizontalAlignment(SwingConstants.TRAILING);
		txt_weight.setColumns(10);
		
		JPanel panel_14 = new JPanel();
		
		JLabel lblNewLabel_1 = new JLabel("Add Description Here");
		lblNewLabel_1.setForeground(Color.WHITE);
		lblNewLabel_1.setOpaque(true);
		lblNewLabel_1.setFont(new Font("Times New Roman", Font.BOLD, 15));
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_1.setBackground(new Color(102, 205, 170));
		GroupLayout gl_panel_3 = new GroupLayout(panel_3);
		gl_panel_3.setHorizontalGroup(
			gl_panel_3.createParallelGroup(Alignment.LEADING)
				.addComponent(panel_14, GroupLayout.DEFAULT_SIZE, 281, Short.MAX_VALUE)
				.addComponent(panel_13, GroupLayout.DEFAULT_SIZE, 281, Short.MAX_VALUE)
				.addComponent(lblNewLabel_1, GroupLayout.DEFAULT_SIZE, 281, Short.MAX_VALUE)
		);
		gl_panel_3.setVerticalGroup(
			gl_panel_3.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_panel_3.createSequentialGroup()
					.addComponent(lblNewLabel_1, GroupLayout.PREFERRED_SIZE, 29, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addComponent(panel_14, GroupLayout.PREFERRED_SIZE, 74, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(panel_13, GroupLayout.PREFERRED_SIZE, 97, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
		);
		
		JLabel label = new JLabel("");
		label.setFont(new Font("Times New Roman", Font.BOLD, 13));
		panel_13.add(label);
		
		btnWeightCharge = new JButton("ADD");
		btnWeightCharge.addActionListener(this);
		btnWeightCharge.setBackground(new Color(100, 149, 237));
		btnWeightCharge.setForeground(Color.WHITE);
		btnWeightCharge.setFont(new Font("Times New Roman", Font.BOLD, 12));
		panel_13.add(btnWeightCharge);
		panel_14.setLayout(new BorderLayout(0, 0));
		
		JScrollPane scrollPane = new JScrollPane();
		panel_14.add(scrollPane);
		
		txt_area_desc = new JTextArea();
		txt_area_desc.setLineWrap(true);
		txt_area_desc.setFont(new Font("Arial", Font.PLAIN, 12));
		txt_area_desc.setBackground(UIManager.getColor("Button.background"));
		scrollPane.setViewportView(txt_area_desc);
		panel_3.setLayout(gl_panel_3);
	}

	private void setpanel3(JPanel panel_2) {
		JPanel panel_10 = new JPanel();
		panel_10.setBackground(Color.WHITE);
		panel_10.setBounds(5, 22, 251, 202);
		panel_2.add(panel_10);
		panel_10.setLayout(new GridLayout(6, 2, 0, 0));
		
		JLabel lblRateType = new JLabel("Freight Type:");
		panel_10.add(lblRateType);
		lblRateType.setFont(new Font("Times New Roman", Font.BOLD, 13));
		
		cmbxFrtype = new JComboBox();
		panel_10.add(cmbxFrtype);
		cmbxFrtype.setModel(new DefaultComboBoxModel(new String[] {"To Pay", "To be Built"}));
		
		JLabel lblRate = new JLabel("Freight:");
		panel_10.add(lblRate);
		lblRate.setFont(new Font("Times New Roman", Font.BOLD, 13));
		
		txt_fr_rate = new JTextField();
		
		txt_fr_rate.setFont(new Font("Arial", Font.PLAIN, 12));
		panel_10.add(txt_fr_rate);
		txt_fr_rate.setHorizontalAlignment(SwingConstants.TRAILING);
		txt_fr_rate.setColumns(10);
		
		JLabel lblHireAmont = new JLabel("CGST:");
		panel_10.add(lblHireAmont);
		lblHireAmont.setFont(new Font("Times New Roman", Font.BOLD, 13));
		
		txt_cgst = new JTextField();
		
		txt_cgst.setFont(new Font("Arial", Font.PLAIN, 12));
		panel_10.add(txt_cgst);
		txt_cgst.setHorizontalAlignment(SwingConstants.TRAILING);
		txt_cgst.setColumns(10);
		
		JLabel lblLoading = new JLabel("SGST:");
		panel_10.add(lblLoading);
		lblLoading.setFont(new Font("Times New Roman", Font.BOLD, 13));
		
		txt_sgst = new JTextField();
		
		txt_sgst.setFont(new Font("Arial", Font.PLAIN, 12));
		panel_10.add(txt_sgst);
		txt_sgst.setHorizontalAlignment(SwingConstants.TRAILING);
		txt_sgst.setColumns(10);
		
		JLabel lblOtherCharges = new JLabel("Check Post Exp:");
		panel_10.add(lblOtherCharges);
		lblOtherCharges.setFont(new Font("Times New Roman", Font.BOLD, 13));
		
		txt_checkExp = new JTextField();
		
		txt_checkExp.setFont(new Font("Arial", Font.PLAIN, 12));
		panel_10.add(txt_checkExp);
		txt_checkExp.setHorizontalAlignment(SwingConstants.TRAILING);
		txt_checkExp.setColumns(10);
		
		JLabel lblAdvancePaid = new JLabel("Advance Paid:");
		panel_10.add(lblAdvancePaid);
		lblAdvancePaid.setFont(new Font("Times New Roman", Font.BOLD, 13));
		
		txt_adv_pay = new JTextField();
		
		txt_adv_pay.setFont(new Font("Arial", Font.PLAIN, 12));
		panel_10.add(txt_adv_pay);
		txt_adv_pay.setHorizontalAlignment(SwingConstants.TRAILING);
		txt_adv_pay.setColumns(10);
		
		JPanel panel_11 = new JPanel();
		panel_11.setBackground(Color.WHITE);
		panel_11.setBounds(263, 12, 251, 112);
		panel_2.add(panel_11);
		panel_11.setLayout(new GridLayout(3, 2, 0, 0));
		
		JLabel lblStaticalCharge = new JLabel("Statical Charge:");
		lblStaticalCharge.setFont(new Font("Times New Roman", Font.BOLD, 13));
		panel_11.add(lblStaticalCharge);
		
		txt_stat_charge = new JTextField();
		txt_stat_charge.setFont(new Font("Arial", Font.PLAIN, 12));
		txt_stat_charge.setText("20.00");
		txt_stat_charge.setHorizontalAlignment(SwingConstants.TRAILING);
		txt_stat_charge.setColumns(10);
		panel_11.add(txt_stat_charge);
		
		JLabel lblGrandTotal = new JLabel("Grand Total:");
		lblGrandTotal.setFont(new Font("Times New Roman", Font.BOLD, 13));
		panel_11.add(lblGrandTotal);
		
		txt_grand_total = new JTextField();
		txt_grand_total.setFont(new Font("Arial", Font.PLAIN, 12));
		txt_grand_total.setHorizontalAlignment(SwingConstants.TRAILING);
		txt_grand_total.setColumns(10);
		panel_11.add(txt_grand_total);
		
		JLabel lblBalance = new JLabel("Balance:");
		lblBalance.setFont(new Font("Times New Roman", Font.BOLD, 13));
		panel_11.add(lblBalance);
		
		txt_balance = new JTextField();
		txt_balance.setFont(new Font("Arial", Font.PLAIN, 12));
		txt_balance.setHorizontalAlignment(SwingConstants.TRAILING);
		txt_balance.setColumns(10);
		panel_11.add(txt_balance);
		
		JPanel panel_12 = new JPanel();
		panel_12.setBackground(Color.WHITE);
		panel_12.setBounds(263, 123, 251, 101);
		panel_2.add(panel_12);
		panel_12.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("GST Paid By");
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setOpaque(true);
		lblNewLabel.setBackground(new Color(102, 205, 170));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(12, 12, 227, 25);
		panel_12.add(lblNewLabel);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 45, 239, 52);
		panel_12.add(panel);
		panel.setLayout(new GridLayout(0, 2, 0, 0));
		
		chckbxConsignor = new JCheckBox("Consignor");
		chckbxConsignor.setBackground(Color.WHITE);
		panel.add(chckbxConsignor);
		buttonGroup_1.add(chckbxConsignor);
		
		chckbxConsignee = new JCheckBox("Consignee");
		chckbxConsignee.setBackground(Color.WHITE);
		panel.add(chckbxConsignee);
		buttonGroup_1.add(chckbxConsignee);
		
		chckbxTranspoter = new JCheckBox("Transporter");
		chckbxTranspoter.setBackground(Color.WHITE);
		panel.add(chckbxTranspoter);
		buttonGroup_1.add(chckbxTranspoter);
		
		btnCalculate = new JButton("CALCULATE");
		btnCalculate.addActionListener(this);
		btnCalculate.setForeground(Color.WHITE);
		btnCalculate.setBackground(new Color(100, 149, 237));
		panel.add(btnCalculate);
	}

	private void setPanel2(JPanel panel_1) {
		JPanel panel_6 = new JPanel();
		panel_6.setBackground(Color.WHITE);
		panel_1.add(panel_6, "cell 0 0,grow");
		panel_6.setLayout(new MigLayout("", "[][grow][][]", "[][][][][][][][]"));
		
		JLabel lblVehicleNo = new JLabel("Vehicle No.:");
		lblVehicleNo.setHorizontalAlignment(SwingConstants.CENTER);
		lblVehicleNo.setFont(new Font("Times New Roman", Font.BOLD, 13));
		panel_6.add(lblVehicleNo, "cell 0 0,alignx trailing");
		
		txt_veh_no = new AutoComplete(entityDao.getVehicleNames().toArray());
		txt_veh_no.setEditable(true);
		txt_veh_no.setFont(new Font("Arial", Font.PLAIN, 12));
		panel_6.add(txt_veh_no, "cell 1 0,growx");
		
		
		btnVehicleSearch = new JButton("");
		btnVehicleSearch.addActionListener(this);
		btnVehicleSearch.setIcon(new ImageIcon(this.getClass().getResource("/seek.png")));
		btnVehicleSearch.setBackground(new Color(100, 149, 237));
		panel_6.add(btnVehicleSearch, "cell 2 0");
		
		JLabel lblOwnerName = new JLabel("Owner Name");
		lblOwnerName.setHorizontalAlignment(SwingConstants.CENTER);
		lblOwnerName.setFont(new Font("Times New Roman", Font.BOLD, 13));
		panel_6.add(lblOwnerName, "cell 0 1,alignx trailing");
		
		txt_veh_ownr = new JTextField();
		txt_veh_ownr.setFont(new Font("Arial", Font.PLAIN, 12));
		txt_veh_ownr.setColumns(10);
		panel_6.add(txt_veh_ownr, "cell 1 1 2 1,growx");
		
		JLabel lblDriverName = new JLabel("Driver Name");
		lblDriverName.setHorizontalAlignment(SwingConstants.CENTER);
		lblDriverName.setFont(new Font("Times New Roman", Font.BOLD, 13));
		panel_6.add(lblDriverName, "cell 0 2,alignx trailing");
		
		txt_driver = new JTextField();
		txt_driver.setFont(new Font("Arial", Font.PLAIN, 12));
		txt_driver.setColumns(10);
		panel_6.add(txt_driver, "cell 1 2 2 1,growx");
		
		JLabel lblFromStation = new JLabel("From Station");
		panel_6.add(lblFromStation, "cell 0 7,growx,aligny center");
		lblFromStation.setHorizontalAlignment(SwingConstants.CENTER);
		lblFromStation.setFont(new Font("Times New Roman", Font.BOLD, 13));
		
		cmbxFStation = new JComboBox();
		panel_6.add(cmbxFStation, "cell 1 7");
		
		if(list != null)
			cmbxFStation.setModel(new DefaultComboBoxModel(list.toArray()));
		cmbxFStation.setEditable(true);
		
		JLabel lblToStation = new JLabel("To Station");
		panel_6.add(lblToStation, "cell 2 7");
		lblToStation.setHorizontalAlignment(SwingConstants.CENTER);
		lblToStation.setFont(new Font("Times New Roman", Font.BOLD, 13));
		
		cmbxTStation = new JComboBox();
		panel_6.add(cmbxTStation, "cell 3 7");
		if(list != null)
			cmbxTStation.setModel(new DefaultComboBoxModel(list.toArray()));
		cmbxTStation.setEditable(true);
		
		JPanel panel_5 = new JPanel();
		panel_5.setBackground(Color.WHITE);
		panel_1.add(panel_5, "cell 1 0,push ,grow");
		panel_5.setLayout(new GridLayout(0, 2, 30, 0));
//		panel_5.setLayout(new MigLayout());
		
		JLabel label_1 = new JLabel("Consignor");
		label_1.setHorizontalAlignment(SwingConstants.RIGHT);
		label_1.setFont(new Font("Times New Roman", Font.BOLD, 13));
		panel_5.add(label_1);
		
		cmbxConsignor = new JComboBox();
		cmbxConsignor.setFont(new Font("Arial", Font.BOLD, 12));
		cmbxConsignor.setEditable(true);
		panel_5.add(cmbxConsignor);
		
		JLabel label_2 = new JLabel("Address");
		label_2.setHorizontalAlignment(SwingConstants.RIGHT);
		label_2.setFont(new Font("Times New Roman", Font.BOLD, 13));
		panel_5.add(label_2);
		
		txt_consignor_add = new JTextField();
		txt_consignor_add.setFont(new Font("Arial", Font.PLAIN, 12));
		txt_consignor_add.setColumns(10);
		panel_5.add(txt_consignor_add);
		
		JLabel lblGstNo = new JLabel("GST No.:");
		lblGstNo.setHorizontalAlignment(SwingConstants.RIGHT);
		lblGstNo.setFont(new Font("Times New Roman", Font.BOLD, 13));
		panel_5.add(lblGstNo);
		
		txt_consignor_gst = new JTextField();
		txt_consignor_gst.setFont(new Font("Arial", Font.PLAIN, 12));
		txt_consignor_gst.setColumns(10);
		panel_5.add(txt_consignor_gst);
		
		JLabel label_9 = new JLabel("Consignee ");
		label_9.setHorizontalAlignment(SwingConstants.RIGHT);
		label_9.setFont(new Font("Times New Roman", Font.BOLD, 13));
		panel_5.add(label_9);
		
		cmbxConsignee = new JComboBox();
		cmbxConsignee.setFont(new Font("Arial", Font.BOLD, 12));
		cmbxConsignee.setEditable(true);
		panel_5.add(cmbxConsignee);
		
		JLabel label_10 = new JLabel("Address");
		label_10.setHorizontalAlignment(SwingConstants.RIGHT);
		label_10.setFont(new Font("Times New Roman", Font.BOLD, 13));
		panel_5.add(label_10);
		
		txt_consignee_add = new JTextField();
		txt_consignee_add.setFont(new Font("Arial", Font.PLAIN, 12));
		txt_consignee_add.setColumns(10);
		panel_5.add(txt_consignee_add);
		
		JLabel lblGstNo_1 = new JLabel("GST No.:");
		lblGstNo_1.setHorizontalAlignment(SwingConstants.RIGHT);
		lblGstNo_1.setFont(new Font("Times New Roman", Font.BOLD, 13));
		panel_5.add(lblGstNo_1);
		
		txt_consignee_gst = new JTextField();
		txt_consignee_gst.setFont(new Font("Arial", Font.PLAIN, 12));
		txt_consignee_gst.setHorizontalAlignment(SwingConstants.LEFT);
		txt_consignee_gst.setColumns(10);
		panel_5.add(txt_consignee_gst);
	}

	private void setPanel1(JPanel panel) {
		JPanel panel_7 = new JPanel();
		panel_7.setBackground(Color.WHITE);
		panel.add(panel_7, "cell 0 0,grow");
		panel_7.setLayout(new GridLayout(2, 2, 0, 10));
		
		JLabel lblChallanNo = new JLabel("G.R. No.");
		panel_7.add(lblChallanNo);
		lblChallanNo.setHorizontalAlignment(SwingConstants.CENTER);
		lblChallanNo.setFont(new Font("Times New Roman", Font.BOLD, 13));
		
		JPanel panel_8 = new JPanel();
		panel_7.add(panel_8);
		panel_8.setLayout(null);
		
		txt_gr_num = new JTextField();
		increaseGR();
		txt_gr_num.setFont(new Font("Arial", Font.PLAIN, 12));
		txt_gr_num.setBounds(0, 5, 107, 27);
		panel_8.add(txt_gr_num);
		txt_gr_num.setColumns(10);
		
		btnSearchGR = new JButton("");
		btnSearchGR.addActionListener(this);
		btnSearchGR.setBounds(107, 5, 36, 27);
		panel_8.add(btnSearchGR);
		btnSearchGR.setBackground(new Color(100, 149, 237));
		btnSearchGR.setIcon(new ImageIcon(this.getClass().getResource("/seek.png")));
		
		JLabel label = new JLabel("Date");
		panel_7.add(label);
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setFont(new Font("Times New Roman", Font.BOLD, 15));
		
		dateChooser = new JDateChooser();
		dateChooser.setDate(this.date);
		dateChooser.setMinSelectableDate(this.date);
		panel_7.add(dateChooser);
		dateChooser.setDateFormatString("dd-MM-yyyy");
		dateChooser.setBorder(new LineBorder(new Color(0, 0, 0)));
		
		JPanel panel_9 = new JPanel();
		panel_9.setBackground(Color.WHITE);
		panel.add(panel_9, "push ,grow");
		
		btnInsurance = new JButton("Add Insurance\r\n");
		btnInsurance.setIcon(new ImageIcon(this.getClass().getResource("/security.png")));
		btnInsurance.addActionListener(this);
		panel_9.setLayout(new GridLayout(0, 4, 0, 0));
		btnInsurance.setForeground(Color.WHITE);
		btnInsurance.setBackground(new Color(100, 149, 237));
		btnInsurance.setFont(new Font("Times New Roman", Font.BOLD, 21));
		panel_9.add(btnInsurance);
		
		JLabel label_2 = new JLabel("");
		panel_9.add(label_2);
		
		JLabel label_1 = new JLabel("");
		panel_9.add(label_1);
		
		JPanel panel_1 = new JPanel();
		panel_9.add(panel_1);
		panel_1.setLayout(new GridLayout(3, 0, 0, 0));
		
		JLabel lblBiltyStatus = new JLabel("Bilty Status");
		lblBiltyStatus.setForeground(Color.WHITE);
		lblBiltyStatus.setOpaque(true);
		lblBiltyStatus.setBackground(new Color(102, 205, 170));
		lblBiltyStatus.setFont(new Font("Times New Roman", Font.BOLD, 17));
		lblBiltyStatus.setHorizontalAlignment(SwingConstants.CENTER);
		panel_1.add(lblBiltyStatus);
		
		rdbtnReceive = new JRadioButton("Received");
		buttonGroup_2.add(rdbtnReceive);
		rdbtnReceive.setFont(new Font("Times New Roman", Font.BOLD, 14));
		panel_1.add(rdbtnReceive);
		
		JPanel panel_2 = new JPanel();
		panel_1.add(panel_2);
		panel_2.setLayout(new GridLayout(0, 2, 0, 0));
		
		rdbtnPend = new JRadioButton("Pending");
		panel_2.add(rdbtnPend);
		rdbtnPend.setSelected(true);
		buttonGroup_2.add(rdbtnPend);
		rdbtnPend.setFont(new Font("Times New Roman", Font.BOLD, 14));
	}
	
	InsuranceDialog dialog = new InsuranceDialog();
	
	private final ButtonGroup buttonGroup_2 = new ButtonGroup();
	private JButton btnNew;
	private JButton btnSave;
	private JButton btnPrint;
	private JButton btnCancel;
	private JComboBox cmbxConsignee;
	private JComboBox cmbxFStation;
	private JComboBox cmbxTStation;
	private JComboBox cmbxFrtype;
	private JCheckBox chckbxConsignor;
	private JCheckBox chckbxConsignee;
	private JCheckBox chckbxTranspoter;
	private JRadioButton rdbtnC;
	private JRadioButton rdbtnBank;
	private JRadioButton rdbtnReceive;
	private JRadioButton rdbtnPend;
	private JTextArea txt_area_desc;
	private JButton btnSearchGR;
	
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
		if (e.getSource()==btnInsurance) {
			try {
				if (dialog != null) {
					dialog.setInterface(this);
					dialog.setGr_num(txt_gr_num.getText());
					if (dialog.isDisplayable()) {
					dialog.setGr_num(txt_gr_num.getText());
					dialog.setVisible(true);
				}else {
					dialog.setGr_num(txt_gr_num.getText());
					dialog.setInterface(this);
					dialog.setVisible(true);
				}
				
			}else {
				dialog = new InsuranceDialog();
				dialog.setGr_num(txt_gr_num.getText());
				dialog.setVisible(true);
			}
				
			}catch (Exception ex) {
				ex.printStackTrace();
			}
			dialog.addWindowListener(new WindowAdapter() {
				@Override
				public void windowClosing(WindowEvent e) {
					dialog = null;
					
				}
			});
		}
		
		if (e.getSource()==btnNew) {
			bility_model = new BilityModel();
			clearFormFields();
		}
		if (e.getSource() == btnCancel) {
			bility_model = new BilityModel();
			this.dispose();
		}
		
		if (e.getSource() == btnPrint) {
			try {
				int gr = Integer.parseInt(txt_gr_num.getText());
				if (gr != 0) {
					new PdfModel().createBility(gr);
				}
			} catch (Exception e2) {
				showMessageDialog("Something went wrong "+e2);
			}
			
			
		}
		if (e.getSource() == btnSave) {
			if (!validateForm().isEmpty()) {
				showMessageDialog(validateForm());
			}else {
				entityDao.storeInDatabase(getBiltyObject(),Constants.SYNC_FAILED);	
				listener.onNewEntry(bility_model);
				bility_model = new BilityModel();
				showMessageDialog("Data Saved Successfully..!!");
			}
		}
		if (e.getSource() == btnUpdateStatus) {
			if (!validateForm().isEmpty()) {
				showMessageDialog(validateForm());
			}else {
				bility_model = getBiltyObject();
				entityDao.updateDatabase(bility_model);
				listener.onUpdateEntry(bility_model);
				bility_model = new BilityModel();
				showMessageDialog("Data Updated Successfully..!!");
			}
		}
		if (e.getSource()==btnSearchGR) {
			if (!txt_gr_num.getText().isEmpty()) {
//				System.out.println(txt_gr_num.getText().trim());
				searchGR(Integer.parseInt(txt_gr_num.getText().trim()));
			}else {
				showMessageDialog("Please Enter GR No. First..!!");
			}
			
			
		}
		
		if (e.getSource()== btnVehicleSearch) {
			if (!txt_veh_no.getSelectedItem().toString().isEmpty()) {
				setVehiclePanel();
			}else {
				showMessageDialog("Please Enter Vehicle Number First...!!!");
			}
		}
		if (e.getSource() == btnCalculate) {
			txt_grand_total.setText("0");
			updateGrandTotal(parseDouble(txt_fr_rate.getText()));
			updateGrandTotal(parseDouble(txt_cgst.getText()));
			updateGrandTotal(parseDouble(txt_sgst.getText()));
			updateGrandTotal(parseDouble(txt_checkExp.getText()));
			updateGrandTotal(parseDouble(txt_stat_charge.getText()));
			deductAdv(parseDouble(txt_adv_pay.getText()));
		}
		
		if (e.getSource() == btnWeightCharge) {
			double wt = parseDouble(txt_quantity.getText());
			double price = parseDouble(txt_weight.getText());
			price = wt * price;
			if (price >0) {
				updateGrandTotal(price);
				deductAdv(parseDouble(txt_adv_pay.getText()));
			}
		}
	}

	public  void searchGR(int gr) {
		if (gr>0) {
			BilityModel bilty = entityDao.getBilty(gr);
			
			if (bilty != null) {
				setBilityObject(bilty);
			}else {
				showMessageDialog("G R Number not exist in Database..!!");
			}
		}else {
			showMessageDialog("Enter a Valid GR Number..");
		}
		
	}
	EntityDao entityDao = new EntityDao();
	HashSet<String> list = entityDao.getColList(new String[] {"BilityModel","stat_from","stat_to"});
	private JTextField txt_chq_dd;
	private JDateChooser dcChqDate;
	private JButton btnVehicleSearch;
	private JButton btnCalculate;
	private JButton btnUpdateStatus;
	private JButton btnWeightCharge;
	
	private void setVehiclePanel() {
		String vehicle = txt_veh_no.getSelectedItem().toString();
		if (vehicle != null) {
			Vehicle v =  entityDao.getVehicle(vehicle);
			if (v != null) {
				txt_veh_ownr.setText(v.getOwner_name());
				txt_driver.setText(v.getDriver_name());
			}else {
				showMessageDialog("Please Enter a Valid Vehicle Number");
			}
		}
	}
	private BilityModel getBiltyObject() {
		bility_model.setGr_id(Integer.parseInt(txt_gr_num.getText()));
		bility_model.setGr_date(dateChooser.getDate());
		bility_model.setVeh_num(txt_veh_no.getSelectedItem().toString());
		Customer consignee = new Customer();
		consignee.setCustomer_name(cmbxConsignee.getSelectedItem().toString());
		consignee.setAddress(txt_consignee_add.getText());
		consignee.setGst_no(txt_consignee_gst.getText());
		bility_model.setConsignee(consignee);
		Customer consignor = new Customer();
		consignor.setCustomer_name(cmbxConsignor.getSelectedItem().toString());
		consignor.setAddress(txt_consignor_add.getText());
		consignor.setGst_no(txt_consignor_gst.getText());
		bility_model.setConsignor(consignor);
		String strFrom = cmbxFStation.getSelectedItem().toString();
		String strTo = cmbxTStation.getSelectedItem().toString();
		bility_model.setStat_from(strFrom);
		bility_model.setStat_to(strTo);
		list.add(strTo);
		list.add(strFrom);
		bility_model.setFre_type(cmbxFrtype.getSelectedItem().toString());
		bility_model.setFreight(Double.parseDouble(txt_fr_rate.getText()));
		bility_model.setCgst(Double.parseDouble(txt_cgst.getText()));
		bility_model.setSgst(Double.parseDouble(txt_sgst.getText()));
		bility_model.setChck_exp(Double.parseDouble(txt_checkExp.getText()));
		bility_model.setAdv_pay(Double.parseDouble(txt_adv_pay.getText()));
		bility_model.setStat_charge(Double.parseDouble(txt_stat_charge.getText()));
		bility_model.setGrand_total(Double.parseDouble(txt_grand_total.getText()));
		bility_model.setBalnce(Double.parseDouble(txt_balance.getText()));
		String gstpaidby="";
		if (chckbxConsignee.isSelected()) {
			gstpaidby = Constants.consignee;
		}
		if (chckbxConsignor.isSelected()) {
			gstpaidby = Constants.consignor;
		}
		if (chckbxTranspoter.isSelected()) {
			gstpaidby = Constants.transporter;
		}
		if (rdbtnReceive.isSelected()) {
			bility_model.setBilty_status(Constants.stat_receive);
		}else {
			bility_model.setBilty_status(Constants.stat_pend);
		}
		bility_model.setGst_paid_by(gstpaidby);
		String pay_mode="";
		if (rdbtnBank.isSelected()) {
			
			pay_mode = Constants.pay_bank;
			bility_model.setBank_name(txt_bank_name.getText());
			bility_model.setCh_dd_no(txt_chq_dd.getText());
			bility_model.setCh_dd_date(dcChqDate.getDate());
			
		}else {
			pay_mode = Constants.pay_cash;
		}
		String bilty_status = "";
		if (rdbtnReceive.isSelected()) {
			bilty_status = Constants.stat_receive;
		}else {
			bilty_status = Constants.stat_pend;
		}
		bility_model.setPay_mode(pay_mode);
		bility_model.setBilty_status(bilty_status);
		Lagguage lagguage = new Lagguage();
		lagguage.setItems_desc(txt_area_desc.getText());
		lagguage.setT_packages(Double.parseDouble(txt_packages.getText()));
		lagguage.setT_qty(Double.parseDouble(txt_quantity.getText()));
		lagguage.setT_wt(Double.parseDouble(txt_weight.getText()));
		bility_model.setLagguage(lagguage);
		lagguage.getBilityModels().add(bility_model);
		return bility_model;
	}
	
	private void setBilityObject(BilityModel model) {
		txt_gr_num.setText(String.valueOf(model.getGr_id()));
		dateChooser.setDate(model.getGr_date());
		txt_veh_no.setSelectedItem(model.getVeh_num());
		Customer consignee = model.getConsignee();
		cmbxConsignee.setSelectedItem(consignee.getCustomer_name());
		txt_consignee_add.setText(consignee.getAddress());
		txt_consignee_gst.setText(consignee.getGst_no());
		
		Customer consignor = model.getConsignor();
		cmbxConsignor.setSelectedItem(consignor.getCustomer_name());
		txt_consignor_add.setText(consignor.getAddress());
		txt_consignor_gst.setText(consignor.getGst_no());
		
		cmbxFStation.setSelectedItem(model.getStat_from());
		cmbxTStation.setSelectedItem(model.getStat_to());
		cmbxFrtype.setSelectedItem(model.getFre_type());
		txt_fr_rate.setText(String.format("%.2f", model.getFreight()));
		txt_cgst.setText(String.format("%.2f", model.getCgst()));
		txt_sgst.setText(String.format("%.2f", model.getSgst()));
		txt_checkExp.setText(String.format("%.2f", model.getChck_exp()));
		txt_adv_pay.setText(String.format("%.2f", model.getAdv_pay()));
		txt_stat_charge.setText(String.format("%.2f", model.getStat_charge()));
		txt_grand_total.setText(String.format("%.2f", model.getGrand_total()));
		txt_balance.setText(String.format("%.2f", model.getBalnce()));

		if (model.getGst_paid_by().equalsIgnoreCase(Constants.consignee)) {
			chckbxConsignee.setSelected(true);
		}
		if (model.getGst_paid_by().equalsIgnoreCase(Constants.consignor)) {
			chckbxConsignor.setSelected(true);
		}
		if (model.getGst_paid_by().equalsIgnoreCase(Constants.transporter)) {
			chckbxTranspoter.setSelected(true);
		}
		
		String pay_mode="";
		if (rdbtnBank.isSelected()) {
			pay_mode = Constants.pay_bank;
		}else {
			pay_mode = Constants.pay_cash;
		}
		
		if (model.getPay_mode().equalsIgnoreCase(Constants.pay_bank)) {
			rdbtnBank.setSelected(true);
			txt_bank_name.setText(model.getBank_name());
			txt_chq_dd.setText(model.getCh_dd_no());
			dcChqDate.setDate(model.getCh_dd_date());
		}else {
			rdbtnC.setSelected(true);
		}
		
		if (model.getBilty_status().equalsIgnoreCase(Constants.stat_receive)) {
			rdbtnReceive.setSelected(true);
		}else {
			rdbtnPend.setSelected(true);
		}
		Lagguage l = model.getLagguage();
		if (l != null) {
			txt_area_desc.setText(l.getItems_desc());
			txt_packages.setText(String.format("%.2f", l.getT_packages()));
			txt_quantity.setText(String.format("%.2f", l.getT_qty()));
			txt_weight.setText(String.format("%.2f", l.getT_wt()));
		}
		
		
		
	}
	@Override
	public void onSaveInsurance(Insurance insurance) {
		// TODO Auto-generated method stub
//		System.out.println(insurance);
		
		bility_model.setInsurance(insurance);
		dialog.dispose();
		dialog = null;
	}

	@Override
	public void onPressCancel() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onPressDelete() {
		// TODO Auto-generated method stub
		
	}
	
	private String validateForm() {
		StringBuilder err = new StringBuilder();
//		System.out.println("txt_veh_no.getSelectedItem()=="+txt_veh_no.getSelectedItem());
//		System.out.println("getSelectedIndex()=="+txt_veh_no.getSelectedIndex());
	
		if (txt_veh_no.getSelectedIndex()==0) {
			err.append("Please Enter Vehicle No.\n");
			txt_veh_no.requestFocus();
		}
		if (cmbxFStation.getSelectedItem().toString().equals("")) {
			err.append("Please Select Station From.\n");
		}
		if (cmbxTStation.getSelectedItem().toString().equals("")) {
			err.append("Please Select Station To.\n");
		}
		
		if (cmbxConsignee.getSelectedIndex() == -1&&txt_consignee_add.getText().isEmpty()) {
			err.append("Please Fill Consignee Name and Address.\n");
		}
		if (cmbxConsignor.getSelectedIndex() == -1&&txt_consignor_add.getText().isEmpty()) {
			err.append("Please Fill Consignor Name and Address.\n");
		}
		if (txt_fr_rate.getText().isEmpty()) {
			err.append("Please Enter Freight. \n");
			txt_fr_rate.requestFocus();
		}
		if (!chckbxConsignee.isSelected()&&!chckbxConsignor.isSelected()&&!chckbxTranspoter.isSelected()) {
			err.append("Please Select GST Paid By.\n");
		}
		if (!rdbtnBank.isSelected()&&!rdbtnC.isSelected()) {
			err.append("Please Select Mode of Payment.\n");
		}
		
		if (rdbtnBank.isSelected()) {
			if (txt_bank_name.getText().isEmpty()) {
				err.append("Please Enter Bank Name.\n");
			}
			if (txt_chq_dd.getText().isEmpty()) {
				err.append("Please Enter Cheque or DD No.\n ");
			}
			
		}
		if (txt_weight.getText().isEmpty()) {
			err.append("Please Enter Weight. \n");
			txt_weight.requestFocus();
		}
		
		return err.toString();
	}
	
	private void showMessageDialog(String message) {
		JOptionPane.showMessageDialog(null, message);
	}

	public void clearFormFields() {
		txt_veh_no.setSelectedIndex(-1);
		txt_veh_ownr.setText("");
		txt_driver.setText("");
		cmbxConsignor.setSelectedIndex(-1);
		txt_consignor_add.setText("");
		txt_consignor_gst.setText("");
		cmbxConsignee.setSelectedIndex(-1);
		txt_consignee_add.setText("");
		txt_consignee_gst.setText("");
		txt_fr_rate.setText("");
		txt_cgst.setText("");
		txt_sgst.setText("");
		txt_checkExp.setText("");
		txt_adv_pay.setText("");
		txt_grand_total.setText("");
		txt_balance.setText("");
		txt_packages.setText("");
		txt_quantity.setText("");
		txt_weight.setText("");
		txt_area_desc.setText("");
		rdbtnPend.setSelected(true);
		increaseGR();
		dateChooser.setDate(this.date);
		cmbxFStation.setModel(new DefaultComboBoxModel<>(list.toArray()));
		cmbxTStation.setModel(new DefaultComboBoxModel<>(list.toArray()));
	}

	private void increaseGR() {
		txt_gr_num.setText(String.valueOf(entityDao.getMaxCount("bility_table")));
	}


	
	
	
	private Double parseDouble(String data) {
		double amt = 0.00;
		if (!data.isEmpty()) {
			amt = Double.parseDouble(data);
		}
		return amt;
	}
	
	private void updateGrandTotal(double price) {
//		System.out.println("price i get "+price);
		double temp  =0;
		if (!txt_grand_total.getText().isEmpty()) {
			temp = parseDouble(txt_grand_total.getText());
		}
		
		temp += price;
		txt_grand_total.setText(String.format("%.2f", temp));
	}
	//method for calculating grandTotal
	
	private void deductAdv(double price) {
		double temp =0;
		if (!txt_grand_total.getText().isEmpty()) {
			temp = parseDouble(txt_grand_total.getText());
		}
		temp -= price;
		txt_balance.setText(String.format("%.2f", temp));
	}
}
