package com.javacore.transportMgmt;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;
import java.util.Date;

import javax.swing.border.MatteBorder;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import java.awt.Font;

import com.javacore.transportMgmt.db.EntityDao;
import com.javacore.transportMgmt.model.OwnerDetails;
import com.toedter.calendar.JDateChooser;

public class Config extends JDialog implements ActionListener {

	private final JPanel contentPanel = new JPanel();
	private JTextField txt_name;
	private JTextField txt_username;
	private JTextField txt_password;
	private JDateChooser dcSubFrom;
	private JDateChooser dcSubTo;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			Config dialog = new Config();
			
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public Config() {
		setTitle("Configuration");
		setIconImage(new ImageIcon(this.getClass().getResource("/success.png")).getImage());
		setBounds(100, 100, 535, 523);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(29, 63, 459, 378);
		contentPanel.add(panel);
		panel.setLayout(new GridLayout(0, 2, 0, 30));
		
		JLabel lblNewLabel = new JLabel("ID");
		lblNewLabel.setFont(new Font("Arial", Font.BOLD, 20));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		panel.add(lblNewLabel);
		
		JPanel panel_1 = new JPanel();
		panel.add(panel_1);
		panel_1.setLayout(null);
		
		txt_ownerid = new JTextField();
		txt_ownerid.setText("1");
		txt_ownerid.setBounds(0, 0, 167, 38);
		panel_1.add(txt_ownerid);
		txt_ownerid.setFont(new Font("Arial", Font.PLAIN, 15));
		txt_ownerid.setColumns(10);
		
		btnSearch = new JButton("");
		btnSearch.addActionListener(this);
		btnSearch.setBackground(new Color(32, 178, 170));
		btnSearch.setIcon(new ImageIcon(this.getClass().getResource("/seek.png")));
		btnSearch.setFont(new Font("Arial", Font.BOLD, 12));
		btnSearch.setBounds(168, 0, 61, 38);
		panel_1.add(btnSearch);
		
		JLabel label = new JLabel("Owner Name");
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setFont(new Font("Arial", Font.BOLD, 20));
		panel.add(label);
		
		txt_name = new JTextField();
		txt_name.setFont(new Font("Arial", Font.PLAIN, 15));
		panel.add(txt_name);
		txt_name.setColumns(10);
		
		JLabel lblUsername = new JLabel("Username");
		lblUsername.setFont(new Font("Arial", Font.BOLD, 20));
		lblUsername.setHorizontalAlignment(SwingConstants.CENTER);
		panel.add(lblUsername);
		
		txt_username = new JTextField();
		txt_username.setFont(new Font("Arial", Font.PLAIN, 15));
		txt_username.setColumns(10);
		panel.add(txt_username);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setFont(new Font("Arial", Font.BOLD, 20));
		lblPassword.setHorizontalAlignment(SwingConstants.CENTER);
		panel.add(lblPassword);
		
		txt_password = new JTextField();
		txt_password.setFont(new Font("Arial", Font.PLAIN, 15));
		txt_password.setColumns(10);
		panel.add(txt_password);
		
		JLabel lblSubscriptionFrom = new JLabel("Subscription from");
		lblSubscriptionFrom.setFont(new Font("Arial", Font.BOLD, 20));
		lblSubscriptionFrom.setHorizontalAlignment(SwingConstants.CENTER);
		panel.add(lblSubscriptionFrom);
		
		dcSubFrom = new JDateChooser();
		dcSubFrom.setDateFormatString("dd-MM-yyyy");
		panel.add(dcSubFrom);
		
		JLabel lblSubscriptionTill = new JLabel("Subscription Till");
		lblSubscriptionTill.setFont(new Font("Arial", Font.BOLD, 20));
		lblSubscriptionTill.setHorizontalAlignment(SwingConstants.CENTER);
		panel.add(lblSubscriptionTill);
		
		dcSubTo = new JDateChooser();
		dcSubTo.setDateFormatString("dd-MM-yyyy");
		panel.add(dcSubTo);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				okButton = new JButton("OK");
				okButton.addActionListener(this);
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
				
			}
			{
				updateButton = new JButton("Update");
				updateButton.addActionListener(this);
				
				buttonPane.add(updateButton);
			}
		}
		validateData();
	}
	
	
	
	private Date date = new Date();
	private Calendar calendar = Calendar.getInstance();
	private EntityDao entityDao = new EntityDao();
	private JTextField txt_ownerid;
	private JButton btnSearch;
	private JButton updateButton;
	private JButton okButton;
	
	private OwnerDetails getOwnerObject() {
		OwnerDetails ownerDetails = new OwnerDetails();
		ownerDetails.setO_id(Integer.parseInt(txt_ownerid.getText()));
		ownerDetails.setName(txt_name.getText());
		ownerDetails.setUsername(txt_username.getText());
		ownerDetails.setPassword(txt_password.getText());
		ownerDetails.setSubscriptionFrom(dcSubFrom.getDate());
		ownerDetails.setSubscriptionTill(dcSubTo.getDate());
		return ownerDetails;
	}
	
	private void setFormDetails(OwnerDetails o) {
		
		if (o != null) {
			txt_name.setText(o.getName());
			txt_username.setText(o.getUsername());
			txt_password.setText(o.getPassword());
			dcSubFrom.setDate(o.getSubscriptionFrom());
			dcSubTo.setDate(o.getSubscriptionTill());
		}else {
			JOptionPane.showMessageDialog(null, "User doesnot exist..","Alert",JOptionPane.WARNING_MESSAGE);
		}
	}
	
	private String validateData() {
		StringBuilder sb = new StringBuilder();
		if (txt_name.getText().isEmpty()) {
			sb.append("Enter Owner Name. \n");
		}
		if (txt_username.getText().isEmpty()) {
			sb.append("Enter username.\n ");
		}
		
		if (txt_password.getText().isEmpty()) {
			sb.append("Enter Password.\n");
		}
		calendar.setTime(this.date);
		calendar.add(Calendar.YEAR, 1);
		System.out.println(calendar.get(Calendar.YEAR));
		System.out.println(calendar.get(Calendar.DAY_OF_MONTH));
		System.out.println(calendar.get(Calendar.MONTH));
		dcSubFrom.setDate(date);
		
		dcSubTo.setDate(calendar.getTime());
		return sb.toString();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if (e.getSource()== btnSearch) {
			if (!txt_ownerid.getText().isEmpty()) {
				int id = Integer.parseInt(txt_ownerid.getText());
				setFormDetails((OwnerDetails)entityDao.getObject(OwnerDetails.class, id));
			}else {
				JOptionPane.showMessageDialog(null, validateData(),"Alert",JOptionPane.WARNING_MESSAGE);
			}
		}
		
		if (e.getSource() == okButton) {
			if (validateData().isEmpty()) {
				entityDao.storeInDatabase(getOwnerObject());
				JOptionPane.showMessageDialog(null, "User Created","Alert",JOptionPane.INFORMATION_MESSAGE);
			}else {
				JOptionPane.showMessageDialog(null, validateData(),"Alert",JOptionPane.WARNING_MESSAGE);
			}
		}
		
		if (e.getSource() == updateButton) {
			entityDao.updateDatabase(getOwnerObject());
		}
	}
}
