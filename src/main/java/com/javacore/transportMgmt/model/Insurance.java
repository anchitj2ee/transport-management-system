package com.javacore.transportMgmt.model;

import java.util.Date;


public class Insurance {


	private int in_id;
	private int gr_num;

	private String company_name;

	private String policy_num;
	private Date idate;
	private double amount;
	private String risk;
	public int getIn_id() {
		return in_id;
	}
	public void setIn_id(int in_id) {
		this.in_id = in_id;
	}
	public int getGr_num() {
		return gr_num;
	}
	public void setGr_num(int gr_num) {
		this.gr_num = gr_num;
	}
	public String getCompany_name() {
		return company_name;
	}
	public void setCompany_name(String company_name) {
		this.company_name = company_name;
	}
	public String getPolicy_num() {
		return policy_num;
	}
	public void setPolicy_num(String policy_num) {
		this.policy_num = policy_num;
	}
	public Date getIdate() {
		return idate;
	}
	public void setIdate(Date idate) {
		this.idate = idate;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getRisk() {
		return risk;
	}
	public void setRisk(String risk) {
		this.risk = risk;
	}
	@Override
	public String toString() {
		return "Insurance [in_id=" + in_id + ", gr_num=" + gr_num + ", company_name=" + company_name + ", policy_num="
				+ policy_num + ", idate=" + idate + ", amount=" + amount + ", risk=" + risk + "]";
	}
	
	
}
