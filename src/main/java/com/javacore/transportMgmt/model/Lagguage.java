package com.javacore.transportMgmt.model;

import java.util.ArrayList;
import java.util.List;




public class Lagguage {


	private int l_id;
	
	private String items_desc;

	private Double t_packages;

	private Double t_qty;

	private Double t_wt;

	private List<BilityModel> bilityModels = new ArrayList<>();
	
	
	
	
	public int getL_id() {
		return l_id;
	}
	public void setL_id(int l_id) {
		this.l_id = l_id;
	}
	public List<BilityModel> getBilityModels() {
		return bilityModels;
	}
	public void setBilityModels(List<BilityModel> bilityModels) {
		this.bilityModels = bilityModels;
	}
	
	public String getItems_desc() {
		return items_desc;
	}
	public void setItems_desc(String items_desc) {
		this.items_desc = items_desc;
	}
	public Double getT_packages() {
		return t_packages;
	}
	public void setT_packages(Double t_packages) {
		this.t_packages = t_packages;
	}
	public Double getT_qty() {
		return t_qty;
	}
	public void setT_qty(Double t_qty) {
		this.t_qty = t_qty;
	}
	public Double getT_wt() {
		return t_wt;
	}
	public void setT_wt(Double t_wt) {
		this.t_wt = t_wt;
	}
	@Override
	public String toString() {
		return  " items_desc=" + items_desc + ", t_packages=" + t_packages
				+ ", t_qty=" + t_qty + ", t_wt=" + t_wt + "]";
	}
	
	
}
