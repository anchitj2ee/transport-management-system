package com.javacore.transportMgmt.model;

import java.util.Date;






public class OwnerDetails {

	private int o_id;
	private String name;
	private String username;
	private String password;
	private Date subscriptionFrom;
	private Date subscriptionTill;
	public int getO_id() {
		return o_id;
	}
	public void setO_id(int o_id) {
		this.o_id = o_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Date getSubscriptionFrom() {
		return subscriptionFrom;
	}
	public void setSubscriptionFrom(Date subscriptionFrom) {
		this.subscriptionFrom = subscriptionFrom;
	}
	public Date getSubscriptionTill() {
		return subscriptionTill;
	}
	public void setSubscriptionTill(Date subscriptionTill) {
		this.subscriptionTill = subscriptionTill;
	}
	@Override
	public String toString() {
		return "OwnerDetails [o_id=" + o_id + ", name=" + name + ", username=" + username + ", password=" + password
				+ ", subscriptionFrom=" + subscriptionFrom + ", subscriptionTill=" + subscriptionTill + "]";
	}
	
	
	
}
