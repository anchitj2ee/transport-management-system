package com.javacore.transportMgmt.model;
import java.util.Date;

public class CreateBillEntry {
	
	private int s_id;
	private Date bill_date;
	private Vehicle vehicle;
	private String station;
	private String bid_id;
	private String att_gr;
	private String nug_cb;
	private String weight;
	private String rate; 
	private double amount;
	private double total_amt;
	
	public int getS_id() {
		return s_id;
	}
	public void setS_id(int s_id) {
		this.s_id = s_id;
	}
	
	
	public Date getBill_date() {
		return bill_date;
	}
	public void setBill_date(Date bill_date) {
		this.bill_date = bill_date;
	}
	public Vehicle getVehicle() {
		return vehicle;
	}
	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}
	public String getStation() {
		return station;
	}
	public void setStation(String station) {
		this.station = station;
	}
	public String getBid_id() {
		return bid_id;
	}
	public void setBid_id(String bid_id) {
		this.bid_id = bid_id;
	}
	public String getAtt_gr() {
		return att_gr;
	}
	public void setAtt_gr(String att_gr) {
		this.att_gr = att_gr;
	}
	public String getNug_cb() {
		return nug_cb;
	}
	public void setNug_cb(String nug_cb) {
		this.nug_cb = nug_cb;
	}
	public String getWeight() {
		return weight;
	}
	public void setWeight(String weight) {
		this.weight = weight;
	}
	public String getRate() {
		return rate;
	}
	public void setRate(String rate) {
		this.rate = rate;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public double getTotal_amt() {
		return total_amt;
	}
	public void setTotal_amt(double total_amt) {
		this.total_amt = total_amt;
	}
	
	
	
	@Override
	public String toString() {
		return "CreateBill [s_id=" +s_id +", bill_date=" + bill_date + ", vehicle=" + vehicle + ", station=" + station
				+ ", bid_id=" + bid_id + ", att_gr=" + att_gr + ", nug_cb=" + nug_cb + ", weight=" + weight + ", rate="
				+ rate + ", amount=" + amount + ", total_amt=" + total_amt + "]";
	}
	
	
	
}
