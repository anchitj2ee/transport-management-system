package com.javacore.transportMgmt.model;




public class Vehicle {

	int v_id;

	String v_num;

	String owner_name;

	String owner_mob;
	

	String driver_name;

	String driver_mob;

	String v_type;

	String v_model;
	
	String v_engine, v_chasis;

	String ins_from, ins_to;

	String fit_form, fit_to;

	String per_form, per_to;

	String ser_form,ser_to;

	String author, rd_tax, pollute;
	
	
	public String getDriver_name() {
		return driver_name;
	}
	public void setDriver_name(String driver_name) {
		this.driver_name = driver_name;
	}
	public String getDriver_mob() {
		return driver_mob;
	}
	public void setDriver_mob(String driver_mob) {
		this.driver_mob = driver_mob;
	}
	public String getOwner_name() {
		return owner_name;
	}
	public void setOwner_name(String owner_name) {
		this.owner_name = owner_name;
	}
	
	
	public String getOwner_mob() {
		return owner_mob;
	}
	public void setOwner_mob(String owner_mob) {
		this.owner_mob = owner_mob;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getRd_tax() {
		return rd_tax;
	}
	public void setRd_tax(String rd_tax) {
		this.rd_tax = rd_tax;
	}
	public String getPollute() {
		return pollute;
	}
	public void setPollute(String pollute) {
		this.pollute = pollute;
	}
	public int getV_id() {
		return v_id;
	}
	public void setV_id(int v_id) {
		this.v_id = v_id;
	}
	public String getV_num() {
		return v_num;
	}
	public void setV_num(String v_num) {
		this.v_num = v_num;
	}
	public String getV_type() {
		return v_type;
	}
	public void setV_type(String v_type) {
		this.v_type = v_type;
	}
	public String getV_model() {
		return v_model;
	}
	public void setV_model(String v_model) {
		this.v_model = v_model;
	}
	public String getV_engine() {
		return v_engine;
	}
	public void setV_engine(String v_engine) {
		this.v_engine = v_engine;
	}
	public String getV_chasis() {
		return v_chasis;
	}
	public void setV_chasis(String v_chasis) {
		this.v_chasis = v_chasis;
	}
	public String getIns_from() {
		return ins_from;
	}
	public void setIns_from(String ins_from) {
		this.ins_from = ins_from;
	}
	public String getIns_to() {
		return ins_to;
	}
	public void setIns_to(String ins_to) {
		this.ins_to = ins_to;
	}
	public String getFit_form() {
		return fit_form;
	}
	public void setFit_form(String fit_form) {
		this.fit_form = fit_form;
	}
	public String getFit_to() {
		return fit_to;
	}
	public void setFit_to(String fit_to) {
		this.fit_to = fit_to;
	}
	public String getPer_form() {
		return per_form;
	}
	public void setPer_form(String per_form) {
		this.per_form = per_form;
	}
	public String getPer_to() {
		return per_to;
	}
	public void setPer_to(String per_to) {
		this.per_to = per_to;
	}
	public String getSer_form() {
		return ser_form;
	}
	public void setSer_form(String ser_form) {
		this.ser_form = ser_form;
	}
	public String getSer_to() {
		return ser_to;
	}
	public void setSer_to(String ser_to) {
		this.ser_to = ser_to;
	}
	@Override
	public String toString() {
		return "Vehicle [v_id=" + v_id + ", v_num=" + v_num + ", v_type=" + v_type + ", v_model=" + v_model
				+ ", v_engine=" + v_engine + ", v_chasis=" + v_chasis + ", ins_from=" + ins_from + ", ins_to=" + ins_to
				+ ", fit_form=" + fit_form + ", fit_to=" + fit_to + ", per_form=" + per_form + ", per_to=" + per_to
				+ ", ser_form=" + ser_form + ", ser_to=" + ser_to + "]";
	}
	
	
	

}
