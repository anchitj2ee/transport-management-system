package com.javacore.transportMgmt.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;



public class Bill {

	
	private int bill_id;
	private Date bill_date;
	
	private Customer client;
	
	
	private List<CreateBillEntry> entries = new ArrayList<>();
	
	private Double total_amt;
	
	
	public int getBill_id() {
		return bill_id;
	}
	public void setBill_id(int bill_id) {
		this.bill_id = bill_id;
	}
	public Date getBill_date() {
		return bill_date;
	}
	public void setBill_date(Date bill_date) {
		this.bill_date = bill_date;
	}
	public Customer getClient() {
		return client;
	}
	public void setClient(Customer client) {
		this.client = client;
	}
	
	public List<CreateBillEntry> getEntries() {
		return entries;
	}
	public void setEntries(List<CreateBillEntry> entries) {
		this.entries = entries;
	}
	public Double getTotal_amt() {
		return total_amt;
	}
	public void setTotal_amt(Double total_amt) {
		this.total_amt = total_amt;
	}
	
	
}
