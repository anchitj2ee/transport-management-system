package com.javacore.transportMgmt.model;



public class Customer {
//this class behaves both as a consignor and consignee

	private String customer_name;

	private String address;

	private String gst_no;
	public String getCustomer_name() {
		return customer_name;
	}
	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getGst_no() {
		return gst_no;
	}
	public void setGst_no(String gst_no) {
		this.gst_no = gst_no;
	}
	@Override
	public String toString() {
		return "Customer [customer_name=" + customer_name + ", address=" + address + ", gst_no=" + gst_no + "]";
	}
	
	
}
