package com.javacore.transportMgmt.model;

public interface Constants {

	public static final String consignor = "Consignor";
	public static final String consignee = "Consignee";
	public static final String transporter = "Transporter";
	public static final String stat_receive = "Received";
	public static final String stat_pend = "Pending";
	public static final String pay_cash = "Cash";
	public static final String pay_bank = "Bank";
	public static final String ins_exp = "Insurance Expires on ";
	public static final String fit_exp = "Fitness Expires on ";
	public static final String permit = "Permit Expires on ";
	public static final String service = "Next Service on ";
	public static final String auth = "Authorization Expires on ";
	public static final String rd_tax = "Road Tax Expires on ";
	public static final String pollute = "Pollution Expires on ";
	
	public static final String SELECT_VEHICLE = "Select a Vehicle";
	
	public static final String ins_exp_0 = "Insurance Expired on ";
	public static final String fit_exp_0 = "Fitness Expired on ";
	public static final String permit_0 = "Permit Expired on ";
	public static final String service_0 = "Next Service on ";
	public static final String auth_0 = "Authorization Expired on ";
	public static final String rd_tax_0 = "Road Tax Expired on ";
	public static final String pollute_0 = "Pollution Expired on ";
	public static final int SYNC_OK = 1;
	public static final int SYNC_FAILED = 0;
	
}
