package com.javacore.transportMgmt.model;

import java.util.Date;

public class BilityModel {

	
	private int gr_id;
	private Date gr_date;
	
	private Insurance insurance;
	
	private String veh_num;
	
	private String stat_from;
	
	private String stat_to;
	
	private Customer consignor;
	
	private Customer consignee;
	
	private String fre_type;
	
	private Double freight;
	private Double cgst,sgst,chck_exp,adv_pay,stat_charge,grand_total,balnce;
	
	private String gst_paid_by;
	
	
	private Lagguage Lagguage;
	
	private String pay_mode;
	
	
	public String bilty_status;
	
	private String bank_name;
	
	private String ch_dd_no;
	private Date ch_dd_date;
	
	
	public String getBank_name() {
		return bank_name;
	}
	public void setBank_name(String bank_name) {
		this.bank_name = bank_name;
	}
	public String getCh_dd_no() {
		return ch_dd_no;
	}
	public void setCh_dd_no(String ch_dd_no) {
		this.ch_dd_no = ch_dd_no;
	}
	public Date getCh_dd_date() {
		return ch_dd_date;
	}
	public void setCh_dd_date(Date ch_dd_date) {
		this.ch_dd_date = ch_dd_date;
	}
	public String getBilty_status() {
		return bilty_status;
	}
	public void setBilty_status(String bilty_status) {
		this.bilty_status = bilty_status;
	}
	public int getGr_id() {
		return gr_id;
	}
	public void setGr_id(int gr_id) {
		this.gr_id = gr_id;
	}
	public Date getGr_date() {
		return gr_date;
	}
	public void setGr_date(Date gr_date) {
		this.gr_date = gr_date;
	}
	public Insurance getInsurance() {
		return insurance;
	}
	public void setInsurance(Insurance insurance) {
		this.insurance = insurance;
	}
	public String getVeh_num() {
		return veh_num;
	}
	public void setVeh_num(String veh_num) {
		this.veh_num = veh_num;
	}
	public String getStat_from() {
		return stat_from;
	}
	public void setStat_from(String stat_from) {
		this.stat_from = stat_from;
	}
	public String getStat_to() {
		return stat_to;
	}
	public void setStat_to(String stat_to) {
		this.stat_to = stat_to;
	}
	public Customer getConsignor() {
		return consignor;
	}
	public void setConsignor(Customer consignor) {
		this.consignor = consignor;
	}
	public Customer getConsignee() {
		return consignee;
	}
	public void setConsignee(Customer consignee) {
		this.consignee = consignee;
	}
	public String getFre_type() {
		return fre_type;
	}
	public void setFre_type(String fre_type) {
		this.fre_type = fre_type;
	}
	public Double getFreight() {
		return freight;
	}
	public void setFreight(Double freight) {
		this.freight = freight;
	}
	public Double getCgst() {
		return cgst;
	}
	public void setCgst(Double cgst) {
		this.cgst = cgst;
	}
	public Double getSgst() {
		return sgst;
	}
	public void setSgst(Double sgst) {
		this.sgst = sgst;
	}
	public Double getChck_exp() {
		return chck_exp;
	}
	public void setChck_exp(Double chck_exp) {
		this.chck_exp = chck_exp;
	}
	public Double getAdv_pay() {
		return adv_pay;
	}
	public void setAdv_pay(Double adv_pay) {
		this.adv_pay = adv_pay;
	}
	public Double getStat_charge() {
		return stat_charge;
	}
	public void setStat_charge(Double stat_charge) {
		this.stat_charge = stat_charge;
	}
	public Double getGrand_total() {
		return grand_total;
	}
	public void setGrand_total(Double grand_total) {
		this.grand_total = grand_total;
	}
	public Double getBalnce() {
		return balnce;
	}
	public void setBalnce(Double balnce) {
		this.balnce = balnce;
	}
	public String getGst_paid_by() {
		return gst_paid_by;
	}
	public void setGst_paid_by(String gst_paid_by) {
		this.gst_paid_by = gst_paid_by;
	}
	public String getPay_mode() {
		return pay_mode;
	}
	public void setPay_mode(String pay_mode) {
		this.pay_mode = pay_mode;
	}

	public Lagguage getLagguage() {
		return Lagguage;
	}
	public void setLagguage(Lagguage lagguage) {
		Lagguage = lagguage;
	}
	@Override
	public String toString() {
		return "BilityModel [gr_id=" + gr_id + ", gr_date=" + gr_date + ", insurance=" + insurance + ", veh_num="
				+ veh_num + ", stat_from=" + stat_from + ", stat_to=" + stat_to + ", consignor=" + consignor
				+ ", consignee=" + consignee + ", fre_type=" + fre_type + ", freight=" + freight + ", cgst=" + cgst
				+ ", sgst=" + sgst + ", chck_exp=" + chck_exp + ", adv_pay=" + adv_pay + ", stat_charge=" + stat_charge
				+ ", grand_total=" + grand_total + ", balnce=" + balnce + ", gst_paid_by=" + gst_paid_by + ", Lagguage="
				+ Lagguage + ", pay_mode=" + pay_mode + ", bilty_status=" + bilty_status + ", bank_name=" + bank_name
				+ ", ch_dd_no=" + ch_dd_no + ", ch_dd_date=" + ch_dd_date + "]";
	}


	
	
	
	
}
