package com.javacore.transportMgmt;

import java.awt.Dialog.ModalityType;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.EventQueue;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JDialog;
import javax.swing.UIManager;

import com.javacore.transportMgmt.LoginFrame.LoginResponse;

public class App implements LoginResponse	 
{
	private static LoginFrame jf;
	
    public static void main( String[] args )
    {
    	
    	jf = new LoginFrame();
    	jf.setModal(true);
    	jf.setAlwaysOnTop(true);
    	jf.setModalityType(ModalityType.APPLICATION_MODAL);
    	jf.setInterface(new App());
    	jf.setVisible(true);
        
        
    }

	@Override
	public void onLoginSuccess() {
		// TODO Auto-generated method stub
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow frame = new MainWindow();
					frame.setVisible(true);
					if (frame.isVisible()) {
						System.out.println("I'm Active now");
						jf.dispose();
					}
					
					
					frame.addWindowListener(new WindowAdapter() {
						@Override
						public void windowClosing(WindowEvent e) {
							// TODO Auto-generated method stub
							System.exit(0);
						}
					});
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	@Override
	public void onLoginFailure() {
		// TODO Auto-generated method stub
		
	}
	
	
}
