package com.javacore.transportMgmt;

import java.awt.BorderLayout;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JTabbedPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JButton;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.swing.SwingConstants;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.javacore.transportMgmt.VehicleEntry.VehicleListener;
import com.javacore.transportMgmt.db.EntityDao;
import com.javacore.transportMgmt.db.HibernateUtils;
import com.javacore.transportMgmt.db.MySingleton;
import com.javacore.transportMgmt.model.BilityModel;
import com.javacore.transportMgmt.model.Bill;
import com.javacore.transportMgmt.model.Constants;
import com.javacore.transportMgmt.model.CreateBillEntry;
import com.javacore.transportMgmt.model.Customer;
import com.javacore.transportMgmt.model.Lagguage;
import com.javacore.transportMgmt.model.Vehicle;
import com.javacore.transportMgmt.utility.AutoComplete;
import com.javacore.transportMgmt.utility.MyTableModel;
import com.javacore.transportMgmt.utility.PdfModel;
import com.javacore.transportMgmt.utility.RowData;
import com.toedter.calendar.JDateChooser;
import javax.swing.border.LineBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import java.awt.GridLayout;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.MutableComboBoxModel;

public class Dashboard extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTable receipt_table;
	private JPanel panel;
	private JTextField txt_bill_no;
	private JTextField txt_frm_add;
	private JTextField txt_bid;
	private JTextField txt_nug;
	private JTextField txt_rate_per;
	private JTextField txt_stat_1;
	private JTextField txt_gr_no;
	private JTextField txt_wt;
	private JTextField txt_item_amt;
	private JTextField txt_total_amt;
	private DefaultTableModel billHistoryModel;
	public static final boolean DEBUG = true;
	// "select max(bill_id) from "+
	public Dashboard() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 1132, 625);
		contentPane = new JPanel();
		setTitle(
				"CHAUHAN FRIEGHT CARRIERS                                                                                 FREIGHT BILL");

		setIconImage(new ImageIcon(this.getClass().getResource("/success.png")).getImage());
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setMenuBar();
		setContentPane(contentPane);
		date = new Date();
		dateFormat = new SimpleDateFormat("dd-MM-yyyy");

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(gl_contentPane.createParallelGroup(Alignment.LEADING).addComponent(tabbedPane,
				GroupLayout.DEFAULT_SIZE, 1106, Short.MAX_VALUE));
		gl_contentPane.setVerticalGroup(gl_contentPane.createParallelGroup(Alignment.LEADING).addComponent(tabbedPane,
				Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 566, Short.MAX_VALUE));

		setPanelONE(tabbedPane, gl_contentPane);

		JPanel panel_bill_history = new JPanel();
		tabbedPane.addTab("Bill History", null, panel_bill_history, null);

		JPanel panel_1 = new JPanel();

		JPanel panel_2 = new JPanel();
		GroupLayout gl_panel_bill_history = new GroupLayout(panel_bill_history);
		gl_panel_bill_history.setHorizontalGroup(gl_panel_bill_history.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, gl_panel_bill_history.createSequentialGroup().addContainerGap()
						.addGroup(gl_panel_bill_history.createParallelGroup(Alignment.TRAILING)
								.addComponent(panel_2, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 1077,
										Short.MAX_VALUE)
								.addComponent(panel_1, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 1077,
										Short.MAX_VALUE))
						.addContainerGap()));
		gl_panel_bill_history.setVerticalGroup(gl_panel_bill_history.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_bill_history.createSequentialGroup().addContainerGap()
						.addComponent(panel_1, GroupLayout.PREFERRED_SIZE, 37, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addComponent(panel_2, GroupLayout.DEFAULT_SIZE, 476, Short.MAX_VALUE).addContainerGap()));

		btnSelectARow = new JButton("Select a Row");
		btnSelectARow.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				new Thread(new Runnable() {

					@Override
					public void run() {

						int row = (Integer) billHistoryModel.getValueAt(historyTable.getSelectedRow(), 0);
						if (row > 0)
							new PdfModel().createBill(row);

					}
				}).start();
			}
		});
		btnSelectARow.setForeground(Color.WHITE);
		btnSelectARow.setFont(new Font("Dialog", Font.BOLD, 12));
		btnSelectARow.setBackground(new Color(102, 205, 170));
		GroupLayout gl_panel_1 = new GroupLayout(panel_1);
		gl_panel_1.setHorizontalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, gl_panel_1.createSequentialGroup()
					.addContainerGap(908, Short.MAX_VALUE)
					.addComponent(btnSelectARow, GroupLayout.PREFERRED_SIZE, 135, GroupLayout.PREFERRED_SIZE)
					.addGap(34))
		);
		gl_panel_1.setVerticalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, gl_panel_1.createSequentialGroup()
					.addContainerGap()
					.addComponent(btnSelectARow, GroupLayout.DEFAULT_SIZE, 37, Short.MAX_VALUE))
		);
		panel_1.setLayout(gl_panel_1);
		panel_2.setLayout(new BorderLayout(0, 0));

		billHistoryTable(panel_2);
		panel_bill_history.setLayout(gl_panel_bill_history);
		updateCustomerList(null);

	}

	private class RowListener implements ListSelectionListener {

		@Override
		public void valueChanged(ListSelectionEvent e) {
			// TODO Auto-generated method stub
			if (e.getValueIsAdjusting()) {
				return;
			}
			if (historyTable.getSelectedRow() == -1) {
				return;
			}
			btnSelectARow.setText("Print");
		}
	}

	String col2[] = { "Bill No.", "Date", "Cust. Name", "Address", "Total Amt" };

	private void billHistoryTable(JPanel panel_2) {
		JScrollPane scrollPane = new JScrollPane();
		panel_2.add(scrollPane, BorderLayout.CENTER);
		billHistoryModel = new DefaultTableModel() {
			public Class getColumnClass(int column) {
				return getValueAt(0, column).getClass();
			}
		};
		billHistoryModel.setColumnIdentifiers(col2);
		historyTable = new JTable();
		try {

			for (Bill obj : bills) {
				setBillHistoryRow(obj);
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		historyTable.setModel(billHistoryModel);
		JTableHeader h2 = historyTable.getTableHeader();
		scrollPane.setViewportView(historyTable);
		historyTable.setFont(new Font("Arial", Font.PLAIN, 14));
		h2.setFont(new Font("Arial", Font.BOLD, 14));
		historyTable.setRowHeight(20);
		historyTable.getColumnModel().getColumn(0).setMaxWidth(60);
		historyTable.getColumnModel().getColumn(1).setMinWidth(120);
		historyTable.getColumnModel().getColumn(1).setMaxWidth(200);
		historyTable.getSelectionModel().addListSelectionListener(new RowListener());
		panel_2.add(BorderLayout.NORTH, h2);
	}

	private void setPanelONE(JTabbedPane tabbedPane, GroupLayout gl_contentPane) {
		panel = new JPanel();
		panel.setBackground(Color.WHITE);
		tabbedPane.addTab("New Receipt", null, panel, null);

		JPanel panel_1 = new JPanel();
		panel_1.setBackground(new Color(224, 255, 255));

		panel_4 = new JPanel();
		panel_4.setBackground(Color.WHITE);

		btnCreateReceipt = new JButton("Create Receipt");
		btnCreateReceipt.addActionListener(this);
		btnCreateReceipt.setForeground(Color.WHITE);
		btnCreateReceipt.setBackground(new Color(102, 205, 170));

		btnClearInvoice = new JButton("Clear Invoice");
		btnClearInvoice.addActionListener(this);
		btnClearInvoice.setForeground(Color.WHITE);
		btnClearInvoice.setBackground(new Color(102, 205, 170));
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(gl_panel.createParallelGroup(Alignment.LEADING).addGroup(gl_panel
				.createSequentialGroup().addContainerGap()
				.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addComponent(panel_1, GroupLayout.DEFAULT_SIZE, 1077, Short.MAX_VALUE)
						.addGroup(gl_panel.createSequentialGroup()
								.addComponent(panel_4, GroupLayout.DEFAULT_SIZE, 940, Short.MAX_VALUE).addGap(18)
								.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
										.addComponent(btnCreateReceipt, GroupLayout.PREFERRED_SIZE, 118,
												GroupLayout.PREFERRED_SIZE)
										.addComponent(btnClearInvoice, GroupLayout.PREFERRED_SIZE, 118,
												GroupLayout.PREFERRED_SIZE))
								.addGap(1)))
				.addContainerGap()));
		gl_panel.setVerticalGroup(gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup().addContainerGap()
						.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
								.addComponent(panel_4, GroupLayout.PREFERRED_SIZE, 225, GroupLayout.PREFERRED_SIZE)
								.addGroup(gl_panel.createSequentialGroup().addComponent(btnCreateReceipt).addGap(6)
										.addComponent(btnClearInvoice)))
						.addComponent(panel_1, GroupLayout.DEFAULT_SIZE, 278, Short.MAX_VALUE)));
		panel_4.setLayout(null);

		setViewPanel2(panel_4);
		setReceiptTable(gl_contentPane, panel_1, gl_panel);
	}

	private void setViewPanel2(JPanel panel_2) {

		panel_3 = new JPanel();
		panel_3.setBackground(Color.WHITE);
		panel_3.setBounds(0, 12, 227, 202);
		panel_4.add(panel_3);
		panel_3.setLayout(new GridLayout(0, 2, 0, 0));
		JLabel label = new JLabel("Date");
		panel_3.add(label);
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setFont(new Font("Times New Roman", Font.BOLD, 15));

		dateChooser = new JDateChooser();
		dateChooser.setDate(this.date);
		// dateChooser.setMinSelectableDate(this.date);
		panel_3.add(dateChooser);
		dateChooser.setDateFormatString("dd-MM-yyyy");
		dateChooser.setBorder(new LineBorder(new Color(0, 0, 0)));

		JLabel label_3 = new JLabel("Bill No. :");
		panel_3.add(label_3);
		label_3.setHorizontalAlignment(SwingConstants.CENTER);
		label_3.setFont(new Font("Times New Roman", Font.BOLD, 15));

		txt_bill_no = new JTextField();
		panel_3.add(txt_bill_no);
		increaseBillCounter();
		txt_bill_no.setFont(new Font("Arial", Font.PLAIN, 14));
		txt_bill_no.setEditable(false);
		txt_bill_no.setColumns(10);

		JLabel label_5 = new JLabel("Truck No.");
		panel_3.add(label_5);
		label_5.setHorizontalAlignment(SwingConstants.CENTER);
		label_5.setFont(new Font("Times New Roman", Font.BOLD, 15));

		txt_truck_no = new AutoComplete(dao.getVehicleNames().toArray());
		txt_truck_no.setFont(new Font("Arial", Font.BOLD, 14));
		panel_3.add(txt_truck_no);
		txt_truck_no.setEditable(true);

		JLabel label_9 = new JLabel("Station");
		panel_3.add(label_9);
		label_9.setHorizontalAlignment(SwingConstants.CENTER);
		label_9.setFont(new Font("Times New Roman", Font.BOLD, 15));

		txt_stat_1 = new JTextField();
		txt_stat_1.setFont(new Font("Arial", Font.PLAIN, 14));
		panel_3.add(txt_stat_1);
		txt_stat_1.setColumns(10);

		JLabel label_15 = new JLabel("");
		panel_3.add(label_15);

		JPanel panel_8 = new JPanel();
		panel_8.setBounds(233, 12, 695, 57);
		panel_4.add(panel_8);
		panel_8.setLayout(new GridLayout(0, 2, 0, 0));

		JPanel panel_7 = new JPanel();
		panel_7.setBackground(Color.WHITE);
		panel_8.add(panel_7);
		panel_7.setLayout(null);

		JLabel label_1 = new JLabel("C. Name");
		label_1.setBounds(201, 1, 89, 25);
		panel_7.add(label_1);
		label_1.setHorizontalAlignment(SwingConstants.CENTER);
		label_1.setFont(new Font("Times New Roman", Font.BOLD, 15));

		JLabel label_2 = new JLabel("M/S");
		label_2.setBounds(295, 1, 40, 25);
		panel_7.add(label_2);
		label_2.setHorizontalAlignment(SwingConstants.CENTER);

		cmbClient = new AutoComplete(new Object[] {});
		cmbClient.setFont(new Font("Arial", Font.BOLD, 14));
		panel_8.add(cmbClient);
		cmbClient.setEditable(true);

		JLabel lblAddress = new JLabel("Address    ");
		lblAddress.setOpaque(true);
		lblAddress.setBackground(Color.WHITE);
		panel_8.add(lblAddress);
		lblAddress.setHorizontalAlignment(SwingConstants.RIGHT);
		lblAddress.setFont(new Font("Times New Roman", Font.BOLD, 15));

		txt_frm_add = new JTextField();
		txt_frm_add.setFont(new Font("Arial", Font.PLAIN, 14));
		panel_8.add(txt_frm_add);
		txt_frm_add.setColumns(10);

		JPanel panel_5 = new JPanel();
		panel_5.setBackground(Color.WHITE);
		panel_5.setBounds(256, 74, 481, 115);
		panel_4.add(panel_5);
		panel_5.setLayout(new GridLayout(0, 4, 0, 0));

		JLabel label_6 = new JLabel("BID ID No.");
		panel_5.add(label_6);
		label_6.setHorizontalAlignment(SwingConstants.CENTER);
		label_6.setFont(new Font("Times New Roman", Font.BOLD, 15));

		txt_bid = new JTextField();
		txt_bid.setFont(new Font("Arial", Font.PLAIN, 14));
		panel_5.add(txt_bid);
		txt_bid.setColumns(10);

		JLabel label_7 = new JLabel("Nug/C.B.");
		panel_5.add(label_7);
		label_7.setHorizontalAlignment(SwingConstants.CENTER);
		label_7.setFont(new Font("Times New Roman", Font.BOLD, 15));

		txt_nug = new JTextField();
		txt_nug.setFont(new Font("Arial", Font.PLAIN, 14));
		panel_5.add(txt_nug);
		txt_nug.setColumns(10);

		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.WHITE);
		panel_5.add(panel_1);
		panel_1.setLayout(new GridLayout(0, 1, 0, 0));

		JLabel label_10 = new JLabel("Attached");
		panel_1.add(label_10);
		label_10.setHorizontalAlignment(SwingConstants.CENTER);
		label_10.setFont(new Font("Times New Roman", Font.BOLD, 12));

		JLabel label_11 = new JLabel("G.R. No.");
		panel_1.add(label_11);
		label_11.setHorizontalAlignment(SwingConstants.CENTER);
		label_11.setFont(new Font("Times New Roman", Font.BOLD, 14));

		JPanel panel_9 = new JPanel();
		panel_5.add(panel_9);
		panel_9.setLayout(null);

		txt_gr_no = new JTextField();
		txt_gr_no.setBounds(0, 0, 72, 38);
		panel_9.add(txt_gr_no);
		txt_gr_no.setFont(new Font("Arial", Font.PLAIN, 14));
		txt_gr_no.addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
				if (e.getSource() == txt_gr_no && e.getKeyCode() == KeyEvent.VK_ENTER) {
					int gr = Integer.parseInt(txt_gr_no.getText());
					setBillEntries(gr);
				}
			}

			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub

			}
		});
		txt_gr_no.setColumns(10);

		btnSearchGR = new JButton("");
		btnSearchGR.setIcon(new ImageIcon(this.getClass().getResource("/seek.png")));
		btnSearchGR.addActionListener(this);
		btnSearchGR.setBackground(new Color(32, 178, 170));
		btnSearchGR.setBounds(74, 0, 46, 38);
		// btnSearchGR
		panel_9.add(btnSearchGR);

		JLabel label_12 = new JLabel("Weight (Kg)");
		panel_5.add(label_12);
		label_12.setHorizontalAlignment(SwingConstants.CENTER);
		label_12.setFont(new Font("Times New Roman", Font.BOLD, 15));

		txt_wt = new JTextField();
		txt_wt.setFont(new Font("Arial", Font.PLAIN, 14));
		panel_5.add(txt_wt);
		txt_wt.setColumns(10);

		JLabel label_20 = new JLabel("");
		panel_5.add(label_20);

		JLabel label_21 = new JLabel("");
		panel_5.add(label_21);

		JLabel label_22 = new JLabel("");
		panel_5.add(label_22);

		JPanel panel_6 = new JPanel();
		panel_6.setBackground(Color.WHITE);
		panel_6.setBounds(739, 75, 189, 138);
		panel_4.add(panel_6);
		panel_6.setLayout(new GridLayout(0, 2, 0, 0));

		JLabel label_8 = new JLabel("Rate");
		panel_6.add(label_8);
		label_8.setHorizontalAlignment(SwingConstants.CENTER);
		label_8.setFont(new Font("Times New Roman", Font.BOLD, 15));

		txt_rate_per = new JTextField();
		txt_rate_per.setFont(new Font("Arial", Font.PLAIN, 14));
		panel_6.add(txt_rate_per);
		txt_rate_per.setColumns(10);

		JLabel label_13 = new JLabel("Amount");
		panel_6.add(label_13);
		label_13.setHorizontalAlignment(SwingConstants.CENTER);
		label_13.setFont(new Font("Times New Roman", Font.BOLD, 15));

		txt_item_amt = new JTextField();
		panel_6.add(txt_item_amt);
		txt_item_amt.setFont(new Font("Arial", Font.PLAIN, 14));
		txt_item_amt.setColumns(10);

		JLabel label_19 = new JLabel("");
		panel_6.add(label_19);

		btnAddEntry = new JButton("Add Entry");
		btnAddEntry.setFont(new Font("Dialog", Font.BOLD, 12));
		btnAddEntry.addActionListener(this);
		panel_6.add(btnAddEntry);
		btnAddEntry.setForeground(Color.WHITE);
		btnAddEntry.setBackground(new Color(102, 205, 170));

		JLabel label_16 = new JLabel("Total Amount");
		panel_6.add(label_16);
		label_16.setHorizontalAlignment(SwingConstants.CENTER);
		label_16.setFont(new Font("Times New Roman", Font.BOLD, 15));

		txt_total_amt = new JTextField();
		panel_6.add(txt_total_amt);
		txt_total_amt.setFont(new Font("Arial", Font.PLAIN, 14));
		txt_total_amt.setColumns(10);

		JLabel label_14 = new JLabel("Onistech Info System \r\n");
		label_14.setHorizontalAlignment(SwingConstants.CENTER);
		label_14.setForeground(Color.BLUE);
		label_14.setFont(new Font("Times New Roman", Font.BOLD, 20));
		label_14.setBackground(Color.BLACK);
		label_14.setBounds(330, 187, 240, 25);
		panel_2.add(label_14);

		JLabel lblVersion = new JLabel("Version 1.1.0\r\n");
		lblVersion.setBounds(567, 188, 87, 25);
		panel_2.add(lblVersion);
	}
	private boolean checkModel(String med_name, MutableComboBoxModel<String> model) {
		for (int i = 0; i < model.getSize(); i++) {
			String str = model.getElementAt(i);
			if (str.equals(med_name)) {
				return true;
			}
		}
		return false;
	}
	private void updateCustomerList(Bill obj) {

		boolean addCustomer = false;
		MutableComboBoxModel<String> model = (MutableComboBoxModel<String>) cmbClient.getModel();
		for (Bill bill : bills) {
			if (bill != null) {
				String entry = bill.getClient().getCustomer_name();
				if(!checkModel(entry, model))
					model.addElement(entry);
				if (obj != null) {
					if (!checkModel(obj.getClient().getCustomer_name(), model)) {
						addCustomer = true;
					}
				}
			}

		}
		if (addCustomer) {
			model.addElement(obj.getClient().getCustomer_name());
		}
		bills.add(obj);
		cmbClient.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				String str = (String) cmbClient.getSelectedItem();
				if (str != null) {
					for (Bill bill : bills) {
						if (bill != null) {
							if (str.equalsIgnoreCase(bill.getClient().getCustomer_name())) {
								txt_frm_add.setText(bill.getClient().getAddress());
							} 
						}
					}
				}
			}
		});

	}

	private String[] cols = { "SI.NO.", "Date", "Truck No.", "Station", "BID ID No.", "Attached G.R. No.", "Nug/C.B.",
			"Weight(Kg)", "Rate", "Amount" };
	private DateFormat dateFormat;
	private Date date = new Date();
	private MyTableModel receipt_model;
	private JDateChooser dateChooser;
	private JComboBox cmbClient;
	private JComboBox txt_truck_no;
	private JButton btnAddEntry;
	private JButton btnCreateReceipt;
	private JButton btnClearInvoice;
	private JPanel panel_3;
	private JPanel panel_4;

	private void setReceiptTable(GroupLayout gl_contentPane, JPanel receiptPanel, GroupLayout gl_panel) {
		receiptPanel.setLayout(new BorderLayout(0, 0));

		JScrollPane scrollPane = new JScrollPane();
		receiptPanel.add(scrollPane, BorderLayout.CENTER);

		receipt_table = new JTable();
		receipt_model = new MyTableModel();
		receipt_model.setColumnNames(cols);

		receipt_table.setModel(receipt_model);
		JTableHeader h1 = receipt_table.getTableHeader();
		h1.setFont(new Font("Times New Roman", Font.BOLD, 14));
		receipt_table.setRowHeight(20);
		scrollPane.setViewportView(receipt_table);
		panel.setLayout(gl_panel);
		receiptPanel.add(BorderLayout.NORTH, h1);
		contentPane.setLayout(gl_contentPane);

	}

	private void setBillHistoryRow(Bill bill) {
		if (bill != null) {
			Vector<Object> setBillHistoryEntity = setBillHistoryEntity(bill);
			billHistoryModel.addRow(setBillHistoryEntity);
		}

	}

	private void setMenuBar() {

	}

	EntityDao dao = new EntityDao();
	List<Bill> bills = dao.getEntityList("Bill");
	private JTable historyTable;
	private JButton btnSearchGR;
	private JButton btnSelectARow;

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btnAddEntry) {
			if (!validateData().isEmpty()) {
				showMessageDialog(validateData());
			} else {
				setTableData(getBillEntry());
				calulateTotal();
				clearFields();
			}
		}

		if (e.getSource() == btnSearchGR) {
			if (!txt_gr_no.getText().isEmpty()) {
				int gr = Integer.parseInt(txt_gr_no.getText());
				setBillEntries(gr);
			}

		}

		if (e.getSource() == btnCreateReceipt) {
			if (!cmbClient.getSelectedItem().toString().isEmpty() && !txt_frm_add.getText().isEmpty()
					&& receipt_table.getRowCount() > 0) {
				Bill billObject = getBillObject();

				new Thread(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						String[] arr = txt_bill_no.getText().split("/");
						int pk = Integer.parseInt(arr[1]);
							dao.storeInDatabase(billObject, Constants.SYNC_FAILED);
						print("value stored in bill_generate with id:"+pk);
						
						clearFullForm();

					}
				}).start();
				setBillHistoryRow(billObject);
				updateCustomerList(billObject);
				System.out.println("Value Inserted Successfully..!!!");

			} else {

				String message = "Please Fill Client Farm Name \n and Address.\n Insert Bill Entries.";
				showMessageDialog(message);
			}

		}

		if (e.getSource() == btnClearInvoice) {
			clearFullForm();
		}
	}

	private void setBillEntries(int gr) {
		BilityModel bilityModel = (BilityModel) dao.getObject(BilityModel.class, gr);
		if (bilityModel != null) {

			txt_truck_no.setSelectedItem(bilityModel.getVeh_num());
			Lagguage l = bilityModel.getLagguage();
			if (l != null) {
				txt_wt.setText(convertToString(l.getT_wt()));
			}
			txt_item_amt.setText(convertToString(bilityModel.getGrand_total()));
		}
	}
	
	private void print(String msg) {
		
		
		
	}

	private Bill getBillObject() {
		Bill obj = new Bill();
		Customer client = new Customer();
		client.setCustomer_name(cmbClient.getSelectedItem().toString());
		client.setAddress(txt_frm_add.getText());
		String[] split = txt_bill_no.getText().split("/");
		// System.out.println("Bill Number"+split[1]);
		obj.setBill_id(Integer.parseInt(split[1]));
		obj.setClient(client);
		obj.setTotal_amt(parseDouble(txt_total_amt.getText()));
		obj.setBill_date(this.date);

		for (int i = 0; i < receipt_table.getRowCount(); i++) {
			CreateBillEntry entry = new CreateBillEntry();
			int c = dao.getMaxCount("bill_entries");
			if (c == 0) {
				c = 1;
			}
			entry.setS_id(c);
			entry.setStation(receipt_model.getValueAt(i, 3).toString());
			entry.setBid_id(receipt_model.getValueAt(i, 4).toString());
			entry.setAtt_gr(receipt_model.getValueAt(i, 5).toString());
			entry.setNug_cb(receipt_model.getValueAt(i, 6).toString());
			entry.setWeight(receipt_model.getValueAt(i, 7).toString());
			Vehicle vehicle = dao.getVehicle(receipt_model.getValueAt(i, 2).toString());
			if (vehicle != null) {

			} else {
				vehicle = new Vehicle();
				vehicle.setV_num(receipt_model.getValueAt(i, 2).toString());
			}
			entry.setVehicle(vehicle);

			try {
				entry.setRate(receipt_model.getValueAt(i, 8).toString());
				entry.setAmount(parseDouble(receipt_model.getValueAt(i, 9).toString()));
				entry.setBill_date(dateFormat.parse(receipt_model.getValueAt(i, 1).toString()));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {

			}
			// dao.storeInDatabase(entry);
			obj.getEntries().add(entry);

		}
		return obj;
	}

	private Vector<Object> setBillHistoryEntity(Bill bill) {
		// {"Bill No.","Date","Cust. Name","Address","Total Amt"};
		Vector<Object> data = new Vector<>();
		data.add(bill.getBill_id());
		data.add(dateFormat.format(bill.getBill_date()));
		Customer c = bill.getClient();
		if (c != null) {
			data.add(c.getCustomer_name());
			data.add(c.getAddress());
		}
		data.add(bill.getTotal_amt());
		return data;
	}

	private void setTableData(CreateBillEntry entry) {
		// {"SI.NO.","Date","Truck No.","Station","BID ID No.",
		// "Attached G.R. No.","Nug/C.B.","Weight(Kg)","Rate","Amount"};

//		for (int i = 0; i < receipt_model.getRowCount(); i++) {
//			if (receipt_model.getValueAt(i, 4).equals(entry.getBid_id())) {
//				receipt_model.removeRow(i);
//			}
//		}

		Map<String, Object> row = new HashMap<>();
		row.put(cols[0], receipt_table.getRowCount() + 1);
		row.put(cols[1], dateFormat.format(entry.getBill_date()));
		Vehicle v = entry.getVehicle();
		if (v != null) {
			row.put(cols[2], v.getV_num());
		} else {
			row.put(cols[2], "");
		}

		row.put(cols[3], entry.getStation());
		row.put(cols[4], entry.getBid_id());
		row.put(cols[5], entry.getAtt_gr());
		row.put(cols[6], entry.getNug_cb());
		row.put(cols[7], entry.getWeight());
		row.put(cols[8], entry.getRate());
		row.put(cols[9], entry.getAmount());
		receipt_model.addRow(new RowData(row));
	}

	private CreateBillEntry getBillEntry() {
		CreateBillEntry entry = new CreateBillEntry();
		entry.setBill_date(dateChooser.getDate());
		entry.setAmount(parseDouble(txt_item_amt.getText()));
		entry.setAtt_gr(txt_gr_no.getText());
		entry.setBid_id(txt_bid.getText());
		entry.setNug_cb(txt_nug.getText());
		Vehicle vehicle = dao.getVehicle(txt_truck_no.getSelectedItem().toString());
		if (vehicle != null) {

		} else {
			vehicle = new Vehicle();
			vehicle.setV_num(txt_truck_no.getSelectedItem().toString());
		}
		entry.setVehicle(vehicle);
		entry.setStation(txt_stat_1.getText());
		entry.setWeight(txt_wt.getText());
		entry.setRate(txt_rate_per.getText());

		return entry;
	}

	private String validateData() {
		StringBuilder err = new StringBuilder();
		if (txt_truck_no.getSelectedItem().toString().isEmpty()) {
			err.append("Please Fill Truck Number.\n");
		}
		
		if(txt_truck_no.getSelectedItem().toString().equalsIgnoreCase(Constants.SELECT_VEHICLE)) {
			err.append("Please Fill Truck Number.\n");
		}
		if (txt_stat_1.getText().isEmpty()) {
			err.append("Please Fill Station.\n");
		}
		if (txt_bid.getText().isEmpty()) {
			err.append("Please Fill Bid no.\n");
		}

		if (txt_gr_no.getText().isEmpty()) {
			err.append("Please Fill G.R. No. \n");
		}
		if (txt_wt.getText().isEmpty()) {
			err.append("Please Fill Weight 1\n");

		}

		if (txt_item_amt.getText().isEmpty()) {
			err.append("Please Fill Amount .\n");
		}

		return err.toString();
	}

	private Double parseDouble(String data) {
		double amt = 0.00;
		if (!data.isEmpty()) {
			amt = Double.parseDouble(data);
		}
		return amt;
	}

	private void showMessageDialog(String message) {
		JOptionPane.showMessageDialog(null, message);
	}

	private void calulateTotal() {
		double total = parseDouble(txt_total_amt.getText());
		total += parseDouble(txt_item_amt.getText());
		txt_total_amt.setText(String.format("%.2f", total));
	}

	public void clearFields() {
		txt_truck_no.setSelectedIndex(0);
		txt_stat_1.setText(null);
		txt_bid.setText(null);
		txt_gr_no.setText(null);
		txt_nug.setText(null);
		txt_wt.setText(null);
		txt_rate_per.setText(null);
		txt_item_amt.setText(null);

	}

	private void clearFullForm() {
		cmbClient.setSelectedIndex(-1);
		txt_frm_add.setText(null);
		txt_total_amt.setText(null);

		increaseBillCounter();

		clearFields();
		receipt_model.removeAll();

	}

	private void increaseBillCounter() {
		txt_bill_no.setText(String.format("CFC/%s", (dao.getMaxCount("bill_generate"))));
		// "CFC/"+(dao.getMaxCount("bill_generate")+1
	}

	private <T> String convertToString(T obj) {
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		if (obj instanceof Integer) {
			return String.valueOf(obj);
		}
		if (obj instanceof Double) {
			return String.format("%.2f", obj);
		}
		if (obj instanceof Date) {
			return dateFormat.format(obj);
		}
		return "";
	}

}
