package com.javacore.transportMgmt.utility;

import java.text.DecimalFormat;

public class EnglishNumberToWords {

	private static final String[] tensNames = {
			"",
			" ten",
			" twenty",
			" thirty",
			" forty",
			" fifty",
			" sixty",
			" seventy",
			" eighty",
			" ninety"
	};
	
	private static final String[] numNames = {
			"",
			" one",
			" two",
			" three",
			" four",
			" five",
			" six",
			" seven",
			" eight",
			" nine",
			" ten",
			" eleven",
			" twelve",
			" thirteen",
			" fourteen",
			" fifteen",
			" sixteen",
			" seventeen",
			" eighteen",
			" nineteen"
	};
	
	private EnglishNumberToWords() {}
	
	private static String convertLessThanOneThousand(int number) {
		String soFar ="";
		
		if (number % 100 <20) {
			soFar = numNames[number%100];
			number /= 100;
		}else {
			soFar = numNames[number%10];
			number /= 10;
			
			soFar = tensNames[number%10]+soFar;
			number /=10;
		}
		
		if (number == 0) {
			return soFar;
		}
		return numNames[number]+" hundred"+soFar;
	}
	
	public static String convert(long number) {
		if (number == 0) {
			return "zero";
		}
		String snumber = Long.toString(number);
		//pad with "0"
		String mask = "000000000000";
		DecimalFormat df = new DecimalFormat(mask);
		snumber = df.format(number);
		int tcrore = Integer.parseInt(snumber.substring(0, 2));
		int crore = Integer.parseInt(snumber.substring(2, 5));
		int lakh = Integer.parseInt(snumber.substring(5, 7));
		int th = Integer.parseInt(snumber.substring(7, 9));
		int hun = Integer.parseInt(snumber.substring(9, 12));
		
		
		String tradtcrore;
		switch(tcrore) {
		case 0:
			tradtcrore = "";
			break;
		case 1:
			tradtcrore = convertLessThanOneThousand(tcrore)+" thousand crore";
			break;
		default:
			tradtcrore = convertLessThanOneThousand(tcrore)+" thousand crore";
		
		}
		String result = tradtcrore;
		String tradcrore;
		switch(crore) {
		case 0:
			tradcrore = "";
			break;
		case 1:
			tradcrore = convertLessThanOneThousand(crore)+" crore";
			break;
		default:
			tradcrore = convertLessThanOneThousand(crore)+" crore";
		}
		result = result + tradcrore;
		String tradlakh;
		switch(lakh) {
		case 0:
			tradlakh = "";
			break;
		case 1:
			tradlakh = convertLessThanOneThousand(lakh)+" lakh";
			break;
		default:
			tradlakh = convertLessThanOneThousand(lakh)+" lakh";
		}
		result = result + tradlakh;
		
		String tradthou;
		switch(th) {
		case 0:
			tradthou = "";
			break;
		case 1:
			tradthou = convertLessThanOneThousand(th)+" thousand";
			break;
		default:
			tradthou = convertLessThanOneThousand(th)+" thousand";
		}
		result = result + tradthou;
		
		String tradhund;
		switch(hun) {
		case 0:
			tradhund = "";
			break;
		case 1:
			tradhund = convertLessThanOneThousand(hun);
			break;
		default:
			tradhund = convertLessThanOneThousand(hun);
		}
		result = result + tradhund;
		return result.replaceAll("^\\s+", "").replaceAll("\\b\\s{2,}\\b", " ");
	}
}
