package com.javacore.transportMgmt.utility;

import java.awt.Desktop;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.javacore.transportMgmt.db.EntityDao;
import com.javacore.transportMgmt.model.BilityModel;
import com.javacore.transportMgmt.model.Bill;
import com.javacore.transportMgmt.model.CreateBillEntry;
import com.javacore.transportMgmt.model.Customer;
import com.javacore.transportMgmt.model.Insurance;
import com.javacore.transportMgmt.model.Lagguage;
import com.javacore.transportMgmt.model.Vehicle;

import jxl.Workbook;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

public class PdfModel {
	private Document document;

	public static final String NEWLINE = "\n";
	Font boldFont = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD);
	Font normalFont = new Font(Font.FontFamily.TIMES_ROMAN, 9, Font.ITALIC);
	private EntityDao entityDao = new EntityDao();
	float left = 30;
	float right = 30;
	float top = 60;
	float bottom = 0;

	public void createBility(int gr) {
		// TODO Auto-generated method stub
		BilityModel bilityModel = entityDao.getBilty(gr);

		if (null == bilityModel) {
			return;
		}
		document = new Document(PageSize.A4, left, right, top, bottom);
		String doc_name = "bility"+gr + ".pdf";

		try {
			PdfWriter.getInstance(document, new FileOutputStream(doc_name));
			document.open();
			try {
				Image image = Image.getInstance("resources/banner.jpg");
				image.scaleAbsolute(new Rectangle(500, 120));
				image.setAlignment(Element.ALIGN_CENTER);
				document.add(image);
			} catch (Exception e) {
				e.printStackTrace();
			}
			PdfPTable table = new PdfPTable(4);
			table.setWidthPercentage(100);
			table.setSpacingBefore(30f);
			table.setWidths(new int[] { 1, 1, 1, 1 });
			PdfPCell cell;

			setOwnerDetails(bilityModel, table);

			setCustomer(bilityModel, table);
			document.add(table);
			document.add(Chunk.NEWLINE);
			PdfPTable table1 = new PdfPTable(4);
			table1.setWidthPercentage(100);
			table1.setWidths(new float[] { 0.4f, 2.6f, 0.5f, 0.5f });
			cell = new PdfPCell(new Phrase("No.of Pkgs", boldFont));
			table1.addCell(cell);
			cell = new PdfPCell(new Phrase("Description", boldFont));
			table1.addCell(cell);
			cell = new PdfPCell(new Phrase("Actual Weight", boldFont));
			table1.addCell(cell);
			cell = new PdfPCell(new Phrase("Weight Charges", boldFont));
			table1.addCell(cell);
			Lagguage l = bilityModel.getLagguage();
			System.out.println(l);
			if (l != null) {
				cell = new PdfPCell(new Phrase(convertToString(l.getT_packages()), normalFont));
				cell.setFixedHeight(20);
				table1.addCell(cell);
				cell = new PdfPCell(new Phrase(l.getItems_desc(), normalFont));
				cell.setFixedHeight(20);
				table1.addCell(cell);
				cell = new PdfPCell(new Phrase(convertToString(l.getT_qty()), normalFont));
				cell.setFixedHeight(20);
				table1.addCell(cell);
				cell = new PdfPCell(new Phrase(convertToString(l.getT_wt()), normalFont));
				cell.setFixedHeight(20);
				table1.addCell(cell);

			}
			cell = new PdfPCell();
			cell.setFixedHeight(200);
			table1.addCell(cell);
			cell = new PdfPCell();
			cell.setFixedHeight(200);
			table1.addCell(cell);
			cell = new PdfPCell();
			cell.setFixedHeight(200);
			table1.addCell(cell);
			cell = new PdfPCell();
			cell.setFixedHeight(200);
			table1.addCell(cell);

			document.add(table1);

			PdfPTable table2 = new PdfPTable(4);
			table2.setWidthPercentage(100);
			table2.setWidths(new int[] { 1, 1, 1, 1 });
			addCost(bilityModel, table2);
			document.add(table2);
			document.close();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			if (Desktop.isDesktopSupported()) {
				File file = new File(doc_name);
				try {
					Desktop.getDesktop().open(file);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					showWarningMessage();
					e.printStackTrace();
				}
			}

		}
	}

	private void setOwnerDetails(BilityModel bilityModel, PdfPTable table) {
		PdfPCell cell;
		Phrase ph = new Phrase("Insurance", boldFont);
		cell = new PdfPCell(ph);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);

		cell.setColspan(2);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase("G R No.", boldFont));
		table.addCell(cell);
		table.addCell(new Phrase(convertToString(bilityModel.getGr_id()), normalFont));
		Insurance in = bilityModel.getInsurance();
		Vehicle v = new Vehicle();
		v = entityDao.getVehicle(bilityModel.getVeh_num());

		cell = new PdfPCell(new Phrase("Company", boldFont));
		table.addCell(cell);
		if (in == null) {
			in = new Insurance();

		}
		if (v == null) {
			v = new Vehicle();
		}

		table.addCell(new Phrase(in.getCompany_name(), normalFont));
		table.addCell(new Phrase("Driver Name", boldFont));
		table.addCell(new Phrase(v.getDriver_name(), normalFont));
		table.addCell(new Phrase("Policy No.", boldFont));
		table.addCell(new Phrase(in.getPolicy_num(), normalFont));
		table.addCell(new Phrase("Owner Name:", boldFont));
		table.addCell(new Phrase(v.getOwner_name(), normalFont));
		table.addCell(new Phrase("Amount", boldFont));
		table.addCell(new Phrase(convertToString(in.getAmount()), normalFont));
		table.addCell(new Phrase("Vehicle No.", boldFont));
		table.addCell(new Phrase(v.getV_num(), normalFont));

	}

	private void setCustomer(BilityModel bilityModel, PdfPTable table) {
		PdfPCell cell;
		Customer c1 = bilityModel.getConsignor();
		if (c1 != null) {
			cell = new PdfPCell(new Phrase("Consignor Name", boldFont));
			cell.setColspan(2);
			table.addCell(cell);

			cell = new PdfPCell(new Phrase(c1.getCustomer_name(), normalFont));
			cell.setColspan(2);
			table.addCell(cell);
			cell = new PdfPCell(new Phrase("Consignor Address", boldFont));
			cell.setColspan(2);
			table.addCell(cell);

			cell = new PdfPCell(new Phrase(c1.getAddress(), normalFont));
			cell.setColspan(2);
			table.addCell(cell);

			cell = new PdfPCell(new Phrase("GST", boldFont));
			cell.setColspan(2);
			table.addCell(cell);

			cell = new PdfPCell(new Phrase(c1.getGst_no(), normalFont));
			cell.setColspan(2);
			table.addCell(cell);
		}
		Customer c2 = bilityModel.getConsignee();
		if (c1 != null) {
			cell = new PdfPCell(new Phrase("Consignee Name", boldFont));
			cell.setColspan(2);
			table.addCell(cell);

			cell = new PdfPCell(new Phrase(c2.getCustomer_name(), normalFont));
			cell.setColspan(2);
			table.addCell(cell);
			cell = new PdfPCell(new Phrase("Consignee Address", boldFont));
			cell.setColspan(2);
			table.addCell(cell);

			cell = new PdfPCell(new Phrase(c2.getAddress(), normalFont));
			cell.setColspan(2);
			table.addCell(cell);

			cell = new PdfPCell(new Phrase("GST", boldFont));
			cell.setColspan(2);
			table.addCell(cell);

			cell = new PdfPCell(new Phrase(c2.getGst_no(), normalFont));
			cell.setColspan(2);
			table.addCell(cell);
		}
	}

	private void addCost(BilityModel bilityModel, PdfPTable table) {
		PdfPCell cell;
		cell = new PdfPCell(new Phrase("RATE per Kg.", boldFont));
		table.addCell(cell);
		cell = new PdfPCell(new Phrase("Rs.", boldFont));
		table.addCell(cell);
		cell = new PdfPCell(new Phrase("Freight", boldFont));
		table.addCell(cell);
		cell = new PdfPCell(new Phrase(bilityModel.getFre_type(), normalFont));
		table.addCell(cell);
		cell = new PdfPCell(new Phrase("Freight", boldFont));
		table.addCell(cell);
		cell = new PdfPCell(new Phrase(convertToString(bilityModel.getFreight()), normalFont));
		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		// cell.setColspan(3);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase("", normalFont));
		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cell.setColspan(2);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase("CGST", boldFont));
		table.addCell(cell);
		cell = new PdfPCell(new Phrase(convertToString(bilityModel.getCgst()), normalFont));
		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		// cell.setColspan(3);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase("", normalFont));
		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cell.setColspan(2);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase("SGST", boldFont));
		table.addCell(cell);

		cell = new PdfPCell(new Phrase(convertToString(bilityModel.getSgst()), normalFont));
		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		// cell.setColspan(3);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase("", normalFont));
		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cell.setColspan(2);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase("Check Post Exp.", boldFont));
		table.addCell(cell);

		cell = new PdfPCell(new Phrase(convertToString(bilityModel.getChck_exp()), normalFont));
		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		// cell.setColspan(3);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase("", normalFont));
		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cell.setColspan(2);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase("Statical Charge ", boldFont));
		table.addCell(cell);

		cell = new PdfPCell(new Phrase(convertToString(bilityModel.getStat_charge()), normalFont));
		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		// cell.setColspan(3);

		table.addCell(cell);
		cell = new PdfPCell(new Phrase("", normalFont));
		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cell.setColspan(2);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase("Grand Total", boldFont));
		table.addCell(cell);

		cell = new PdfPCell(new Phrase(convertToString(bilityModel.getGrand_total()), normalFont));
		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		// cell.setColspan(3);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase("", normalFont));
		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cell.setColspan(2);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase("Advance ", boldFont));
		table.addCell(cell);

		cell = new PdfPCell(new Phrase(convertToString(bilityModel.getAdv_pay()), normalFont));
		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		// cell.setColspan(3);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase("", normalFont));
		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cell.setColspan(2);
		table.addCell(cell);
		cell = new PdfPCell(new Phrase("Balance", boldFont));
		table.addCell(cell);

		cell = new PdfPCell(new Phrase(convertToString(bilityModel.getBalnce()), normalFont));
		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		table.addCell(cell);

		cell = new PdfPCell(new Phrase("GST Paid By", boldFont));
		table.addCell(cell);

		cell = new PdfPCell(new Phrase(bilityModel.getGst_paid_by(), normalFont));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(cell);
	}

	public void createBill(int pk) {
//		System.out.println(new Date().getTime()+" createBill pk:"+pk);
		Bill bill = entityDao.getBill(pk);
		if (bill != null) {

		} else {
			return;
		}
		document = new Document(PageSize.A4, left, right, top, bottom);
		String doc_name = "CFC-"+pk + ".pdf";
		try {
			PdfWriter.getInstance(document, new FileOutputStream(doc_name));
			document.open();
			Image image = Image.getInstance("resources/banner.jpg");
			image.scaleAbsolute(new Rectangle(500, 120));
			image.setAlignment(Element.ALIGN_CENTER);
			document.add(image);
			
			PdfPTable table = new PdfPTable(10);
			table.setWidthPercentage(100);
			table.setSpacingBefore(40f);
			table.setWidths(new float[] { 0.5f, 1.5f, 1.5f, 2f, 1f, 1, 1f, 1.5f, 1f, 1.5f });
			PdfPCell cell;
			Phrase ph;
			setCustomerDetails(bill, table);

			billTableHeader(table);

			addBillEntries(bill, table);

			document.add(table);
			PdfPTable table1 = new PdfPTable(2);
			table1.setWidthPercentage(100);
			ph = new Phrase("Total Amount: ", normalFont);
			ph.add(new Chunk(convertToString(bill.getTotal_amt())));
			cell = new PdfPCell(ph);
			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell.setColspan(2);
			table1.addCell(cell);

			ph = new Phrase("Amount in words: ", normalFont);
			ph.add(new Chunk(EnglishNumberToWords.convert(bill.getTotal_amt().longValue()).toUpperCase()+" ONLY"));
			cell = new PdfPCell(ph);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setColspan(2);
			table1.addCell(cell);

			ph = new Phrase("GST PAID BY CONSIGNOR ", normalFont);
			cell = new PdfPCell(ph);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell.setFixedHeight(30);
			table1.addCell(cell);

			ph = new Phrase("FOR CHAUHAN FRFEIGHT CARIERS ", normalFont);
			cell = new PdfPCell(ph);
			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell.setFixedHeight(30);
			table1.addCell(cell);

			document.add(table1);
			document.close();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (Desktop.isDesktopSupported()) {
				File file = new File(doc_name);
				try {
					Desktop.getDesktop().open(file);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					showWarningMessage();
					e.printStackTrace();
				}
			}

		}

	}

	private void setCustomerDetails(Bill bill, PdfPTable table) {
		PdfPCell cell;
		Phrase ph = new Phrase("M/s ", boldFont);
		cell = new PdfPCell(ph);
		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cell.setColspan(2);
		table.addCell(cell);
		Customer c = bill.getClient();

		if (c != null) {
			ph = new Phrase(c.getCustomer_name() , boldFont);
			cell = new PdfPCell(ph);
			cell.setPaddingLeft(5);
			cell.setColspan(6);
			table.addCell(cell);
		}
		ph = new Phrase("Bill No.", boldFont);
		cell = new PdfPCell(ph);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(cell);

		ph = new Phrase("CFC/" + bill.getBill_id(), boldFont);
		cell = new PdfPCell(ph);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(cell);

		ph = new Phrase("Add ",boldFont);
		cell = new PdfPCell(ph);
		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cell.setColspan(2);
		table.addCell(cell);
		if(c != null) 
			ph = new Phrase(c.getAddress(),boldFont);
		else
			ph = new Phrase();
		cell = new PdfPCell(ph);
		cell.setColspan(6);
		table.addCell(cell);
		
		ph = new Phrase("Date", boldFont);
		cell = new PdfPCell(ph);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(cell);

		ph = new Phrase(convertToString(bill.getBill_date()), boldFont);
		cell = new PdfPCell(ph);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);

		table.addCell(cell);
	}

	private void billTableHeader(PdfPTable table) {
		PdfPCell cell;
		Phrase ph;
		ph = new Phrase("SL NO", boldFont);
		cell = new PdfPCell(ph);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_CENTER);
		cell.setPaddingTop(5);
		cell.setPaddingBottom(5);
		table.addCell(cell);

		ph = new Phrase("Date", boldFont);
		cell = new PdfPCell(ph);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_CENTER);
		cell.setPaddingTop(5);
		cell.setPaddingBottom(5);
		table.addCell(cell);

		ph = new Phrase("Truck No.", boldFont);
		cell = new PdfPCell(ph);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_CENTER);
		cell.setPaddingTop(5);
		cell.setPaddingBottom(5);
		table.addCell(cell);

		ph = new Phrase(" Station ", boldFont);
		cell = new PdfPCell(ph);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_CENTER);
		cell.setPaddingTop(5);
		cell.setPaddingBottom(5);
		table.addCell(cell);

		ph = new Phrase("BID ID", boldFont);
		cell = new PdfPCell(ph);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_CENTER);
		cell.setPaddingTop(5);
		cell.setPaddingBottom(5);
		table.addCell(cell);

		ph = new Phrase("Att. G.R.No.", boldFont);
		cell = new PdfPCell(ph);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_CENTER);
		cell.setPaddingTop(5);
		cell.setPaddingBottom(5);
		table.addCell(cell);

		ph = new Phrase("Nug/C.B.", boldFont);
		cell = new PdfPCell(ph);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_CENTER);
		cell.setPaddingTop(5);
		cell.setPaddingBottom(5);
		table.addCell(cell);

		ph = new Phrase("Weight (Kg)", boldFont);
		cell = new PdfPCell(ph);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_CENTER);
		cell.setPaddingTop(5);
		cell.setPaddingBottom(5);
		table.addCell(cell);

		ph = new Phrase("Rate", boldFont);
		cell = new PdfPCell(ph);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_CENTER);
		cell.setPaddingTop(5);
		cell.setPaddingBottom(5);
		table.addCell(cell);

		ph = new Phrase("Amount", boldFont);
		cell = new PdfPCell(ph);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_CENTER);
		cell.setPaddingTop(5);
		cell.setPaddingBottom(5);
		table.addCell(cell);
	}

	private void addBillEntries(Bill bill, PdfPTable table) {
		PdfPCell cell;
		Phrase ph;
		int j = 0;
		List<CreateBillEntry> entries = bill.getEntries();
		if (entries != null) {
			for (CreateBillEntry entry : entries) {

				ph = new Phrase(convertToString(++j), normalFont);
				cell = new PdfPCell(ph);
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell.setVerticalAlignment(Element.ALIGN_CENTER);
				cell.setFixedHeight(20);
				table.addCell(cell);

				ph = new Phrase(convertToString(entry.getBill_date()), normalFont);
				cell = new PdfPCell(ph);
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell.setVerticalAlignment(Element.ALIGN_CENTER);
				cell.setFixedHeight(20);
				table.addCell(cell);
				Vehicle v = entry.getVehicle();
				if (v != null) {
					ph = new Phrase(v.getV_num(), normalFont);
				} else {
					ph = new Phrase();
				}

				cell = new PdfPCell(ph);
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell.setVerticalAlignment(Element.ALIGN_CENTER);
				cell.setFixedHeight(20);
				table.addCell(cell);

				ph = new Phrase(entry.getStation(), normalFont);
				cell = new PdfPCell(ph);
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell.setVerticalAlignment(Element.ALIGN_CENTER);
				cell.setFixedHeight(20);
				table.addCell(cell);

				ph = new Phrase(entry.getBid_id(), normalFont);
				cell = new PdfPCell(ph);
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell.setVerticalAlignment(Element.ALIGN_CENTER);
				cell.setFixedHeight(20);
				table.addCell(cell);

				ph = new Phrase(entry.getAtt_gr(), normalFont);
				cell = new PdfPCell(ph);
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell.setVerticalAlignment(Element.ALIGN_CENTER);
				cell.setFixedHeight(20);
				table.addCell(cell);

				ph = new Phrase(entry.getNug_cb(), normalFont);
				cell = new PdfPCell(ph);
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell.setVerticalAlignment(Element.ALIGN_CENTER);
				cell.setFixedHeight(20);
				table.addCell(cell);

				ph = new Phrase(entry.getWeight(), normalFont);
				cell = new PdfPCell(ph);
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell.setVerticalAlignment(Element.ALIGN_CENTER);
				cell.setFixedHeight(20);
				table.addCell(cell);

				ph = new Phrase(entry.getRate(), normalFont);
				cell = new PdfPCell(ph);
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell.setVerticalAlignment(Element.ALIGN_CENTER);
				cell.setFixedHeight(20);
				table.addCell(cell);

				ph = new Phrase(convertToString(entry.getAmount()), normalFont);
				cell = new PdfPCell(ph);
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell.setVerticalAlignment(Element.ALIGN_CENTER);
				cell.setFixedHeight(20);
				table.addCell(cell);
			}
			for (int i = 0; i < 10; i++) {
				ph = new Phrase();
				cell = new PdfPCell(ph);
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell.setVerticalAlignment(Element.ALIGN_CENTER);
				cell.setFixedHeight(200);

				table.addCell(cell);
			}
		}
	}

	private <T> String convertToString(T obj) {
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		if (obj instanceof Integer) {
			return String.valueOf(obj);
		}
		if (obj instanceof Double) {
			return String.format("%.2f", obj);
		}
		if (obj instanceof Date) {
			return dateFormat.format(obj);
		}
		return "";
	}

	public static void exportExcel(String heading, DefaultTableModel model) {

		int colCount = model.getColumnCount();
		int rowCount = model.getRowCount();
		String doc_name = "resources/cList.xls";
		WritableWorkbook myFirstWbook = null;
		try {
			myFirstWbook = Workbook.createWorkbook(new File(doc_name));
			// create an Excel sheet
			WritableSheet excelSheet = myFirstWbook.createSheet("Sheet 1", 0);
			WritableCellFormat cFormat = new WritableCellFormat();
			WritableFont font = new WritableFont(WritableFont.TIMES, 14, WritableFont.BOLD);
			cFormat.setFont(font);
			excelSheet.setRowView(100, true);
			for (int i = 0; i < colCount; i++) {
				excelSheet.setColumnView(i, 20);
			}
			/*
			 * excelSheet.setColumnView(0, 20);// DATE excelSheet.setColumnView(1, 20);//
			 * FEES excelSheet.setColumnView(2, 20);// MED excelSheet.setColumnView(3,
			 * 20);// PWR
			 */
			Label label = null;
			label = new Label(0, 0, heading, cFormat);
			excelSheet.addCell(label);
			for (int i = 0; i < colCount; i++) {
				label = new Label(i, 1, model.getColumnName(i), getCellFormat("COME"));
				excelSheet.addCell(label);
			}

			for (int j = 0; j < rowCount; j++) {

				for (int k = 0; k < colCount; k++) {

					label = new Label(k, j + 2, model.getValueAt(j, k).toString(), cFormat);
					excelSheet.addCell(label);

				}
			}

			myFirstWbook.write();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (WriteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				myFirstWbook.close();
				if (Desktop.isDesktopSupported()) {
					File file = new File(doc_name);
					Desktop.getDesktop().open(file);
				}
			} catch (IOException e) {
				showMessageDialog("Excel File is not Supported.");
				e.printStackTrace();
			} catch (WriteException e2) {
				// TODO: handle exception
				e2.printStackTrace();
			}
		}
	}
	
	private static void showWarningMessage() {
		JOptionPane.showMessageDialog(null, "Please Close the open document first", "Alert", JOptionPane.WARNING_MESSAGE);
	}

	private static void showMessageDialog(String message) {
		JOptionPane.showMessageDialog(null, message);
	}

	private static WritableCellFormat getCellFormat(String type) throws WriteException {
		WritableCellFormat cf = new WritableCellFormat();
		cf.setBorder(Border.ALL, BorderLineStyle.THIN, Colour.RED);
		cf.setFont(new WritableFont(WritableFont.TIMES, 14, WritableFont.BOLD));
		switch (type) {
		case "TOTAL":
			cf.setBackground(Colour.WHITE);
			break;
		case "COME":
			cf.setBackground(Colour.RED);
			break;
		case "NEW":
			cf.setBackground(Colour.WHITE);
			break;
		case "REPEAT":
			cf.setBackground(Colour.YELLOW);
			break;
		case "OLD":
			cf.setBackground(Colour.WHITE);
			break;
		case "COURIER":
			cf.setBackground(Colour.WHITE);
			break;
		case "ABSENT":
			cf.setBackground(Colour.PINK);
			break;
		case "CANCEL":
			cf.setBackground(Colour.GREY_25_PERCENT);
			break;
		default:
			cf.setBackground(Colour.WHITE);

		}
		return cf;
	}

}
