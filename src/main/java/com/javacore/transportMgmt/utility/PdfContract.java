package com.javacore.transportMgmt.utility;

import com.javacore.transportMgmt.model.BilityModel;
import com.javacore.transportMgmt.model.Bill;

public interface PdfContract {

	public void createBility(BilityModel bilityModel);
	public void createBill(Bill bill);
}
